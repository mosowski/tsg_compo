package fabulous {
    import flash.display.BitmapData;
    import flash.utils.ByteArray;
	
    class TextureSource {
        public var _bitmapData:BitmapData;
        public var _byteArray:ByteArray;

        public function TextureSource() {
        }

        public function dispose():Void {
            if (_bitmapData) {
                _bitmapData.dispose();
                _bitmapData = null;
            }

            if (_byteArray) {
                _byteArray.clear();
                _byteArray = null;
            }
        }
    }

}