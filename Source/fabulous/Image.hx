package fabulous;

class Image extends Renderable {
    public var _texture:Texture;
    public var _material:Material;
    public var _frame:SpriteSheetFrame;

    public var _u00:Float;
    public var _v00:Float;
    public var _u01:Float;
    public var _v01:Float;
    public var _u11:Float;
    public var _v11:Float;
    public var _u10:Float;
    public var _v10:Float;

    public var _x00:Float;
    public var _y00:Float;
    public var _x01:Float;
    public var _y01:Float;
    public var _x11:Float;
    public var _y11:Float;
    public var _x10:Float;
    public var _y10:Float;

    public var _alpha00:Float;
    public var _alpha01:Float;
    public var _alpha11:Float;
    public var _alpha10:Float;

    public var _hitmask:Hitmask;
	public var _radius:Float;

    public static var _renderer:Renderer;

    public function new(material:Material) {
		super();
        _texture = null;
        _material = material;

        _u00 = 0;
        _v00 = 0;

        _u01 = 0;
        _v01 = 1;

        _u11 = 1;
        _v11 = 1;

        _u10 = 1;
        _v10 = 0;

        _alpha00 = 1;
        _alpha01 = 1;
        _alpha11 = 1;
        _alpha10 = 1;

        _hitmask = new Hitmask();
        _hitmask.makeQuad();
    }

    override public function get_renderer():Renderer {
        return _renderer;
    }

    override public function get_hitmask():Hitmask {
        return _hitmask;
    }

	override public function get_radius():Float {
		return _radius;
	}

    public var texture(get, set):Texture;
    public function set_texture(v:Texture):Texture {
        return _texture = v;
    }
    public function get_texture():Texture {
        return _texture;
    }
    
    public function setUVRect(x:Float, y:Float, width:Float, height:Float):Void {
        _u00 = x;
        _v00 = y;

        _u01 = x;
        _v01 = y + height;

        _u11 = x + width;
        _v11 = y + height;

        _u10 = x + width;
        _v10 = y;
    }

    /**
     * Sets the shape of the image to the rectangle shape, where x and y are top-left
     * corner coordinates
     * @param	x
     * @param	y
     * @param	width
     * @param	height
     */
    
    public function setShapeRect(x:Float, y:Float, width:Float, height:Float):Void {
        _x00 = x;
        _y00 = y;

        _x01 = x;
        _y01 = y + height;

        _x11 = x + width;
        _y11 = y + height;

        _x10 = x + width;
        _y10 = y;

		_radius = Math.max(Math.max(-x, -y), Math.max(x+width, y+height));

		_hitmask.setRectangle(x, y, width, height);
    }

    /**
     * Sets the shape of the image to the rectangle shape, where x and y are top-left
     * corner coordinates. Then, the rectangle is rotaded by rotation radians around originX
     * and originY.
     * @param	x
     * @param	y
     * @param	width
     * @param	height
     * @param	rotation
     * @param	originX
     * @param	originY
     */
    public function setShapeRotatedRect(x:Float, y:Float, width:Float, height:Float, rotation:Float, originX:Float = 0, originY:Float = 0):Void {
        var sin:Float = Math.sin(rotation);
        var cos:Float = Math.cos(rotation);
        _x00 = x * cos - y * sin;
        _y00 = x * sin + y * cos;

        _x01 = x * cos - (y + height) * sin;
        _y01 = x * sin + (y + height) * cos;

        _x11 = (x + width) * cos - (y + height) * sin;
        _y11 = (x + width) * sin + (y + height) * cos;

        _x10 = (x + width) * cos - y * sin;
        _y10 = (x + width) * sin + y * cos;

        var left:Float = Math.min(Math.min(_x00, _x01), Math.min(_x11, _x10));
        var top:Float = Math.min(Math.min(_y00, _y01), Math.min(_y11, _y10));
        var right:Float = Math.max(Math.max(_x00, _x01), Math.max(_x11, _x10));
        var bottom:Float = Math.max(Math.max(_y00, _y01), Math.max(_y11, _y10));
		_radius = Math.max(Math.max(-left, -top), Math.max(right, bottom));

		_hitmask.setRectangle(left, top, right - left, bottom - top);
    }

    /**
     * Sets alpha to the vertices, where a00 means top left vertex and a11 right bottom
     * one considering positive scale.
     * @param	a00
     * @param	a01
     * @param	a11
     * @param	a10
     */
    public function setVerticesAlpha(a00:Float = 1, a01:Float = 1, a11:Float = 1, a10:Float = 1):Void {
        _alpha00 = a00;
        _alpha01 = a01;
        _alpha11 = a11;
        _alpha10 = a10;
    }

	
	public function setVertices(x00:Float,y00:Float,x01:Float, y01:Float, x11:Float,y11:Float,x10:Float, y10:Float):Void {
		_x00 = x00;
		_y00 = y00;
		_x01 = x01;
		_y01 = y01;
		_x11 = x11;
		_y11 = y11;
		_x10 = x10;
		_y10 = y10;

        var left:Float = Math.min(Math.min(_x00, _x01), Math.min(_x11, _x10));
        var top:Float = Math.min(Math.min(_y00, _y01), Math.min(_y11, _y10));
        var right:Float = Math.max(Math.max(_x00, _x01), Math.max(_x11, _x10));
        var bottom:Float = Math.max(Math.max(_y00, _y01), Math.max(_y11, _y10));
		_radius = Math.max(Math.max(-left, -top), Math.max(right, bottom));

		_hitmask.setRectangle(left, top, right - left, bottom - top);
	}
    
	public var material(get,set):Material;
    public function set_material(v:Material):Material{
        return _material = v;
    }
    public function get_material():Material {
        return _material;
    }


	public var frame(get,set):SpriteSheetFrame;
    public function set_frame(frame:SpriteSheetFrame):SpriteSheetFrame {
        if (_frame != frame) {
            _frame = frame;
			if (_frame != null) {
            	_texture = _frame._texture;
				setShapeRect(0, 0, _frame._imageWidth, _frame._imageHeight);
				setFrameUV();
			} else {
				_texture = null;
			}
        }
		return frame;
    }
	public function get_frame():SpriteSheetFrame {
		return _frame;
	}

    private function setFrameUV():Void {
        setUVRect(_frame._left/_texture._width, _frame._top/_texture._height, (_frame._right - _frame._left)/_texture._width, (_frame._bottom - _frame._top)/_texture._height);
    }
}
