package fabulous;

class RenderBatchBuffer {
    public var _device:Device;

    public var _ib:IndexBuffer;
    public var _vb:VertexBuffer;

    public var _numQueuedIndices:Int;

    public static var VBTYPE_NONE:Int = 0;
    public static var VBTYPE_FLOAT:Int = 1;

    public function new(device:Device, vbSize:Int, vbType:Int, ibSize:Int, vbFormat:Array<VertexBufferAttrib>) {
        _device = device;
        _ib = new IndexBuffer(device);
        _ib.setSize(ibSize);
        if (vbType == VBTYPE_FLOAT) {
            _vb = new FloatVertexBuffer(device);
        }

        if (vbType != VBTYPE_NONE ) {
            _vb.setSizeAndFormat(vbSize, vbFormat);
        }
        _numQueuedIndices = 0;
    }

    public function upload():Void {
		_vb.upload();
        _ib.upload();
    }

    public function reset():Void {
		_vb.position = 0;
        _ib.position = 0;
        _numQueuedIndices = 0;
    }

    public function bind():Void {
		_device.setVertexBuffer(_vb);
		_device.setIndexBuffer(_ib);
    }

    public function unbind():Void {
    }
}
