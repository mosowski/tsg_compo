package fabulous {
    import flash.utils.Dictionary;
	
    class ClipDataManager {
        public var _device:Device;
        public var _clipDatas:Dictionary;

        public var _clipDataConfig:Dictionary;

        public var _unloadedClipDataSource:Dynamic;
        public static const UNLOADED_CLIP_DATA_NAME:String = "__unloadedClipData__";
        public static const UNLOADED_ANIMATION_DATA_NAME:String = "__unloadedAnimationData__";

        public function ClipDataManager(device:Device) {
            _device = device;
            _clipDatas = new Dictionary();
        }

        public function init():Void {
            _unloadedClipDataSource = {
                animations: { }
            };
            _unloadedClipDataSource.name = UNLOADED_CLIP_DATA_NAME;
            _unloadedClipDataSource.animations[UNLOADED_ANIMATION_DATA_NAME] = {
                totalFrames: 1,
                frames: [
                    {
                        texture: TextureManager.UNLOADED_TEXTURE_NAME,
                        vertices: [ ],
                        indices: [ ]
                    }
                ],
                hitmask: {
                    path: [ ],
                    indices: [ ]
                },
                labels: [ ],
                frameTexture: [  ],
                metaNodes: [ ],
				bounds: [0, 0, 0, 0]
            };
        }

        public function loadClipDataConfigFromDynamic(object:Dynamic):Void {
            _clipDataConfig = new Dictionary();
            for (var clipDataName:String in object) {
                _clipDataConfig[clipDataName] = object[clipDataName];
            }
        }

        public function loadClipDataFromDynamic(object:Dynamic):Void {
            var clipData:ClipData = new ClipData();
            clipData.fromDynamic(object, _device);
            _clipDatas[clipData._name] = clipData;
        }

        public function getClipData(name:String):ClipData {
            var clipData:ClipData = _clipDatas[name];
            if (!clipData) {
                clipData = _clipDatas[name] = new ClipData();
                clipData.fromDynamic(_unloadedClipDataSource, _device);
                if (!_clipDataConfig[name]) {
                    throw "No clipData for name '" + name + "'.";
                } else if (!_clipDataConfig[name][_device._deviceProfile]) {
                    throw "No clipData for profile '" + _device._deviceProfile + "' for '" + name + "'.";
                } else {
                    var path:String = _device.getPath(_clipDataConfig[name][_device._deviceProfile]);

                    _device._loaderMgr.startTextLoader(path, path,
                        function(ldrName:String):Void {
                            var loader:BinaryLoader = _device._loaderMgr.getLoader(ldrName) as BinaryLoader;
                            var loadedSource:Dynamic = loader.getJSON();
                            clipData.fromDynamic(loadedSource, _device);
                            loader.unload();
                        }
                    );
                }
            }
            return _clipDatas[name];
        }

        public function getClipDataNames():Array<String> {
            var names:Array<String> = new Array<String>();
            for (var name:String in _clipDataConfig) {
                names.push(name);
            }
            return names;
        }

    }

}
