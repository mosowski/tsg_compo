package ai;

import map.MapTile;
import object.Character;
import spell.Spell;
import spell.SpellTarget;

class AI {
	public var actionPoints:Int;
	public var character:Character;

	public function new(character:Character = null) {
		if (character != null) {
			setCharacter(character);
		}
		actionPoints = 1;
	}

	public function beginTurn():Void {
		if (character != null) {
            Layers.showMap(character.tile.worldMap);
			Layers.spellbar.setCharacter(null);
			Layers.lookAtCharacter(character);
			actionPoints = character.actionPoints;
			Timers.animation.add(think);
		}
	}

	public function think(_:Dynamic):Void {
	}

	public function setCharacter(v:Character):Void {
		if (v != character) {
            if (null != character) {
                character.died.remove(onCharacterDeath);
            }
			character = v;
			if (character != null) {
				character.aiOwner = this;
            	character.died.add(onCharacterDeath);
			}
		}
	}

    public function onCharacterDeath(_:Dynamic) {
		setCharacter(null);
		trace("Umiera AI");
		if (State.activeAI == this) {
			trace("end turn, guy is dead");
        	State.endAITurn();
		} else {
			trace("Remove immediately");
			State.removeAI(this);
		}
    }

	public function performSpell(spell:Spell, tile:MapTile):Void {
		if (character != null) {
			var cost:Int = spell.cost;
			if (Spellcaster.perform(spell, new SpellTarget(character, tile))) {
				if (cost >= 0) {
					actionPoints -= cost;
					if (actionPoints <= 0) {
						Timers.animation.remove(think);
						if (State.activeAI != this) {
							trace("To sie nie dzieje");
						}
                        State.endAITurn();
					}
				}
			}
		}
	}

	public function skipTurn():Void {
		actionPoints = 0;
		Timers.animation.remove(think);
		State.endAITurn();
	}
}
