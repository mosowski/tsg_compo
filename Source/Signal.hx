package;

class Signal {
    public var _listeners:Array<SignalListener>;

    public function new() {
		_listeners = new Array<SignalListener>();
    }

    public function add(func:Dynamic->Void):Void {
        addExpiring(func, -1);
    }

    public function addOnce(func:Dynamic->Void):Void {
        addExpiring(func, 1);
    }

    public function addExpiring(func:Dynamic->Void, life:Int):Void {
        var sl:SignalListener = new SignalListener();
        sl._func = func;
        sl._life = life;
		remove(func);
		for (i in 0..._listeners.length) {
			if (_listeners[i]._func == func) {
				trace("LISTENER ZDUPLIKOWANY");
			}
		}
        _listeners.push(sl);
    }

    public function remove(func:Dynamic->Void):Void {
		for (i in 0..._listeners.length) {
            if (_listeners[i]._func == func) {
                _listeners.splice(i, 1);
				break;
            }
        }
    }

    public function clean():Void {
        _listeners.splice(0,_listeners.length);
    }

	public function cleanExpiring():Void {
		var i:Int = _listeners.length - 1;
		while (i >= 0) {
			if (_listeners[i]._life >= 0) {
				_listeners.splice(i, 1);
			}
			--i;
		}
	}

    public function post(arg:Dynamic = null):Void {
		var i:Int = 0;
		var lastListener:SignalListener;
		while (i < _listeners.length) {
			lastListener = _listeners[i];
            _listeners[i]._func(arg);
			if (i < _listeners.length && _listeners[i] == lastListener) {
				++i;
			}
        }

		i = _listeners.length - 1;
		while (i >= 0) {
            if (_listeners[i]._life > 0) {
                if (--_listeners[i]._life == 0) {
                    _listeners.splice(i, 1);
                }
            }
			--i;
        }
    }
}


class SignalListener {
	public var _func:Dynamic->Void;
	public var _life:Int;

	public function new() {
	}
}
