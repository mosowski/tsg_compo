package fabulous;

class SpriteSheet {
    public var _device:Device;
    public var _name:String;
    public var _frames:Map<String,SpriteSheetFrame>;

    public function new(device:Device) {
        _device = device;
        _frames = new Map<String,SpriteSheetFrame>();
    }

    public function fromObject(object:Dynamic):Void {
        for (spriteId in Reflect.fields(object)) {
            var src:Dynamic = Reflect.field(object,spriteId);
            _frames[spriteId] = new SpriteSheetFrame(src.left, src.top, src.right, src.bottom, src.width, src.height, src.pivotX, src.pivotY, src.texture);
        }
    }
	
	public function hasFrame(spriteId:String):Bool {
		return _frames.exists(spriteId);
	}

    public function getFrame(spriteId:String):SpriteSheetFrame {
        var frame:SpriteSheetFrame = _frames[spriteId];
        if (frame != null) {
			if (frame._texture == null) {
            	frame._texture = _device._textureMgr.loadTexture(frame._textureId);
			}
            return frame;
        } else {
            throw "Sprite id '" + spriteId + "' not found in sprite sheet '" + _name + "'.";
        }
    }
}
