package fabulous;

import flash.geom.Matrix;
	
class Node {
    public var _x : Float;
    public var _y : Float;
    public var _scaleX : Float;
    public var _scaleY : Float;
    public var _alpha : Float;
    public var _rotation : Float;
	public var _mtx:Matrix;

    public var _derivedIsVisible : Bool;
    public var _derivedAlpha : Float;
	public var _derivedMtx:Matrix;

    public var _hasTransformationChanged:Bool;
    public var _isVisible:Bool;
    public var _hitBitMask:Int;

    public var _parent:Node;
    public var _renderables:Array<Renderable>;
    public var _children:Array<Node>;

    public var _isSortingEnabled : Bool;
    public var _sortingWeight : Float;
	public var _isClippingChildren:Bool;

    public var _touchBegan:Signal;
    public var _touchMoved:Signal;
    public var _touchEnded:Signal;
    public var _tapped:Signal;
    public var _swipeBegan:Signal;
    public var _swipeMoved:Signal;
    public var _swipeEnded:Signal;

	public static var HITMASK_ANY:Int = 1;
	public static var HITMASK_TOUCH:Int = 2;
	public static var HITMASK_SWIPE_X:Int = 4;
	public static var HITMASK_SWIPE_Y:Int = 8;

    public function new() {
        _x = 0;
        _y = 0;
        _scaleX = 1;
        _scaleY = 1;
        _alpha = 1;
        _rotation = 0;
        _derivedAlpha = 1;
        _derivedIsVisible = true;
		_mtx = new Matrix();
		_derivedMtx = new Matrix();
        _hasTransformationChanged = true;
        _isVisible = true;
        _isSortingEnabled = false;
		_isClippingChildren = true;
		_hitBitMask = 1;

		_renderables = new Array<Renderable>();
		_children = new Array<Node>();
    }

	public function updateIfNeeded():Void {
		if (_hasTransformationChanged) {
			updateTransformation();
		} else {
			for (i in 0..._children.length) {
				_children[i].updateIfNeeded();
			}
		}
	}

    public function updateTransformation():Void {
        if (_parent != null) {
            if (_rotation == 0) {
				_mtx.identity();
				_mtx.scale(_scaleX, _scaleY);
				_mtx.translate(_x, _y);
            } else {
				_mtx.identity();
				_mtx.scale(_scaleX, _scaleY);
				_mtx.rotate(_rotation);
				_mtx.translate(_x, _y);
            }
			_derivedMtx.copyFrom(_mtx);
			_derivedMtx.concat(_parent._derivedMtx);

            _derivedAlpha = _parent._derivedAlpha * _alpha;
            _derivedIsVisible = _parent._derivedIsVisible && _isVisible;

            if (_parent._isSortingEnabled) {
                _sortingWeight = _y * 10000 + _x;
            }
        } else {
			_mtx.identity();
			_mtx.scale(_scaleX, _scaleY);
			_mtx.rotate(_rotation);
			_mtx.translate(_x, _y);

			_derivedMtx.copyFrom(_mtx);
        }
        _hasTransformationChanged = false;

		var sortingNeeded:Bool = false;
		for (i in 0..._children.length) {
            sortingNeeded = sortingNeeded || _children[i]._hasTransformationChanged;
            _children[i].updateTransformation();
        }

        if (_isSortingEnabled && sortingNeeded) {
            var v:Float, j:Int, child:Node;
			var numSwaps:Int = 0;
			for (i in 0..._children.length) {
				child = _children[i];
                v = child._sortingWeight;
				j = i;
                while (j > 0 && _children[j-1]._sortingWeight > v) {
					_children[j] = _children[j-1];
					numSwaps++;
					--j;
                }
				_children[j] = child;
            }
        }
    }

    public function addChild(child:Node):Void {
        if (child._parent != null) {
			detachFromParent();
        }
        _children.push(child);
        child._parent = this;
        child.updateTransformation();
		child._hasTransformationChanged = child._hasTransformationChanged || _isSortingEnabled;
    }

    public function addChildAt(child:Node, index:Int):Void {
        if (child._parent != null) {
			detachFromParent();
        }
        _children.insert(index, child);
        child.updateTransformation();
        child._hasTransformationChanged = true;
        child._parent = this;
    }

    public function removeChild(child:Node):Void {
		var index:Int = Lambda.indexOf(_children, child);
		if (index >= 0) {
			_children.splice(index, 1);
			child._parent = null;
		}
    }

    public function removeChildren():Void {
		for (i in 0..._children.length) {
			_children[i]._parent = null;
		}
		_children.splice(0, _children.length);
    }

    public function getChild(index:Int):Node {
		return _children[index];
    }

	public function detachFromParent():Void {
		if (_parent != null) {
			_parent.removeChild(this);
		}
	}

    /**
     * Adds renderable to the end of node's renderables list.
     * @param	renderable
     */
    public function addRenderable(renderable:Renderable):Void {
        if (renderable._node != null) {
            throw "Renderable already attached to node.";
        }
		_renderables.push(renderable);
        renderable._node = this;
    }

    /**
     * Adds renderable at index position. If index is greater than current
     * number of renderables, list is filled with preceeding nulls.
     * @param	renderable
     * @param	index
     */
    public function addRenderableAt(renderable:Renderable, index:Int):Void {
        if (renderable._node != null) {
            throw "Renderable already attached to node.";
        }
		while (_renderables.length < index) {
			_renderables.push(null);
		}
		_renderables.insert(index, renderable);
        renderable._node = this;
    }

    /**
     * Removes renderable from list if found.
     * @param	renderable
     */
    public function removeRenderable(renderable:Renderable):Void {
        var index:Int = Lambda.indexOf(_renderables, renderable);
        if (index >= 0) {
            _renderables[index]._node = null;
            _renderables.splice(index, 1);
        }
    }

    /**
     * Returns renderable at specified index. It may return null if at
     * index preceeding null is found.
     * @param	index
     * @return
     */
    public function getRenderable(index:Int):Renderable {
        return _renderables[index];
    }

    /**
     * Removes all renderables from Node
     */
    public function removeRenderables():Void {
		for (i in 0..._renderables.length) {
			if (_renderables[i] != null) {
				_renderables[i]._node = null;
			}
		}
		_renderables.splice(0, _renderables.length);
	}

    public function getHitNode(x:Float, y:Float, mask:Int = 1):Node {
		var i:Int;
		var hitNode:Node = null;
		if (_derivedIsVisible) {
			if (_children != null) {
				i = _children.length -1;
				while (i >= 0) {
					hitNode = _children[i].getHitNode(x, y, mask);
					if (hitNode != null) {
						return hitNode;
					}
					--i;
				}
			}

			if (_hitBitMask & mask != 0) {
				var hitmask:Hitmask;
				i = _renderables.length - 1;
				while (i >= 0) {
					if (_renderables[i] != null) {
						hitmask = _renderables[i].hitmask;
						if (hitmask != null) {
							if (hitmask.hitTest(x, y, _derivedMtx)) {
								return this;
							}
						}
					}
					--i;
				}
			}
		}
        return null;
    }

	public var numChildren(get, null):Int;
    public function get_numChildren():Int {
		return _children.length;
    }

	public var parent(get, null):Node;
    public function get_parent():Node {
        return _parent;
    }

    /**
     * Returns number of renderables. Preceeding nulls are counted too.
     */
	public var numRenderables(get, null):Int;
    public function get_numRenderables():Int {
		return _renderables.length;
    }
    
	public var x(get,set):Float;
    public function set_x(v:Float):Float {
        _hasTransformationChanged = true;
        return _x = v;
    }
	public function get_x():Float {
		return _x;
	}

	public var y(get,set):Float;
    public function set_y(v:Float):Float {
        _hasTransformationChanged = true;
        return _y = v;
    }
	public function get_y():Float {
		return _y;
	}

	public var scaleX(get,set):Float;
    public function set_scaleX(v:Float):Float {
        _hasTransformationChanged = true;
        return _scaleX = v;
    }
	public function get_scaleX():Float {
		return _scaleX;
	}

	public var scaleY(get,set):Float;
    public function set_scaleY(v:Float):Float {
        _hasTransformationChanged = true;
        return _scaleY = v;
    }
	public function get_scaleY():Float {
		return _scaleY;
	}

	public var alpha(get,set):Float;
    public function set_alpha(v:Float):Float {
        _hasTransformationChanged = true;
        return _alpha = v;
    }
	public function get_alpha():Float {
		return _alpha;
	}

	public var rotation(get,set):Float;
    public function set_rotation(v:Float):Float {
        _hasTransformationChanged = true;
        return _rotation = v;
    }
	public function get_rotation():Float {
		return _rotation;
	}
    

	public var isVisible(get,set):Bool;
    public function set_isVisible(v:Bool):Bool {
        _hasTransformationChanged = true;
        return _isVisible = v;
    }
	public function get_isVisible():Bool {
		return _isVisible;
	}

    /**
     * Enables receiving touch events by the node.
     */
	public var isTouchEnabled(get,set):Bool;
    public function set_isTouchEnabled(v:Bool):Bool {
		if (v) {
			_hitBitMask |= HITMASK_TOUCH;
		} else {
			_hitBitMask &= ~HITMASK_TOUCH;
		}
		return v;
    }
	public function get_isTouchEnabled():Bool {
		return _hitBitMask & HITMASK_TOUCH != 0;
	}

    /**
     * Enables receiving swipe signals by the node.
     */
	public var isSwipeXEnabled(get,set):Bool;
    public function set_isSwipeXEnabled(v:Bool):Bool {
		if (v) {
			_hitBitMask |= HITMASK_SWIPE_X;
		} else {
			_hitBitMask &= ~HITMASK_SWIPE_X;
		}
		return v;
    }
	public function get_isSwipeXEnabled():Bool {
		return _hitBitMask & HITMASK_SWIPE_X != 0;
	}

    /**
     * Enables receiving swipe signals by the node.
     */
	public var isSwipeYEnabled(get,set):Bool;
    public function set_isSwipeYEnabled(v:Bool):Bool {
		if (v) {
			_hitBitMask |= HITMASK_SWIPE_Y;
		} else {
			_hitBitMask &= ~HITMASK_SWIPE_Y;
		}
		return v;
    }
	public function get_isSwipeYEnabled():Bool {
		return _hitBitMask & HITMASK_SWIPE_Y != 0;
	}

    /**
     * Enables sorting on Y (X if Y's are equal)
     */
	public var isSortingEnabled(get,set):Bool;
    public function set_isSortingEnabled(v:Bool):Bool {
        if (v != _isSortingEnabled) {
            _isSortingEnabled = v;
            _hasTransformationChanged = true;
        }
		return v;
    }
	public function get_isSortingEnabled():Bool {
		return _isSortingEnabled;
	}

	public var isClippingChildren(get,set):Bool;
	public function set_isClippingChildren(v:Bool):Bool {
		return _isClippingChildren = v;
	}
	public function get_isClippingChildren():Bool {
		return _isClippingChildren;
	}
    
	public var touchBegan(get, null):Signal;
    public function get_touchBegan():Signal {
		if (_touchBegan == null) {
			_touchBegan = new Signal();
		}
        return _touchBegan;
    }

	public var touchEnded(get, null):Signal;
    public function get_touchEnded():Signal {
		if (_touchEnded == null) {
			_touchEnded = new Signal();
		}
        return _touchEnded;
    }

	public var touchMoved(get, null):Signal;
    public function get_touchMoved():Signal {
		if (_touchMoved == null) {
			_touchMoved = new Signal();
		}
        return _touchMoved;
    }

	public var tapped(get, null):Signal;
    public function get_tapped():Signal {
		if (_tapped == null) {
			_tapped = new Signal();
		}
        return _tapped;
    }

	public var swipeBegan(get, null):Signal;
    public function get_swipeBegan():Signal {
		if (_swipeBegan == null) {
			_swipeBegan = new Signal();
		}
        return _swipeBegan;
    }

	public var swipeEnded(get, null):Signal;
    public function get_swipeEnded():Signal {
		if (_swipeEnded == null) {
			_swipeEnded = new Signal();
		}
        return _swipeEnded;
    }

	public var swipeMoved(get, null):Signal;
    public function get_swipeMoved():Signal {
		if (_swipeMoved == null) {
			_swipeMoved = new Signal();
		}
        return _swipeMoved;
    }
}
