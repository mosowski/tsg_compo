package fabulous {
	import flash.display.Sprite;
	
    class DebugRenderer extends Renderer {
        public var _sprite:flash.display.Sprite;

        public function DebugRenderer(device:Device) {
            super(device);

            _sprite = new flash.display.Sprite();
            _sprite.mouseEnabled = false;
            _sprite.mouseChildren = false;
            device._stage.addChild(_sprite);
        }

        override public function beginFrame():Void {
            super.beginFrame();
            _sprite.removeChildren();
        }

        override public function endFrame():Void {
            super.endFrame();
        }

        override public function processRenderable(renderable:Renderable):Void {
            var debugSprite:DebugSprite = renderable as DebugSprite;
            debugSprite._sprite.x = debugSprite._node._derivedX;
            debugSprite._sprite.y = debugSprite._node._derivedY;
            debugSprite._sprite.scaleX = debugSprite._node._derivedScaleX;
            debugSprite._sprite.scaleY = debugSprite._node._derivedScaleY;
            debugSprite._sprite.alpha = debugSprite._node._derivedAlpha;
            _sprite.addChild(debugSprite._sprite);
            debugSprite.updateBounds();
        }

    }

}
