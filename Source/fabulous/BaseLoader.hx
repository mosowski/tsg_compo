package fabulous {
    import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.utils.ByteArray;
	
    class BaseLoader {
        public static var loaderQueue:LoaderQueue;

        public var _completed:Signal;
        public var _crashed:Signal;

        public var _device:Device;
        public var _name:String;
        public var _url:String;

        public function BaseLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null) {
            _device = device;
            _name = name;
            _url = url;		
            _completed = new Signal();
            _crashed = new Signal();
            if (onComplete != null) {
                _completed.addOnce(onComplete);
            }
        }

        public function get name():String {
            return _name;
        }

        public function load():Void {
        }

        public function queue():Void {
            _device._loaderMgr._loaderQueue.add(this);
        }

        protected function onLoaderError(e:IOErrorEvent):Void {
            trace ("Loader \"" + _name + "\" for url " + _url + " failed with IO error " + e.errorID);
            _crashed.post(_name);
        }

        protected function onLoaderComplete(e:Event):Void {
            _completed.post(_name);
        }

        public function getByteArray():ByteArray {
            throw "Not implemented";
        }

        public function getCompressedByteArray():ByteArray {
            throw "Not implemented";
        }

        public function getString():String {
            throw "Not implemented";
        }

        public function getJSON():Dynamic {
            throw "Not implemented";
        }

        public function getAMF3():Dynamic {
            throw "Not implemented";
        }

        public function getCompressedAMF3():Dynamic {
            throw "Not implemented";
        }

        public function getBitmap():Bitmap {
            throw "Not implemented";
        }

        public function unload():Void {
            throw "Not implemented";
        }

    }

}
