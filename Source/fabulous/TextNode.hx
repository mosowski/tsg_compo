package fabulous {
    class TextNode extends Node {
        public var _text:Text;

        public function TextNode(material:Material) {
            _text = new Text(material);
            _text.widthLimit = 1;
            _text.heightLimit = 1;
            _text.wordWrap = true;
            _text.downscaleToLimits = false;
            _text.upscaleToLimits = false;
            _text.fontSize = 10;
            addRenderable(_text);
        }

        
        public function set font(value:Font):Void {
            _text.font = value;
        }

        
        public function set fontSize(value:Float):Void {
            _text.fontSize = value;
        }

        public function set downscaleToLimits(value:Bool):Void {
            _text.downscaleToLimits = value;
        }
		
		public function set upscaleToLimits(value:Bool):Void {
            _text.upscaleToLimits = value;
        }

        public function set widthLimit(value:Float):Void {
            _text.widthLimit = value;
        }

        public function set heightLimit(value:Float):Void {
            _text.heightLimit = value;
        }

        public function set horizontalAlignment(value:Int):Void {
            _text.horizontalAlignment = value;
        }
		
		public function set verticalAlignment(value:Int):Void {
            _text.verticalAlignment = value;
        }

		public function get wordWrap():Bool{
			return _text.wordWrap;
		}
		public function set wordWrap(v:Bool):Void {
			_text.wordWrap = v;
		}

		public function get screenSize():Float {
			return _text.screenSize;
		}

        public function set text(value:String):Void {
            _text.text = value;
        }




    }

}
