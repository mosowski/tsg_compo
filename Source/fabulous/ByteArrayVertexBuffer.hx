package fabulous;

import flash.utils.ByteArray;
import flash.utils.Endian;

class ByteArrayVertexBuffer extends VertexBuffer {

    public var _data:Float32Array;

    public function ByteArrayVertexBuffer(device:Device) {
        super(device);
    }

    override public function setSizeAndFormat(numVertices:Int, format:Array):Void {
        super.setSizeAndFormat(numVertices, format);
		_data = new Float32Array(_numVertices * _data32PerVertex);
        _changeRangeEnd = numVertices;
        upload();
    }

    
    override public function upload():Void {
        if (_changeRangeBegin < _changeRangeEnd) {
			GL.bufferData(GL.ARRAY_BUFFER, _data, GL.DYNAMIC_DRAW);
            _changeRangeBegin = _numVertices;
            _changeRangeEnd = 0;
        }
    }

	/** Call it before writing to the *consecutive* vertices in the buffer, but 
	  * just after setting the right position. */
    override public function beginWrite():Void {
		super.beginWrite();
    }

    override public function endWrite():Void {
		super.endWrite();
    }

    
    override public function writeFloat(value:Float):Void {
		_data[_position] = value;
    }

    
    public function writeUnsigned:Int(value:Int):Void {
		si32(value, _position*4);
        ++_position;
    }

    
    override public function set position(value:Int):Void {
        _position = value;
        _data.position = _position * 4;
    }

    
    override public function get position():Int {
        return _position;
    }
}
