package fabulous;

import openfl.Assets;
	
class FontManager {
	public var _device:Device;
    public var _fonts:Map<String,Font>;
    public var _fontsConfig:Map<String,String>;

    public var _debugFont:Font;

    public function new(device:Device) {
		_device = device;
        _fonts = new Map<String,Font>();
    }

    public function init():Void {
        _device._textureMgr.createTextureFromBitmapData("__debugFontBitmap__", DebugFont.getDebugFontBitmapData());
        _debugFont = new Font(_device);
        _debugFont.fromObject(haxe.Json.parse(DebugFont.debugFontAsset));
    }

    public function loadFontsConfigFromObject(object:Dynamic):Void {
        _fontsConfig = new Map<String,String>();
		var objectIterator:Map<String,Map<String,String>> = object;
        for (fontName in objectIterator.keys()) {
			Log.i("Registered font " + fontName);
            _fontsConfig[fontName] = objectIterator[fontName][_device._deviceProfile];
        }
		Log.i("Font Manager config loaded.");
    }

    public function getFont(name:String):Font {
        var font:Font = _fonts[name];
        if (font == null) {
            if (_fontsConfig[name] == null) {
                Log.e("No font config for name '" + name + "'.");
				throw "FabulousError";
            } else {
                font = _fonts[name] = new Font(_device);
                var path:String = _device.getPath(_fontsConfig[name]);
				var json:String = Assets.getText(path);
				var source:Dynamic = haxe.Json.parse(json);
				font.fromObject(source);
            }
        }
        return font;
    }
}
