package fabulous {
    import flash.display3D.Context3DBlendFactor;
    import flash.display3D.Context3DProgramType;
    import flash.utils.ByteArray;
    import flash.utils.Endian;

    class MaterialDistanceFieldOutline extends Material {
        public static var _shader:Shader;

        public var _color:ByteArray;
        public var _outlineColor:ByteArray;
		public var _dfParams:ByteArray;

		public var _smoothing:Float;
		public var _threshold:Float;
		public var _outlineSize:Float;
		public var _outlineOffsetX:Float;
		public var _outlineOffsetY:Float;

        public function MaterialDistanceFieldOutline() {
			_smoothing = 0.1;
			_threshold = 0.5;
			_outlineSize = 0.2;
			_outlineOffsetX = 0;
			_outlineOffsetY = 0;

            _color = new ByteArray();
            _color.endian = Endian.LITTLE_ENDIAN;
            _color.writeFloat(1);
            _color.writeFloat(1);
            _color.writeFloat(1);
            _color.writeFloat(1);

            _outlineColor = new ByteArray();
            _outlineColor.endian = Endian.LITTLE_ENDIAN;
            _outlineColor.writeFloat(1);
            _outlineColor.writeFloat(1);
            _outlineColor.writeFloat(1);
            _outlineColor.writeFloat(1);

			_dfParams = new ByteArray();
			_dfParams.endian = Endian.LITTLE_ENDIAN;
			updateParams();
        }

        public static function initShader(device:Device):Void {
            _shader = new Shader(device);
            _shader.setCode(
                [
                    // va0.xy: vertex position
                    // va1.xy: vertex uv
                    // va2.x: vertex alpha
                    // vc0.xy: screen scale
                    // vc0.zw: screen translation
                    "mov vt0, va0",
                    "mul vt0.xy, vt0.xy, vc0.xy",
                    "add vt0.xy, vt0.xy, vc0.zw",
                    "mov op, vt0",
                    "mov v0, va1",
                    "mov v1, va2"
                ].join("\n"),
                [
                    // v0: vertex uv
                    // v1: vertex alpha
                    // fs0: texture
                    // fc0: color to multiply by texture color
					// fc1: outline color
					// fc2.xyzw: glyph threshold, inv smoothing range, outline threshold, 1
					// fc3.xy: outline offset
					"add ft1, v0, fc3",
                    "tex ft0, ft1, fs0 <sampler>",
					"sub ft1.x, ft0.x, fc2.z",
					"mul ft1.x, ft1.x, fc2.y",
					"sat ft1.x, ft1.x",
					"mul ft2.xyzw, fc1.xyzw, ft1.xxxx",

					"tex ft0, v0, fs0 <sampler>",
					"sub ft1.x, ft0.x, fc2.x",
					"mul ft1.x, ft1.x, fc2.y",
					"sat ft1.x, ft1.x",
					"mul ft0.xyzw, fc0.xyzw, ft1.xxxx",

					"sub ft1.xyzw, fc2.wwww, ft0.wwww",
					"mul ft2, ft2, ft1",
					"add ft0, ft0, ft2",

                    "mov oc, ft0"
                ].join("\n")
            );
        }

		public function set color(c:Int):Void {
			_color.position = 0;
			_color.writeFloat(((c >> 16) & 255) / 255);
			_color.writeFloat(((c >> 8) & 255) / 255);
			_color.writeFloat((c & 255) / 255);
			_color.writeFloat(((c >> 24) & 255) / 255);
		}

		public function set outlineColor(c:Int):Void {
			_outlineColor.position = 0;
			_outlineColor.writeFloat(((c >> 16) & 255) / 255);
			_outlineColor.writeFloat(((c >> 8) & 255) / 255);
			_outlineColor.writeFloat((c & 255) / 255);
			_outlineColor.writeFloat(((c >> 24) & 255) / 255);
		}

		public function set threshold(v:Float):Void {
			if (_threshold != v) {
				_threshold = v;
				updateParams();
			}
		}

		public function set smoothing(v:Float):Void {
			if (_smoothing != v) {
				_smoothing = v;
				updateParams();
			}
		}

		public function set outlineSize(v:Float):Void {
			if (_outlineSize != v) {
				_outlineSize = v;
				updateParams();
			}
		}

		public function set outlineOffsetX(v:Float):Void {
			if (_outlineOffsetX != v) {
				_outlineOffsetX = v;
				updateParams();
			}
		}

		public function set outlineOffsetY(v:Float):Void {
			if (_outlineOffsetY != v) {
				_outlineOffsetY = v;
				updateParams();
			}
		}

		private function updateParams():Void {
			_dfParams.position = 0;
			_dfParams.writeFloat(_threshold - _smoothing/2);
			_dfParams.writeFloat(1/_smoothing);
			_dfParams.writeFloat(_threshold - _outlineSize);
			_dfParams.writeFloat(1);

			_dfParams.writeFloat(_outlineOffsetX);
			_dfParams.writeFloat(_outlineOffsetY);
			_dfParams.writeFloat(0);
			_dfParams.writeFloat(0);
		}


        
        override public function bind(device:Device, batch:RenderBatch):Void {
            device.setTextureAt(0, batch._textureResource);
            device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
            device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);

            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _color, 0);
            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 1, 1, _outlineColor, 0);
            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 2, 2, _dfParams, 0);
        }
    }

}
