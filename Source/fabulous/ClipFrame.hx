package fabulous {
	
    class ClipFrame {
        public var _texture:TextureResource;
        public var _vertices:Array<Float>;
        public var _indices:Array<int>;

        public function ClipFrame() {
        }

        public function fromDynamic(object:Dynamic, device:Device):Void {
            _vertices = Array<Float>(object.vertices);
			if ("indices" in object) {
            	_indices = Array<int>(object.indices);
			} else {
				_indices = new Array<int>();
				for (var i:Int = 0; i < _vertices.length; i+=4) {
					_indices.push(i, i + 1, i + 2, i + 1, i + 2, i +3);
				}
			}
            _texture = device._textureMgr.loadTexture(object.texture);
        }

		public function getX(i:Int):Float {
			return _vertices[i*5];
		}
		public function getY(i:Int):Float {
			return _vertices[i*5+1];
		}

		public function get numVertices():Int {
			return _vertices.length/5;
		}


    }

}
