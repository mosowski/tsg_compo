package fabulous;



class Tweener {
    public var _tweenListHead:TweenItem;
    public var _tweenListLast:TweenItem;
    public var _time:Float;
    public var _currentId:Int;

    public function new() {
        _tweenListHead = new TweenItem();
        _tweenListLast = _tweenListHead;
        _time = 0;
        _currentId = 0;
    }

    public function tween(object:Dynamic, from:Dynamic, to:Dynamic, duration:Float, ease:Float->Float, onComplete:Void->Void = null):TweenItem {
        var item:TweenItem = getTweenItem();
        item._active = true;
        item._id = _currentId++;
        item._object = object;
        item._props.splice(0, item._props.length);
        item._from = from;
        item._diff = to;
        item._startTime = _time;
        item._duration = duration;
        item._ease = ease;
        item._onComplete = onComplete;

        for (prop in Reflect.fields(from)) {
            item._props[item._props.length] = prop;
        }
        var i:Int = 0;
        for (i in 0...item._props.length) {
            Reflect.setField(item._diff, item._props[i] , Reflect.field(item._diff, item._props[i]) - Reflect.field(from, item._props[i]));
            Reflect.setField(object, item._props[i], Reflect.field(from, item._props[i]));
        }

        return item;
    }

    public function tick():Void {
        _time = flash.Lib.getTimer() / 1000;
        var q:Float;
        var i:Int;
        var prop:String;
        var ease:Float;
        var item:TweenItem = _tweenListHead;
        while (item != null) {
            if (item._active) {
                q = (_time - item._startTime) / item._duration;
                if (q >= 1.0) {
                    for (i in 0...item._props.length) {
                        Reflect.setField(item._object, item._props[i], Reflect.field(item._from, item._props[i]) + Reflect.field(item._diff, item._props[i]));
                    }

                    if (item._onComplete != null) {
                        item._onComplete();
                        item._onComplete = null;
                    }
                    item._active = false;
                    item._object = null;
                    item._ease = null;
                } else {
                    ease = item._ease(q);
                    for (i in 0...item._props.length) {
                        Reflect.setField(item._object, item._props[i], Reflect.field(item._from, item._props[i]) + Reflect.field(item._diff, item._props[i]) * ease);
                    }
                }
            }
            item = item._next;
        }
    }

    public function getTweenItem():TweenItem {
        var item:TweenItem = _tweenListHead;
        while (item != null && item._active == true) {
            item = item._next;
        }
        if (item != null) {
            item = new TweenItem();
            _tweenListLast._next = item;
            _tweenListLast = item;
        }
        return item;
    }

    public function linearEase(q:Float):Float {
        return q;
    }

    public function quadraticEase(q:Float):Float {
        return q*q;
    }

    public function sqrtEase(q:Float):Float {
        return Math.sqrt(q);
    }

    public function sineEase(q:Float):Float {
        return Math.sin(q * Math.PI * 0.5);
    }

    public function mediumBounceEase(q:Float):Float {
        return Math.sin(q * Math.PI * 0.686403) * 1.2;
    }


}

