package fabulous {
    import flash.display3D.Context3DBlendFactor;
    import flash.display3D.Context3DProgramType;
	import flash.geom.Matrix3D;
	import flash.geom.Vector3D;
    import flash.utils.ByteArray;
    import flash.utils.Endian;

    class MaterialHSL extends Material {
        public static var _shader:Shader;

		public var _saturation:Float;
        public var _saturationParams:ByteArray;

		public var _hueRotation:Float;
		public var _hueRotationMtx:Matrix3D;
		public var _oneVector:Vector3D

        public function MaterialHSL() {
			_saturation = 1;

			_hueRotation = 0;
			_hueRotationMtx = new Matrix3D();
			_oneVector = new Vector3D(1,1,1);

            _saturationParams = new ByteArray();
            _saturationParams.endian = Endian.LITTLE_ENDIAN;
            _saturationParams.writeFloat(0.3);
            _saturationParams.writeFloat(0.59);
            _saturationParams.writeFloat(0.11);
            _saturationParams.writeFloat(1);
        }

        public static function initShader(device:Device):Void {
            _shader = new Shader(device);
            _shader.setCode(
                [
                    // va0.xy: vertex position
                    // va1.xy: vertex uv
                    // va2.x: vertex alpha
                    // vc0.xy: screen scale
                    // vc0.zw: screen translation
                    "mov vt0, va0",
                    "mul vt0.xy, vt0.xy, vc0.xy",
                    "add vt0.xy, vt0.xy, vc0.zw",
                    "mov op, vt0",
                    "mov v0, va1",
                    "mov v1, va2"
                ].join("\n"),
                [
                    // v0: vertex uv
                    // v1: vertex alpha
                    // fs0: texture
                    // fc0.xyz: desaturation consts
					// fc0.w: saturation weight
					// fc1,fc2,fc3,fc4: hue rotation mtx
                    "tex ft0, v0, fs0 <sampler>",

					"dp3 ft1, ft0.xyz, fc0.xyz",
					"sub ft0.xyz, ft0.xyz, ft1.xxx",
					"mul ft0.xyz, ft0.xyz, fc0.www",
					"add ft0.xyz, ft0.xyz, ft1.xxx",

					"m44 ft0, ft0, fc1",
					"mov oc, ft0"
                ].join("\n")
            );
        }

        
        public function set saturation(v:Float):Void {
			if (_saturation != v) {
				_saturation = v;
            	_saturationParams.position = 12;
            	_saturationParams.writeFloat(_saturation);
			}
        }
		public function get saturation():Float {
			return _saturation;
		}


		public function set hueRotation(v:Float):Void {
			if (_hueRotation != v) {
				_hueRotation = v;
				_hueRotationMtx.identity();
				_hueRotationMtx.appendRotation(_hueRotation, _oneVector);
			}
		}
		public function get hueRotation():Float {
			return _hueRotation;
		}

        
        override public function bind(device:Device, batch:RenderBatch):Void {
            device.setTextureAt(0, batch._textureResource);
            device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
            device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);
            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _saturationParams, 0);
            device._ctx.setProgramConstantsFromMatrix(Context3DProgramType.FRAGMENT, 1, _hueRotationMtx, true);
        }
    }

}
