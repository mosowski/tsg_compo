package fabulous {

    class ImageNode extends Node {
        public var _image:Image;

        public function ImageNode(material:Material) {
            _image = new Image(material);
            addRenderable(_image);

        }

        
        public function setShapeRect(x:Float, y:Float, width:Float, height:Float):Void {
            _image.setShapeRect(x, y, width, height);
        }

        
        public function setUVRect(x:Float, y:Float, width:Float, height:Float):Void {
            _image.setUVRect(x, y, width, height);
        }
		
		
		public function setVertices(x00:Float,y00:Float,x01:Float, y01:Float, x11:Float,y11:Float,x10:Float, y10:Float):Void {
			_image.setVertices(x00,y00,x01,y01,x11,y11,x10,y10);
		}

        
        public function set textureResource(value:TextureResource):Void {
            _image.textureResource = value;
        }

        
        public function get textureResource():TextureResource {
            return _image.textureResource;
        }

        
        public function set material(value:Material):Void {
            _image.material = value;
        }

        
        public function get material():Material {
            return _image.material;
        }

        
        public function set u00(value:Float):Void {
            _image._u00 = value;
        }

        
        public function set v00(value:Float):Void {
            _image._v00 = value;
        }

        
        public function set u10(value:Float):Void {
            _image._u10 = value;
        }

        
        public function set v10(value:Float):Void {
            _image._v10 = value;
        }

        
        public function set u11(value:Float):Void {
            _image._u11 = value;
        }

        
        public function set v11(value:Float):Void {
            _image._v11 = value;
        }

        
        public function set u01(value:Float):Void {
            _image._u01 = value;
        }

        
        public function set v01(value:Float):Void {
            _image._v01 = value;
        }

		public function set frame(v:SpriteSheetFrame):Void {
			_image.frame = v;
		}


    }

}
