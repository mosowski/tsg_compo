package spell;

class Kamikaze extends Spell {
	public function new() {
		super("icon-fire", -1);
		displayName = "Kamikaze";
		cost = 1;
	}

	override private function canApply(target:SpellTarget):Bool {
		return super.canApply(target) && target.tile.isWalkable();
	}

	override public function apply(target:SpellTarget):Void {
		State.extendAnimationDelay(1);
		State.tweener.tween(this, { }, { }, 1, State.tweener.linearEase, function():Void {
			Spellcaster.curse(new CurseFire(100), new CurseTarget(null, target.tile));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(0,1)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(0,-1)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(1,0)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(-1,0)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(0,2)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(0,-2)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(2,0)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(-2,0)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(1,1)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(1,-1)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(-1,1)));
			Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(-1,-1)));
		});
	}
}
