package spell;
import atlas.AtlasSprite;
import object.Character;
import map.MapTile;
import fabulous.F;

class MagicBullet extends Spell {
    private var target:SpellTarget;
    private var spellFx:AtlasSprite;

	public function new() {
		super("icon-magic-bullet", 7);
		displayName = "Magic bolt";
        cost = 1;
	}

	override private function canApply(target:SpellTarget):Bool {
        return super.canApply(target) && true;
	}

    override public function apply(target:SpellTarget):Void {
        this.target = target;
        var delay:Float = (Math.abs(target.caster.tile.column - target.tile.column) + Math.abs(target.caster.tile.row - target.tile.row)) * 0.07;

        spellFx = new AtlasSprite(Atlases.get("magic-bullet"), "idle");
        Layers.fxLayer.addChild(spellFx);
        if (target.caster.tile.column - target.tile.column < 0) {
            spellFx.scaleY = -1;
        }
        if (target.caster.tile.column - target.tile.column != 0) {
            spellFx.rotation = 1.6;
        }

        if (target.caster.tile.row - target.tile.row > 0) {
            spellFx.scaleY = -1;
        }
		State.extendAnimationDelay(delay);
        State.tweener.tween(spellFx, {x: target.caster.tile.column * 32, y:target.caster.tile.row * 32}, {x:target.tile.column * 32, y:target.tile.row * 32}, delay, State.tweener.linearEase, finish);
    }

    public function finish():Void {
        Layers.fxLayer.removeChild(spellFx);
        spellFx.dispose();
        target.tile.damage();
    }
}
