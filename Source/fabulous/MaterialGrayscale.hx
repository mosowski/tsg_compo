package fabulous {
    import flash.display3D.Context3DBlendFactor;
    import flash.display3D.Context3DProgramType;
    import flash.utils.ByteArray;
    import flash.utils.Endian;

    class MaterialGrayscale extends Material {
        public static var _shader:Shader;

        public var _weights:ByteArray;

        public function MaterialGrayscale() {
            _weights = new ByteArray();
            _weights.endian = Endian.LITTLE_ENDIAN;
            _weights.writeFloat(0.3);
            _weights.writeFloat(0.59);
            _weights.writeFloat(0.11);
            _weights.writeFloat(0);
        }

        public static function initShader(device:Device):Void {
            _shader = new Shader(device);
            _shader.setCode(
                [
                    // va0.xy: vertex position
                    // va1.xy: vertex uv
                    // va2.x: vertex alpha
                    // vc0.xy: screen scale
                    // vc0.zw: screen translation
                    "mov vt0, va0",
                    "mul vt0.xy, vt0.xy, vc0.xy",
                    "add vt0.xy, vt0.xy, vc0.zw",
                    "mov op, vt0",
                    "mov v0, va1",
                    "mov v1, va2"
                ].join("\n"),
                [
                    // v0: vertex uv
                    // v1: vertex alpha
                    // fs0: texture
                    // fc0.xyz: desaturation consts
                    "tex ft0, v0, fs0 <sampler>",
					"dp3 ft0.xyz, ft0.xyz, fc0.xyz",
					"mov oc, ft0"
                ].join("\n")
            );
        }

        
        public function set weight(v:Float):Void {
            _weights.position = 12;
            _weights.writeFloat(v);
        }

        
        override public function bind(device:Device, batch:RenderBatch):Void {
            device.setTextureAt(0, batch._textureResource);
            device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
            device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);
            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _weights, 0);
        }
    }

}
