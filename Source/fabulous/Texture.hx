package fabulous;

import openfl.gl.GLTexture;
import openfl.gl.GL;

class Texture {
	public var _name:String;
    public var _glTexture:GLTexture;
    public var _isLoaded:Bool;
    public var _isLoading:Bool;
    public var _isCompressed:Bool;
    public var _hasAlpha:Bool;
    public var _width:Int;
    public var _height:Int;
	public var _sourceWidth:Int;
	public var _sourceHeight:Int;

    public var _loaded:Signal;

    public function new() {
        _isLoaded = false;
        _isLoading = false;
        _isCompressed = false;
        _hasAlpha = true;
        _loaded = new Signal();
    }

    public function dispose():Void {
        if (_glTexture != null && _glTexture != F.device._textureMgr._unloadedFallbackGLTexture) {
			GL.deleteTexture(_glTexture);
            _glTexture = null;
            F.device._textureMgr.initAsUnloaded(this);
            _isLoaded = false;
        }
    }

    public function callAfterLoad(callback:Dynamic->Void):Void {
        if (_isLoaded) {
            callback(null);
        } else {
            _loaded.addOnce(callback);
        }
    }

	public var sourceWidth(get,null):Int;
	public function get_sourceWidth():Int {
		return _sourceWidth;
	}

	public var sourceHeight(get,null):Int;
	public function get_sourceHeight():Int {
		return _sourceHeight;
	}

}
