package player;

import map.MapTile;
import object.Character;
import spell.Spell;
import spell.SpellTarget;
import hud.InfoText;

class Player {
	public var actionPoints:Int;
	public var activeSpell:Spell;
	public var character:Character;
    public var originalBody:Character;

	public function new() {
		actionPoints = 0;
	}

	public function beginTurn():Void {
		if (character != null) {
            Layers.showMap(character.tile.worldMap);
			Layers.spellbar.setCharacter(character);
			Layers.spellbar.highlightSpell(activeSpell);
			Layers.lookAtCharacter(character);
			actionPoints = 2; // character.numBaseSpells;
		}
	}

	public function setCharacter(v:Character):Void {
		if (v != character) {
            if (null != character) {
                character.died.remove(onCharacterDeath);
            }
            else {
                v.originalOwner = this;
                originalBody = v;
            }
			if (character != null) {
				character.spellsChanged.remove(onSpellsChanged);
			}
			character = v;
			if (character.aiOwner != null) {
				character.aiOwner.setCharacter(null);
				State.removeAI(character.aiOwner);
				character.aiOwner = null;
			}
			if (this == State.activePlayer) {
				Layers.lookAtCharacter(character);
			}
			character.spellsChanged.add(onSpellsChanged);
			Layers.spellbar.setCharacter(character);
            character.died.add(onCharacterDeath);
		}
	}

    public function onCharacterDeath(_:Dynamic) {
        if (character != originalBody) {
            setCharacter(originalBody);
        }
		if (State.activePlayer == this) {
        	State.endTurn();
		}
    }

	private function onSpellsChanged(character:Character):Void {
		if (Lambda.indexOf(character.spells, activeSpell) < 0) {
			setActiveSpell(null);
		}
		if (State.activePlayer == this) {
			Layers.spellbar.setCharacter(character);
		}
	}

	public function setActiveSpell(v:Spell):Void {
		activeSpell = v;
		if (State.activePlayer == this && activeSpell != null) {
			Layers.spellbar.highlightSpell(v);
			new InfoText(v.displayName, character.getSpriteX(), character.getSpriteY());
		}
	}

	public function performSpell(tile:MapTile):Void {
		if (character != null && activeSpell != null) {
			var cost:Int = activeSpell.cost;
			if (Spellcaster.perform(activeSpell, new SpellTarget(character, tile))) {
				Layers.spellbar.setCharacter(character);
				if (cost >= 0) {
					actionPoints -= cost;
					if (actionPoints <= 0) {
                        State.endTurn();
					}
				}
			}
		}
	}
}
