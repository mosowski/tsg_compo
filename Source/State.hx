package;

import map.WorldMap;
import map.MapTile;
import map.Tilesets;
import player.Player;
import object.Character;
import spell.Spell;
import fabulous.Tweener;
import ai.AI;
import map.Populator;
import spell.MagicBullet;
import spell.MindControl;
import spell.Firewall;
import spell.Heal;

import openfl.Assets;

class State {

	public static var normalMap:WorldMap;
	public static var hellMap:WorldMap;
	public static var endMap:WorldMap;
	
	public static var players:Array<Player>;
	public static var activePlayer:Player;
	private static var ais:Array<AI>;
	public static var activeAI:AI;

	public static var tileClicked:Signal;
    public static var animationDelay:Float;
    public static var animationDelayZero:Signal;
    public static var tweener:Tweener;

    private static var spellWeights:Array<Int>;
    private static var spells:Array<Void->Spell>;

	public static function init():Void {
        spells = [function ():Spell { return new Firewall();},
                  function ():Spell { return new MagicBullet();},
                  function ():Spell { return new MindControl();},
                  function ():Spell { return new Heal();}];


        spellWeights = [1, 15, 3, 15];
        tweener = new Tweener();
		normalMap = new WorldMap(Tilesets.Normal, "assets/map.json");
        hellMap = new WorldMap(Tilesets.Hell, "assets/map_hell.json");
        endMap = new WorldMap(Tilesets.End, "assets/map_end.json");
		Layers.showMap(normalMap);

		players = new Array<Player>();
		ais = new Array<AI>();

		Populator.populateNormal(50);
		Populator.populateHell(25);

		tileClicked = new Signal();
		tileClicked.add(onTileClicked);
        animationDelay = 0;
        animationDelayZero = new Signal();
		Timers.animation.add(decreaseAnimationDelay);

		Assets.getSound("assets/music.mp3").play(0, 1000);
	}

    public static function getRandomSpell():Spell {
        var rand:Int = Std.int(Math.random() * 32);
        var j:Int = 0;
        var sum:Int = 0;
        for (i in spellWeights) {
            sum += i;
            if (sum >= rand) {
                return spells[j]();
            }
            j++;
        }
        return null;
    }

    public static function decreaseAnimationDelay(_:Int):Void {
		if (animationDelay > 0) {
        	animationDelay -= 0.1;
        	if (animationDelay <= 0) {
				trace("koniec czekania!");
            	animationDelayZero.post();
        	}
		}
    }

    public static function setAnimationDelay(delay:Float):Void {
		trace("poczekam " + delay);
        animationDelay = delay;
    }

	public static function extendAnimationDelay(delay:Float):Void {
		animationDelay = Math.max(animationDelay, delay);
		trace("poczekam max: " + animationDelay);
	}

	public static function createPlayer():Player {
		var player:Player = new Player();
		players.push(player);
		return player;
	}

	public static function addAI(ai:AI):Void {
		ais.push(ai);
	}

	public static function removeAI(ai:AI):Void {
		trace("Usuwam AI ",  Lambda.indexOf(ais,ai));
		ais.splice(Lambda.indexOf(ais,ai), 1);
	}

	public static function beginGame():Void {
		beginPlayersTurn();
	}

	public static function beginAITurn():Void {
		trace("-----> no AI plays the game");
		if (ais.length > 0) {
			setActiveAI(ais[0]);
		} else {
			trace("-- nie ma co --");
			beginPlayersTurn();
		}
	}

	public static function setActiveAI(ai:AI):Void {
		activeAI = ai;
		activeAI.beginTurn();
	}

	public static function getNextAI(ai:AI):AI {
		var index = Lambda.indexOf(ais, ai);
		if (index >= 0 && index < ais.length - 1) {
			return ais[index+1];
		} else {
			return null;
		}
	}

	public static function endAITurn(_:Dynamic=null):Void {
		if (activeAI == null) {
			trace("dostalem endAITurn, a activeAI == null.");
		}
        if (animationDelay <= 0) {
			var nextAI:AI = getNextAI(activeAI);
			if (activeAI.character == null) {
				trace("Usuwam AI w turze");
				removeAI(activeAI);
			}
			if (nextAI != null) {
            	setActiveAI(nextAI);
			} else {
				beginPlayersTurn();
			}
        }
        else {
			trace("endAITurn. nydyrydy, czekamy");
			animationDelayZero.remove(endAITurn);
            animationDelayZero.addOnce(endAITurn);
        }
	}

	public static function beginPlayersTurn():Void {
		trace("-----> no mankind plays the game");
		activeAI = null;
		setActivePlayer(players[0]);
	}

	public static function setActivePlayer(player:Player):Void {
		activePlayer = player;
		activePlayer.beginTurn();
	}

	public static function getNextPlayer(player:Player):Player {
		var index = Lambda.indexOf(players, player);
		if (index >= 0 && index < players.length - 1) {
			return players[index+1];
		} else {
			return null;
		}
	}

	public static function endTurn(_:Int= 0):Void {
        if (animationDelay <= 0) {
            Spellcaster.endTurn();
			var nextPlayer = getNextPlayer(activePlayer);
			if (nextPlayer != null) {
            	setActivePlayer(nextPlayer);
			} else {
				beginAITurn();
			}
        }
        else {
			animationDelayZero.remove(endTurn);
            animationDelayZero.addOnce(endTurn);
        }
	}

	private static function onTileClicked(tile:MapTile):Void {
		if (activePlayer != null) {
			activePlayer.performSpell(tile);
		}
	}

	public static function selectSpell(spell:Spell):Void {
		if (activePlayer != null) {
			activePlayer.setActiveSpell(spell);
		}
	}
}
