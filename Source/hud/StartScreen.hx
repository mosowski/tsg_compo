package hud;

import fabulous.F;
import fabulous.Node;
import fabulous.Image;
import fabulous.DebugTextNode;
import atlas.AtlasSprite;
import object.Item;
import object.Character;
import player.Player;
import spell.*;
import ai.*;

import flash.events.Event;
import flash.Lib;
import spell.Spell;
import object.Character;
import openfl.Assets;

class StartScreen extends Node {
	public function new() {
		super();
		scaleX = scaleY =Math.round(Lib.current.stage.stageWidth / 512);

		makeBtn("Play 1", 20, 20, createOnePlayer);
		makeBtn("Play 2", 20, 70, createTwoPlayers);
		makeBtn("Play 3", 20, 120, createThreePlayers);
		makeBtn("Play 4", 20, 170, createFourPlayers);
	}

	public function makeBtn(text:String, x:Float, y:Float, onClick:Void->Void):Void {
		var node:Node = new Node();
		var bgImg:Image = new Image(F.materialBasic);
		bgImg.frame= F.getSheet("sheet").getFrame("spellbar-bg");
		node.addRenderable(bgImg);
		var dt:DebugTextNode = new DebugTextNode(F.device, 320,40);
		dt.text = text;
		dt.x=20;
		dt.y=20;
		node.addChild(dt);
		node.isTouchEnabled=true;
		node.tapped.addOnce(function(e:Event):Void {
			this.detachFromParent();
			F.ticked.addOnce(function(_:Dynamic):Void {
				prepareGame();
				onClick();
				startTheGame();
			});
		});
		addChild(node);
		node.x = x;
		node.y = y;
	}

    public function createOnePlayer() {
        var char:Character = new Character("hero_1");
		char.actionPoints = 2;
        char.setTile(State.normalMap.getTile(1,1));
        char.addSpell(new Walk());
        char.addSpell(new MeleeAttack(20));
        State.createPlayer().setCharacter(char);
    }
    public function createTwoPlayers() {
        createOnePlayer();
		var char:Character = new Character("hero_2");
		char.actionPoints = 2;
		char.setTile(State.normalMap.getTile(1, State.normalMap.columnNo-2));
		char.addSpell(new Walk());
		char.addSpell(new MeleeAttack(20));
		State.createPlayer().setCharacter(char);
    }
    public function createThreePlayers() {
        createTwoPlayers();
		var char:Character = new Character("hero_3");
		char.actionPoints = 2;
		char.setTile(State.normalMap.getTile(State.normalMap.rowNo - 2,1));
		char.addSpell(new Walk());
		char.addSpell(new MeleeAttack(20));
		State.createPlayer().setCharacter(char);
    }
    public function createFourPlayers() {
        createThreePlayers();
		var char2:Character = new Character("hero_4");
		char2.actionPoints = 2;
		char2.setTile(State.normalMap.getTile(State.normalMap.rowNo - 2,State.normalMap.columnNo - 2));
		char2.addSpell(new Walk());
		char2.addSpell(new MeleeAttack(20));
		State.createPlayer().setCharacter(char2);
    }

	public function prepareGame():Void {
		Timers.init();
		Atlases.init();
		Layers.init();
		Spellcaster.init();
		State.init();
	}

	public function startTheGame():Void {
        State.normalMap.getRandomFreeTile().addItem(new Item("hatch", "locked-hatch"));
        State.hellMap.getRandomFreeTile().addItem(new Item("pentagram", "pentagram"));

		State.beginGame();
	}

}

