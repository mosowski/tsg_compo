package fabulous {
    import flash.geom.Rectangle;    
	
    class Sprite extends Renderable {
        public var _textureResource:TextureResource;
        public var _material:Material;

        public var _frame:SpriteSheetFrame;

        public var _pivotX:Float;
        public var _pivotY:Float;

        public var _uvTop:Float;
        public var _uvLeft:Float;
        public var _uvBottom:Float;
        public var _uvRight:Float;

        public var _hitmask:Hitmask;
        public var _bounds:Rectangle;
		public var _radius:Float;

        public static var _renderer:Renderer;

        public function Sprite(material:Material) {
            _textureResource = null;
            _material = material;
            _frame =  new SpriteSheetFrame(0, 0, 0, 0, 0, 0, 0, 0, TextureManager.UNLOADED_TEXTURE_NAME);

            _pivotX = 0;
            _pivotY = 0;

            _uvTop = _uvLeft = 0;
            _uvBottom = _uvRight = 1;

            _hitmask = new Hitmask();
            _hitmask.makeQuad();

            _bounds = new Rectangle();
        }

        
        override public function get renderer():Renderer {
            return _renderer;
        }

        
        override public function getHitmask():Hitmask {
            return _hitmask;
        }

        
        override public function getBounds():Rectangle {
            return _bounds;
        }
		
		
		override public function get radius():Float {
			return _radius;
		}

        
        public function get width():Float {
            return _frame._imageWidth;
        }
		public function set width(v:Float):Void {
			if (_frame._imageWidth != v) {
				_frame._imageWidth = v;
            	updateBounds();
			}
		}

        
        public function get height():Float {
            return _frame._imageHeight;
        }
		public function set height(v:Float):Void {
			if (_frame._imageHeight != v) {
				_frame._imageHeight = v;
            	updateBounds();
			}
		}


        
        public function set pivotX(value:Float):Void {
			if (_pivotX != value) {
            	_pivotX = value;
            	updateBounds();
			}
        }

        
        public function set pivotY(value:Float):Void {
			if (_pivotY != value) {
            	_pivotY = value;
            	updateBounds();
			}
        }

        
        public function get pivotX():Float {
            return _pivotX;
        }

        
        public function get pivotY():Float {
            return _pivotY;
        }

        
        public function set textureResource(value:TextureResource):Void {
            _textureResource = value;
        }

        
        public function get textureResource():TextureResource {
            return _textureResource;
        }

        
        public function set material(value :Material):Void {
            _material = value;
        }

        
        public function get material():Material {
            return _material;
        }
		
		public function setUV(l:Float, t:Float, w:Float, h:Float):Void {
			_uvLeft = l;
			_uvTop = t;
			_uvRight = l + w;
			_uvBottom = t + h;
            updateBounds();
		}	

        public function setUVFromFrame(frame:SpriteSheetFrame = null):Void {
			if (!frame) {
				frame = _frame;
			}
            _uvLeft = frame._left / _textureResource._width;
            _uvTop = frame._top / _textureResource._height;
            _uvRight = frame._right / _textureResource._width;
            _uvBottom = frame._bottom / _textureResource._height;
            _pivotX = frame._pivotX;
            _pivotY = frame._pivotY;
            updateBounds();
        }

        public function updateBounds():Void {
            _bounds.x = -_pivotX;
            _bounds.y = -_pivotY;
            _bounds.width = _frame._imageWidth;
            _bounds.height = _frame._imageHeight;
			_radius = Math.max(_pivotX, _pivotY, _frame._imageWidth -_pivotX, _frame.imageHeight - _pivotY);
            _hitmask.fromRectangle(_bounds);
        }

		public function setTextureFromFrame(frame:SpriteSheetFrame):Void {
			if (frame) {
                _textureResource = frame._textureResource;
				_textureResource.callAfterLoad(function():Void {
                    setUVFromFrame(frame);
                });
			}
		}

        public function set frame(value:SpriteSheetFrame):Void {
            if (_frame != value) {
                _frame  = value;
				if (_frame) {
                	_textureResource = _frame._textureResource;
					_textureResource.callAfterLoad(function():Void {
                        setUVFromFrame();
                    });
                	updateBounds();
				}
            }
        }

        
        public function get frame():SpriteSheetFrame {
            return _frame;
        }



    }

}
