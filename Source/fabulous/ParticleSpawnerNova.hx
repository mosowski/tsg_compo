package fabulous {
    class ParticleSpawnerNova extends ParticleSpawnerUniformBehaviour {

        public var _minRadius:Float;
        public var _maxRadius:Float;

        public function ParticleSpawnerNova() {
            super();
        }

        public function set minRadius(v:Float):Void {
            _minRadius = v;
        }

        public function set maxRadius(v:Float):Void {
            _maxRadius = v;
        }

        override public function spawn():Void {
            var angle:Float = Math.random() * 2 * Math.PI;
            var radius:Float = _minRadius + Math.sqrt(Math.random()) * (_maxRadius - _minRadius);
            _originOffsetX = Math.sin(angle) * radius;
            _originOffsetY = Math.cos(angle) * radius;

            var vel:Float = minVel + Math.random() * (maxVel - minVel);

            _life = minLife + Math.random() * (maxLife - minLife);
            _startX = _spawnerX + _originOffsetX;
            _startY = _spawnerY + _originOffsetY;
            _deltaX = Math.sin(angle) * _life * vel;
            _deltaY = Math.cos(angle) * _life * vel;

            _spawnerX += _originStepX;
            _spawnerY += _originStepY;

            _startSize = sMinSize + Math.random() * (sMaxSize - sMinSize);
            _deltaSize = eMinSize + Math.random() * (eMaxSize - eMinSize) - _startSize;

            _startRotation = minRotation + Math.random() * (maxRotation - minRotation);
            _deltaRotation = (minOmega + Math.random() * ( maxOmega - minOmega)) * _life;

            var sColorFactor:Float = Math.random();
            var eColorFactor:Float = Math.random();
            var sR:Float = sMinR + sColorFactor * (sMaxR - sMinR);
            var sG:Float = sMinG + sColorFactor * (sMaxG - sMinG);
            var sB:Float = sMinB + sColorFactor * (sMaxB - sMinB);
            var sA:Float = sMinA + sColorFactor * (sMaxA - sMinA);
            var eR:Float = eMinR + eColorFactor * (eMaxR - eMinR);
            var eG:Float = eMinG + eColorFactor * (eMaxG - eMinG);
            var eB:Float = eMinB + eColorFactor * (eMaxB - eMinB);
            var eA:Float = eMinA + eColorFactor * (eMaxA - eMinA);

            _startColor = Int(sR * 255) | (Int(sG * 255) << 8) | (Int(sB * 255) << 16) | (Int(sA * 255) << 24);
            _endColor = Int(eR * 255) | (Int(eG * 255) << 8) | (Int(eB * 255) << 16) | (Int(eA * 255) << 24);
        }

    }

}
