package fabulous {
    import flash.display.Bitmap;    
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.net.URLRequest;
    import flash.system.ImageDecodingPolicy;
    import flash.system.LoaderContext;    
	
    class BitmapLoader extends BaseLoader {
        private static const IMG_CONTEXT:LoaderContext = new LoaderContext();
        {
            IMG_CONTEXT.imageDecodingPolicy = ImageDecodingPolicy.ON_LOAD;
        }

        public var _loader:Loader;

        public function BitmapLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null) {
            super(device, name, url, onComplete);
            _loader = new Loader();
        }

        override public function load():Void {
            super.load();

            addListeners();
            _loader.load(new URLRequest(_url), IMG_CONTEXT);
        }

        override public function getBitmap():Bitmap {
            return _loader.content as Bitmap;
        }

        private function addListeners():Void {
            _loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderComplete);
            _loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onLoaderError);
            _loader.contentLoaderInfo.addEventListener(IOErrorEvent.DISK_ERROR, onLoaderError);
        }

        private function removeListeners():Void {
            _loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoaderComplete);
            _loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onLoaderError);
            _loader.contentLoaderInfo.removeEventListener(IOErrorEvent.DISK_ERROR, onLoaderError);
        }

        override protected function onLoaderComplete(e:Event):Void {
            removeListeners();
            super.onLoaderComplete(e);
        }

        override protected function onLoaderError(e:IOErrorEvent):Void {
            removeListeners();
            super.onLoaderError(e);
        }

        override public function unload():Void {
            (_loader.content as Bitmap).bitmapData.dispose();
            _loader.unloadAndStop();
        }

    }

}
