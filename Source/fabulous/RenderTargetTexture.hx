package fabulous;

import openfl.gl.GL;
import openfl.gl.GLFramebuffer;

class RenderTargetTexture extends Texture {

	private var _glFramebuffer:GLFramebuffer;
	public var _isCreated:Bool;

    public function new() {
        super();
    }

    /**
     * Uploads bitmap as texture source to GPU. Disposes already uploaded
     * texture; throws exception when trying to upload texture being loaded
     * using texture manager.
     * @param	bitmapData
     */
    public function init(width:Int, height:Int):Void {
        if (_glTexture != null && _glTexture != F.device._textureMgr._unloadedFallbackGLTexture) {
			GL.deleteTexture(_glTexture);
        }

		if (F.device._defaultFramebuffer == 0) {
			return;
		}
		_glFramebuffer = GL.createFramebuffer();


		_glTexture = GL.createTexture();
		GL.bindTexture(GL.TEXTURE_2D, _glTexture);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);

		_sourceWidth = _width = width;
		_sourceHeight = _height = height;
		GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGB, _width, _height, 0, GL.RGB, GL.UNSIGNED_BYTE, null);

		GL.bindFramebuffer(GL.FRAMEBUFFER, _glFramebuffer);

		GL.framebufferTexture2D(GL.FRAMEBUFFER, GL.COLOR_ATTACHMENT0, GL.TEXTURE_2D, _glTexture, 0);

		_isCreated = GL.checkFramebufferStatus(GL.FRAMEBUFFER) == GL.FRAMEBUFFER_COMPLETE;

        _isLoaded = true;
        _loaded.post();
    }

	public function bind():Void {
		GL.bindFramebuffer(GL.FRAMEBUFFER, _glFramebuffer);
		GL.clear(GL.COLOR_BUFFER_BIT);
	}
}
