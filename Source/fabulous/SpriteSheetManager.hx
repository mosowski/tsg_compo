package fabulous;

import openfl.Assets;

class SpriteSheetManager {
    public var _device:Device;
    public var _spriteSheetsByName:Map<String,SpriteSheet>;

    public function new(device:Device) {
        _device = device;
    }

    public function init():Void {
    }
	
    public function loadConfigFromObject(object:Dynamic):Void {
        _spriteSheetsByName = new Map<String,SpriteSheet>();
        for (name in Reflect.fields(object)) {
            var spriteSheet:SpriteSheet = new SpriteSheet(_device);
            spriteSheet._name = name;
            _spriteSheetsByName[name] = spriteSheet;
            var path:String = _device.getPath(Reflect.field(Reflect.field(object, name), _device._deviceProfile));
			spriteSheet.fromObject(haxe.Json.parse(Assets.getText(path)));
        }
    }

    public function getSheet(name:String):SpriteSheet {
        return _spriteSheetsByName[name];
    }

}
