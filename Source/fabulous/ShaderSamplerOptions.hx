package fabulous {
	
    class ShaderSamplerOptions {

        public static const TEX_2D_NOMIP_LINEAR:Int = 0;
        public static const TEX_2D_NOMIP_NEAREST:Int = 3;
        public static const TEX_2D_TRILINEAR:Int = 6;

        public static const _samplerCodeBy:Int:Array = [
            "<2d, nomip, linear, clamp>",
            "<2d, nomip, linear, clamp, dxt1>",
            "<2d, nomip, linear, clamp, dxt5>",

            "<2d, nomip, nearest, clamp>",
            "<2d, nomip, nearest, clamp, dxt1>",
            "<2d, nomip, nearest, clamp, dxt5>",

            "<2d, miplinear, linear, clamp>",
            "<2d, miplinear, linear, clamp, dxt1>",
            "<2d, miplinear, linear, clamp, dxt5>"
        ];

    }

}
