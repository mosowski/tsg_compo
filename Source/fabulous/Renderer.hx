package fabulous;

class Renderer {
    public var _device:Device;

    public function new(device:Device) {
        _device = device;
    }

    public function init():Void {
    }

    public function beginFrame():Void {
    }

    public function endFrame():Void {
    }

    public function processRenderable(renderable:Renderable):Void {
    }
}
