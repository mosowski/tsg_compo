package fabulous {    
	
    class ParticleRenderer extends Renderer {

        public function ParticleRenderer(device:Device) {
            super(device);
        }

        override public function processRenderable(renderable:Renderable):Void {
            var particles:Particles = renderable as Particles;

            if (particles._textureResource && particles._spawner) {
                particles._renderBatchBuffer.reset();
                _device._renderBatcher.queue(particles._renderBatchBuffer, particles._material, particles._textureResource, particles._quota * 6);
            }

        }

    }
}

