package fabulous;

class Touch {
    public var _id:Int;
    public var _x:Float;
    public var _y:Float;
	public var _originalX:Float;
	public var _originalY:Float;
	public var _swipeDistance:Float;
    public var _touchedNode:Node;
    public var _swipedXNode:Node;
    public var _swipedYNode:Node;
	public var _swipeStarted:Bool;

    public function new(id:Int, x:Float, y:Float) {
        _id = id;
        _x = x;
        _y = y;
		_originalX = x;
		_originalY = y;
		_swipeDistance = 0;
		_swipeStarted = false;
    }

	public var id(get, null):Int;
    public function get_id():Int {
        return _id;
    }

	public var x(get, null):Float;
    public function get_x():Float {
        return _x;
    }

	public var y(get, null):Float;
    public function get_y():Float {
        return _y;
    }
	
	public var originalX(get,null):Float;
    public function get_originalX():Float {
        return _originalX;
    }

	public var originalY(get,null):Float;
    public function get_originalY():Float {
        return _originalY;
    }

	public var swipeDistance(get,null):Float;
	public function get_swipeDistance():Float {
		return _swipeDistance;
	}

	public function addMovePosition(x:Float, y:Float):Void {
		_swipeDistance += Math.sqrt((x-_x)*(x-_x)+(y-_y)*(y-_y));
		_x = x;
		_y = y;
	}

}
