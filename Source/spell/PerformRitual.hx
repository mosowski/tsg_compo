package spell;

import fabulous.F;
import fabulous.DebugTextNode;

class PerformRitual extends Spell {
	public function new() {
		super("spell-ritual");
		cost = 2;
		displayName = "Bloody Ritual";
	}

	override private function canApply(target:SpellTarget):Bool {
		return true;
	}

	override public function apply(target:SpellTarget):Void {
        if (target.tile.haveItem("pentagram")) {
            Layers.showMap(State.normalMap);
            target.caster.setTile(State.normalMap.getTile(1, 1));
        }
        else {
            target.caster.tile.damage(200);
        }
	}
}
