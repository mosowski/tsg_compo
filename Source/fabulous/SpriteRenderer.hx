package fabulous {    
	
    class SpriteRenderer extends Renderer {
        public var _buffer:RenderBatchBuffer;

        public function SpriteRenderer(device:Device) {
            super(device);
        }

        override public function init():Void {
            _buffer = _device._renderBatcher._quadBuffer;
        }

        override public function processRenderable(renderable:Renderable):Void {
            var sprite:Sprite = renderable as Sprite;
            var x:Int = sprite._node._derivedX;
            var y:Int = sprite._node._derivedY;
            var scaleX:Float = sprite._node._derivedScaleX;
            var scaleY:Float = sprite._node._derivedScaleY;
            var alpha:Float = sprite._node._derivedAlpha;
            var left:Float = Math.round(sprite._bounds.left * scaleX);
            var right:Float = Math.round(sprite._bounds.right * scaleX);
            var top:Float = Math.round(sprite._bounds.top * scaleY);
            var bottom:Float = Math.round(sprite._bounds.bottom * scaleY);
            //var bounds:Rectangle = sprite._bounds;

            if (sprite._textureResource) {
                var vertexBuffer:ByteArrayVertexBuffer = _buffer._vbs[0] as ByteArrayVertexBuffer;

                _device._renderBatcher.queue(_buffer, sprite._material, sprite._textureResource, 6);

                vertexBuffer.beginWrite();

				if (sprite._node._derivedRotation != 0) { 
            		var sin:Float = Math.sin(sprite._node._derivedRotation);
            		var cos:Float = Math.cos(sprite._node._derivedRotation);

                	vertexBuffer.writeFloat(left * cos - top * sin + x);
                	vertexBuffer.writeFloat(top * cos + left * sin + y);
                	vertexBuffer.writeFloat(sprite._uvLeft);
                	vertexBuffer.writeFloat(sprite._uvTop);
                	vertexBuffer.writeFloat(alpha);

                	vertexBuffer.writeFloat(left * cos - bottom * sin + x);
                	vertexBuffer.writeFloat(bottom * cos + left * sin + y);
                	vertexBuffer.writeFloat(sprite._uvLeft);
                	vertexBuffer.writeFloat(sprite._uvBottom);
                	vertexBuffer.writeFloat(alpha);

                	vertexBuffer.writeFloat(right * cos - top * sin + x);
                	vertexBuffer.writeFloat(top * cos + right * sin + y);
                	vertexBuffer.writeFloat(sprite._uvRight);
                	vertexBuffer.writeFloat(sprite._uvTop);
                	vertexBuffer.writeFloat(alpha);

                	vertexBuffer.writeFloat(right * cos - bottom * sin + x);
                	vertexBuffer.writeFloat(bottom * cos + right * sin + y);
                	vertexBuffer.writeFloat(sprite._uvRight);
                	vertexBuffer.writeFloat(sprite._uvBottom);
                	vertexBuffer.writeFloat(alpha);
				} else {
                	vertexBuffer.writeFloat(left + x);
                	vertexBuffer.writeFloat(top + y);
                	vertexBuffer.writeFloat(sprite._uvLeft);
                	vertexBuffer.writeFloat(sprite._uvTop);
                	vertexBuffer.writeFloat(alpha);

                	vertexBuffer.writeFloat(left + x);
                	vertexBuffer.writeFloat(bottom + y);
                	vertexBuffer.writeFloat(sprite._uvLeft);
                	vertexBuffer.writeFloat(sprite._uvBottom);
                	vertexBuffer.writeFloat(alpha);

                	vertexBuffer.writeFloat(right + x);
                	vertexBuffer.writeFloat(top + y);
                	vertexBuffer.writeFloat(sprite._uvRight);
                	vertexBuffer.writeFloat(sprite._uvTop);
                	vertexBuffer.writeFloat(alpha);

                	vertexBuffer.writeFloat(right + x);
                	vertexBuffer.writeFloat(bottom + y);
                	vertexBuffer.writeFloat(sprite._uvRight);
                	vertexBuffer.writeFloat(sprite._uvBottom);
                	vertexBuffer.writeFloat(alpha);
				}


                vertexBuffer.endWrite();
            }

        }

    }
}

