package spell;

class Puff extends Spell {
	public function new() {
		super("icon-magic-bullet", -1);
	}

	override private function canApply(target:SpellTarget):Bool {
		return false;
	}
}
