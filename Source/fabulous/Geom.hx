package fabulous {
	import flash.geom.Point;
	
	class Geom {
		
		
		public static function cross(a:Point, b:Point):Float {
			return a.x * b.y - a.y * b.x;
		}
		
		
		public static function cross2(ax:Float, ay:Float, bx:Float, by:Float):Float {
			return ax * by - ay * bx;
		}
		
		
		public static function dot(a:Point, b:Point):Float {
			return a.x * b.x + a.y * b.y;
		}
		
		
		public static function dot2(ax:Float, ay:Float, bx:Float, by:Float):Float {
			return ax * bx + ay * by;
		}
		
		
		public static function sgn(x:Float):Float {
			return x > 0 ? 1 : -1;
		}
		
		
		public static function intersection(out:Point, a:Point, b:Point, c:Point, d:Point):Bool {
			var bx:Int = b.x - a.x;
			var by:Int = b.y - a.y;
			var dx:Int = d.x - c.x;
			var dy:Int = d.y - c.y;
			var bdotd:Float = bx * dy - by * dx;
			if (bdotd == 0) {
				return false;
			}
			var cx:Int = c.x - a.x;
			var cy:Int = c.y - a.y;
			var t:Float = (cx * dy - cy * dx) / bdotd;
			if (t > 1 || t < 0) {
				return false;
			}
			var u:Float = (cx * by - cy * bx) / bdotd;
			if (u > 1 || u < 0) {
				return false;
			}
			out.x = a.x + t * bx;
			out.y = a.y + t * by;
			return true;
		}

        public static function polygonArea(path:Array<Point>):Float {
            var n:Int = path.length;
			var a:Float = 0.0;
			for (var p:Int = n - 1, q:Int = 0; q < n; p = q++) {
				a += path[p].x * path[q].y - path[q].x * path[p].y;
			}
			return a * 0.5;
        }

        
        public static function insideTriangle(ax:Float, ay:Float, bx:Float, by:Float, cx:Float, cy:Float, px:Float, py:Float):Bool {
            return cross2(px - ax, py - ay, ax - bx, ay - by) >= 0
                && cross2(px - bx, py - by, bx - cx, by - cy) >= 0
                && cross2(px - cx, py - cy, cx - ax, cy - ay) >= 0;
        }

        /**
         * Performs a classical ear-clipping triangulation.
         * @param out
         * @param path
         * @return
         */
		public static function triangulate(out:Array<Int>, path:Array<Point>):Bool {
            out.length = 0;
			var n:Int = path.length;
			if (n < 3) {
				return false;
            }
			
			var verts:Array<Int> = new Array<Int>();
			var v:Int;
			
			if (polygonArea(path) > 0) {
				for (v = 0; v < n; v++) {
					verts[v] = v;
                }
			} else {
				for (v = 0; v < n; v++) {
					verts[v] = (n - 1) - v;
                }
			}
			
			var nv:Int = n;
			
			var count:Int = 2 * nv;
			var m:Int;
			for (m = 0, v = nv - 1; nv > 2; ) {
				if (count-- <= 0) {
					return false;
				}
				
				var u:Int = v;
				if (nv <= u) {
					u = 0;
                }
				v = u + 1;
				if (nv <= v) {
					v = 0;
                }
				var w:Int = v + 1;
				if (nv <= w) {
					w = 0;
                }
				
				if (triangulateSnip(path, u, v, w, nv, verts)) {
					var a:Int, b:Int, c:Int, s:Int, t:Int;
					
					a = verts[u];
					b = verts[v];
					c = verts[w];
					
					out.push(a);
					out.push(b);
					out.push(c);
					m++;
					
					for (s = v, t = v + 1; t < nv; s++, t++) {
						verts[s] = verts[t];
                    }
					nv--;
					
					count = 2 * nv;
				}
			}
			
			return true;
		}
		
		private static function triangulateSnip(path:Array<Point>, u:Int, v:Int, w:Int, n:Int, verts:Array<Int>):Bool {
			var p:Int;
			var ax:Float, ay:Float, bx:Float, by:Float;
			var cx:Float, cy:Float, px:Float, py:Float;
			
			ax = path[verts[u]].x;
			ay = path[verts[u]].y;
			
			bx = path[verts[v]].x;
			by = path[verts[v]].y;
			
			cx = path[verts[w]].x;
			cy = path[verts[w]].y;

            if (cross2(bx - ax, by - ay, cx - ax, cy - ay) < 0.00001) {
                return false;
            }

			for (p = 0; p < n; p++) {
				if ((p == u) || (p == v) || (p == w)) {
					continue;
                }
				px = path[verts[p]].x;
				py = path[verts[p]].y;
				if (insideTriangle(ax, ay, bx, by, cx, cy, px, py)) {
					return false;
                }
			}
			return true;
		}
	}

}