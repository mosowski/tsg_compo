package fabulous {
    import flash.utils.Dictionary;
	
    class LoaderManager {
        public var _device:Device;
        public var _loaders:Dictionary;
        public var _loadersByUrl:Dictionary;
        public var _loaderQueue:LoaderQueue;

        public function LoaderManager(device:Device) {
            _device = device;
            _loaders = new Dictionary();
            _loadersByUrl = new Dictionary();
            _loaderQueue = new LoaderQueue(_device);
        }

        private function startLoader(name:String, url:String, onComplete:Function, loaderType:Class):BaseLoader {
            if (_loaders[name]) {
                _loaders[name].load(onComplete);
                return _loaders[name];
            } else {
                var loader:BaseLoader = _loaders[name] = new loaderType(_device, name, url, onComplete);
                _loadersByUrl[url] = loader;
                loader.queue();
                return loader;
            }
        }

        public function getLoader(nameOrUrl:String):BaseLoader {
            var loader:BaseLoader = _loaders[nameOrUrl];
            if (loader == null) {
                loader = _loadersByUrl[nameOrUrl];
            }
            return loader;
        }

        public function startBitmapLoader(name:String, url:String, onComplete:Function = null):BitmapLoader {
            return startLoader(name, url, onComplete, BitmapLoader) as BitmapLoader;
        }

        public function startBinaryLoader(name:String, url:String, onComplete:Function = null):BinaryLoader {
            return startLoader(name, url, onComplete, BinaryLoader) as BinaryLoader;
        }

        public function startTextLoader(name:String, url:String, onComplete:Function = null):TextLoader {
            return startLoader(name, url, onComplete, TextLoader) as TextLoader;
        }

        public function startBatchedLoader(name:String, onComplete:Function = null, children:Array = null):BatchedLoader {
            var batchedLoader:BatchedLoader = _loaders[name];            
            if (batchedLoader == null) {
                batchedLoader = _loaders[name] = new BatchedLoader(_device, name, "", onComplete);
            }

            if (children) {
                batchedLoader.addChildren(children);
                if (batchedLoader._hasLoaderStarted == false) {
                    batchedLoader._completed.add(onComplete);
                    batchedLoader.load();
                }
            }
            return batchedLoader;
        }

    }

}
