package spell;

import object.Character;
import map.MapTile;

class Spell {
	public var amount:Int;
	public var iconName:String;
	public var displayName:String;
	public var timeout:Int;
	public var cost:Int;
	public var depleted:Signal;

	public function new(iconName:String, amount:Int = 1) {
		this.iconName = iconName;
		this.amount = amount;
		displayName = "";
		timeout = 0;
		cost = 0;
		depleted = new Signal();
	}

	public function perform(target:SpellTarget):Bool {
		var success = canApply(target);
		if (success) {
			if (amount > 0) {
				amount--;
				if (amount == 0) {
					depleted.post(this);
				}
			}
		}
		return success;
	}

	public function apply(target:SpellTarget):Void {
	}

	private function canApply(target:SpellTarget):Bool {
		return State.animationDelay <= 0;
	}
}
