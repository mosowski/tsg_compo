package fabulous {
    import flash.display3D.Context3DBlendFactor;
    import flash.display3D.Context3DProgramType;
    import flash.utils.ByteArray;
    import flash.utils.Endian;

    class MaterialMaskedWaves extends Material {
        public static var _shader:Shader;

        public var _params:ByteArray;

        public function MaterialMaskedWaves(amplitude:Float, frequency:Float, speed:Float) {
            _params = new ByteArray();
            _params.endian = Endian.LITTLE_ENDIAN;
            _params.writeFloat(amplitude);
            _params.writeFloat(frequency);
            _params.writeFloat(speed);
            _params.writeFloat(0);
        }

        public static function initShader(device:Device):Void {
            _shader = new Shader(device);
            _shader.setCode(
                [
                    // va0.xy: vertex position
                    // va1.xy: vertex uv
                    // va2.x: vertex alpha
                    // vc0.xy: screen scale
                    // vc0.zw: screen translation
                    "mov vt0, va0",
                    "mul vt0.xy, vt0.xy, vc0.xy",
                    "add vt0.xy, vt0.xy, vc0.zw",
                    "mov op, vt0",
                    "mov v0, va1",
                    "mov v1, va2"
                ].join("\n"),
                [
                    // v0: vertex uv
                    // v1: vertex alpha
                    // fs0: texture
                    // fc0: amplitude, frequency, speed, time
					"mov ft1, v0",
					"mov ft1.x, fc0.w",
					"mul ft1.x, ft1.x, fc0.z",
					"add ft1.x, ft1.x, v0.y",
					"mul ft1.x, ft1.x, fc0.y",
					"sin ft1.x, ft1.x",
					"mul ft1.x, ft1.x, fc0.x",
					"add ft1.x, ft1.x, v0.x",
                    "tex oc, ft1, fs0 <sampler>"
                ].join("\n")
            );
        }

        
        override public function bind(device:Device, batch:RenderBatch):Void {
            device.setTextureAt(0, batch._textureResource);
            device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
            device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

			_params.position = 12;
			_params.writeFloat(device._currentTime);

            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);
            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _params, 0);
        }
    }

}
