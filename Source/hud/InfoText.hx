package hud;

import fabulous.DebugTextNode;
import fabulous.F;

class InfoText extends DebugTextNode {
	
	public function new(text:String, startX:Float, startY:Float) {
		super(F.device, 200,50);
		this.text = text;
		Layers.fxLayer.addChild(this);
		this.x = startX;
		this.y = startY;
        State.tweener.tween(this, { y: startY }, { y: startY - 64 }, 1.5, State.tweener.linearEase, onComplete);
	}

	public function onComplete():Void {
		Layers.fxLayer.removeChild(this);
	}
}
