package fabulous {
    import flash.display3D.Context3DTextureFormat;
    import flash.utils.ByteArray;
	
    class CompressedTextureResource extends TextureResource {

		public var _atfOffset:Int;

        public function CompressedTextureResource(device:Device) {
            super(device);
        }

        override public function loadFromSource(source:TextureSource):Void {
            loadFromByteArray(source._byteArray);
        }

        public function loadFromByteArray(source:ByteArray):Void {
            if (_isLoading) {
                throw "Trying to upload source to texture being loaded.";
            }

            if (_texture && _texture != _device._textureMgr._unloadedFallbackTexture) {
                _texture.dispose();
            }

            var signature:String = String.fromCharCode(source[0], source[1], source[2]);
            if (signature != "ATF") {
                throw "Invalid ATF.";
            }

			_atfOffset = 6;
			if (source[_atfOffset] == 255) {
				_atfOffset+= 6;
			}
            
            _width = Math.pow(2, source[_atfOffset+1]);
            _height = Math.pow(2, source[_atfOffset+2]);

			_sourceWidth = _width;
			_sourceHeight = _height;

            var textureFormat:String = getTextureFormat(source);
            _isCompressed = textureFormat != Context3DTextureFormat.BGRA;
            _hasAlpha = textureFormat != Context3DTextureFormat.COMPRESSED;

            _texture = _device._ctx.createTexture(_width, _height, textureFormat, false);

            _texture.uploadCompressedTextureFromByteArray(source, 0);

			Log.i("Compressed texture loaded " + _name + ", " + _width + ", " + _height);

            _isLoaded = true;
            _loaded.post();
        }

        private function getTextureFormat(source:ByteArray):String
        {
            switch (source[_atfOffset+0])
            {
                case 0:
                case 1: return Context3DTextureFormat.BGRA;
                case 2:
                case 3: return Context3DTextureFormat.COMPRESSED;
                case 4:
                case 5: return Context3DTextureFormat.COMPRESSED_ALPHA;
                default: return null;
            }
        }

    }

}
