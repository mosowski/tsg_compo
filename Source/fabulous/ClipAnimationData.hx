package fabulous {  

	import flash.geom.Rectangle;
	
    class ClipAnimationData {
        public var _totalFrames:Float;

        public var _frames:Array<ClipFrame>;
        public var _bounds:Rectangle;
		public var _radius:Float;
        public var _hitmask:Hitmask;
        public var _labels:Array;
        public var _metaNodesData:Array<MetaNodeData>;

        public static const ERR_NO_HITMASK_FOR_ANIMATION:String = "NoHitmaskForAnimation";

        public function ClipAnimationData() {
        }

        public function fromDynamic(object:Dynamic, device:Device):Void {
            var i:Int;

            _totalFrames = object.frames.length;

            _frames = new Array<ClipFrame>(_totalFrames);
            for (i = 0; i < _totalFrames; ++i) {
                _frames[i] = new ClipFrame();
                _frames[i].fromDynamic(object.frames[i], device);
            }

			if ("bounds" in object) {
            	_bounds = new Rectangle(object.bounds[0], object.bounds[1], object.bounds[2] - object.bounds[0], object.bounds[3] - object.bounds[1]);
			} else {
				throw "No bounds in clip.";
			}

			_radius =  Math.max( -_bounds.left, -_bounds.top, _bounds.right, _bounds.bottom);

            _hitmask = new Hitmask();
			if ("hitmask" in object) {
            	if (object.hitmask != null) {
                	_hitmask.fromDynamic(object.hitmask);
            	}
			}

            _labels = [];
			if ("labels" in object) {
            	for (i = 0; i < object.labels.length; ++i) {
                	_labels[object.labels[i][0]] = object.labels[i][1];
            	}
			}

			if ("metaNodes" in object) {
            	_metaNodesData = new Array<MetaNodeData>(object.metaNodes.length, true);
            	for (i = 0; i <  object.metaNodes.length; ++i) {
                	var metaNodeData:MetaNodeData = _metaNodesData[i] = new MetaNodeData();
                	metaNodeData.fromDynamic(object.metaNodes[i]);
            	}
			} else {
            	_metaNodesData = new Array<MetaNodeData>();
			}
        }

        public function get numMetaNodes():Int {
            return _metaNodesData.length;
        }

        public function getMetaNode(index:Int, frameNo:Int):MetaNode {
            return _metaNodesData[index].getMetaNodeForFrame(frameNo);
        }

		public function get frames():Array<ClipFrame> {
			return _frames;
		}

    }

}
