package fabulous;

import openfl.gl.GL;

class MaterialBurned extends Material {
    public static var _shader:Shader;

    public function new() {
		super();
    }
	
    public static function initShader():Void {
        _shader = new Shader();

		_shader.setVertexProgram([
			"attribute vec2 a_pos;",
			"attribute vec2 a_uv0;",

			"uniform vec4 u_screen;",

			"varying vec2 v_uv0;",

			"void main(void) {",
			"	gl_Position = vec4(a_pos.xy * u_screen.xy + u_screen.zw, 0.0, 1.0);",
			"	v_uv0 = a_uv0;",
			"}"
		].join("\n"));

		_shader.setFragmentProgram([
			"#ifdef GL_ES",
			"precision mediump float;",
			"#endif",

			"varying vec2 v_uv0;",

			"uniform sampler2D u_sampler0;",

			"void main(void) {",
			"	gl_FragColor = texture2D(u_sampler0, v_uv0) * vec4(vec3(0.75), 1.0);",
			"}"
		].join("\n"));

		_shader.compile();
    }

    
    override public function bind(device:Device, batch:RenderBatch):Void {
        device.setTextureAt(0, batch._texture);
        device.setBlendFactors(GL.ONE, GL.ONE_MINUS_SRC_ALPHA);
		device.setShader(_shader);

		device.setVertexAttrib("a_pos");
		device.setVertexAttrib("a_uv0");

		GL.uniform1i(_shader.getUniform("u_sampler0"), 0);
		GL.uniform4fv(_shader.getUniform("u_screen"), device._screenConstants);
    }
}
