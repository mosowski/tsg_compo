package fabulous;

import flash.geom.Rectangle;

class Text extends Renderable {
    public var _font:Font;
    public var _material:Material;

    public var _hitmask:Hitmask;
	public var _radius:Float;

    public var _text:String;

    public var _widthLimit:Float;
    public var _heightLimit:Float;
    public var _downscaleToLimits:Bool;
    public var _upscaleToLimits:Bool;
    public var _formatScale:Float;
    public var _wordWrap:Bool;
    public var _fontRelativeSize:Float;
    public var _fontPointSize:Float;

    public var _characterOffsetsX:Array<Float>;
    public var _characterOffsetsY:Array<Float>;

    public var _horizontalAlignment:Int;
    public var _verticalAlignment:Int;

    public var _numLines:Int;

	public var _screenSize:Float;

    public static var LEFT:Int = 0;
    public static var CENTER:Int = 1;
    public static var RIGHT:Int = 2;
    public static var TOP:Int = 0;
    public static var BOTTOM:Int = 2;

    public static var _renderer:Renderer;

    public function new() {
		super();
        _material = F.materialBasic;
        _hitmask = new Hitmask();
        _hitmask.makeQuad();
        _text = "";
        _formatScale = 1;
        _fontRelativeSize = 1;
        _fontPointSize = 1;
        _downscaleToLimits = false;
        _upscaleToLimits = false;
        _characterOffsetsX = new Array<Float>();
        _characterOffsetsY = new Array<Float>();
        _numLines = 0;
		_screenSize = 1;
    }

    
    override public function get_renderer():Renderer {
        return _renderer;
    }

    override public function get_hitmask():Hitmask {
        return _hitmask;
    }

	override public function get_radius():Float {
		return _radius;
	}
	
	public var material(get,set):Material;
	public function get_material():Material {
		return _material;
	}
	public function set_material(v:Material):Material {
		return _material = v;
	}

    
	public var font(get,set):Font;
    public function set_font(v:Font):Font{
		if (_font != v) {
            _font = v;
            revalidate();
		}
		return v;
    }
    public function get_font():Font {
        return _font;
    }
    
	public var text(get,set):String;
    public function set_text(v:String):String {
		if (_text != v) {
            _text = v;
            revalidate();
		}
		return v;
    }
    public function get_text():String {
        return _text;
    }

    /**
     * If true, text will be scaled down to fit the widthLimit and heightLimit.
     */
	public var downscaleToLimits(get,set):Bool;
    public function set_downscaleToLimits(v:Bool):Bool {
        if (v != _downscaleToLimits) {
            _downscaleToLimits = v;
            revalidate();
        }
		return v;
    }
    public function get_downscaleToLimits():Bool {
        return _downscaleToLimits;
    }

    /**
     * If true, text will be scaled uo to fit the widthLimit and heightLimit.
     * downscaleToLimits must be true to allow text upscaling.
     */
    public var upscaleToLimits(get,set):Bool;
    public function set_upscaleToLimits(v:Bool):Bool {
        if (v != _upscaleToLimits) {
            _upscaleToLimits = v;
            revalidate();
        }
		return v;
    }
    public function get_upscaleToLimits():Bool {
        return _upscaleToLimits;
    }

    /**
     * If true, words that doesn't fit the line will be wrapped to next line.
     * If word is wider than line, it will be split up.
     */
    public var wordWrap(get,set):Bool;
    public function set_wordWrap(v:Bool):Bool {
        _wordWrap = v;
		return v;
    }
    public function get_wordWrap():Bool {
        return _wordWrap;
    }

    public var widthLimit(get,set):Float;
    public function set_widthLimit(v:Float):Float {
        if (v != _widthLimit) {
            if (v < 0) {
                v = 0;
            }
            _widthLimit = v;
            revalidate();
        }
		return v;
    }
    public function get_widthLimit():Float {
        return _widthLimit;
    }

    
	public var heightLimit(get,set):Float;
    public function set_heightLimit(v:Float):Float {
        if (v != _heightLimit) {
            if (v < 0) {
                v = 0;
            }
            _heightLimit = v;
            revalidate();
        }
		return v;
    }
    public function get_heightLimit():Float {
        return _heightLimit;
    }

    /**
     * Alignemnt of the text in horizontal space. Can be Text.LEFT, Text.CENTER or Text.RIGHT.
     */
    public var horizontalAlignment(get,set):Int;
    public function set_horizontalAlignment(v:Int):Int {
        if (v != _horizontalAlignment) {
            _horizontalAlignment = v;
            revalidate();
        }
		return v;
    }
    public function get_horizontalAlignment():Int {
        return _horizontalAlignment;
    }

    /**
     * Alignemnt of the text in vertical space. Can be Text.TOP, Text.CENTER or Text.BOTTOM.
     */
    public var verticalAlignment(get,set):Int;
    public function set_verticalAlignment(v:Int):Int {
        if (v != _verticalAlignment) {
            _verticalAlignment = v;
            revalidate();
        }
		return v;
    }
    public function get_verticalAlignment():Int {
        return _verticalAlignment;
    }

    /**
     * Scale applied to text before doing the formatting.
     */
	public var fontSize(get,set):Float;	
    public function set_fontSize(v:Float):Float {
        if (v != _fontPointSize) {
            _fontPointSize = v;
            revalidate();
        }
		return v;
    }
    public function get_fontSize():Float {
        return _fontPointSize;
    }

	public var screenSize(get,null):Float;
	public function get_screenSize():Float {
		return _screenSize;
	}

	public var numLines(get,null):Int;
    public function get_numLines():Int {
        return _numLines;
    }

    public function getCharBoundaries(characterIndex:Int):Rectangle {
        var glyph:Glyph = _font._glyphs[_text.charCodeAt(characterIndex)];
        if (glyph != null) {
            return new Rectangle(
                _characterOffsetsX[characterIndex],
                _characterOffsetsY[characterIndex],
                glyph._width * _formatScale * _fontRelativeSize,
                glyph._height * _formatScale * _fontRelativeSize
            );
        } else {
            return new Rectangle(
                _characterOffsetsX[characterIndex],
                _characterOffsetsY[characterIndex]
            );
        }

    }
    
    public function getCharacterX(characterIndex:Int):Float {
        return _characterOffsetsX[characterIndex];
    }

    
    public function getCharacterY(characterIndex:Int):Float {
        return _characterOffsetsY[characterIndex];
    }

    public function revalidateHorizontalAlignment(from:Int, to:Int, space:Float):Void {
		for (i in from...to) {
            if (_horizontalAlignment == CENTER) {
                _characterOffsetsX[i] += space / 2;
            } else if (_horizontalAlignment == RIGHT) {
                _characterOffsetsX[i] += space;
            }
        }
    }


    /**
     * CAUTION: probably the most dirty piece of code in this engine
     */
    public function revalidate():Void {
        if (_font == null || _text == null) {
            return;
        }

		while (_characterOffsetsX.length < _text.length) {
			_characterOffsetsX.push(0);
			_characterOffsetsY.push(0);
		}
        if (_font != null && _font._isLoaded) {
            _fontRelativeSize = _fontPointSize / _font._baseSize;
        } else {
            _fontRelativeSize = 1;
        }

        _numLines = 1;

        var i:Int = 0;
        var glyph:Glyph;
        var altRelativeSize:Float = _fontRelativeSize;
        var maxWidth:Float = 0;
        var maxHeight:Float = 0;

        while (true) {
            var x:Float = 0;
            var y:Float = 0;
            var linePos:Int = 0;
            var lineWidth:Float = 0;
            var wordPos:Int = 0;
            var wordWidth:Float = 0;
            var wordX:Float = 0;
            var ws:Bool = true;
            var pos:Int = 0;
            while (pos != _text.length) {
                var charCode:Int = _text.charCodeAt(pos);
                glyph = _font._glyphs[charCode];
                if (glyph != null) {
                    if (_wordWrap) {
                        if (charCode == 32) {
                            ws = true;
                        } else {
                            if (ws) {
                                wordPos = pos;
                                wordX = x;
                                wordWidth = 0;
                                ws = false;
                            }
                            wordWidth += glyph._width * altRelativeSize;
                            if (wordX + wordWidth > _widthLimit) {
                                if (wordWidth < _widthLimit) {
                                    revalidateHorizontalAlignment(linePos, wordPos, _widthLimit - (x - wordWidth));
                                    pos = wordPos;
                                } else {
                                    if (pos == wordPos) {
                                        break;
                                    }
                                    revalidateHorizontalAlignment(linePos, pos, _widthLimit - x);
                                }
                                x = 0;
                                y += _font._lineHeight * altRelativeSize;
                                linePos = pos;
                                lineWidth = 0;
                                ws = true;
                                _numLines++;
                                continue;
                            }
                            wordWidth = wordWidth - glyph._width * altRelativeSize + glyph._xadvance * altRelativeSize;
                        }
                    }
                    _characterOffsetsX[pos] = x + glyph._xoffset * altRelativeSize;
                    _characterOffsetsY[pos] = y + glyph._yoffset * altRelativeSize;

                    x += glyph._xadvance * altRelativeSize;
                } else {
                    if (charCode == 10) {
                        revalidateHorizontalAlignment(linePos, pos, _widthLimit - x);
                        x = 0;
                        y += _font._lineHeight * altRelativeSize;
                        linePos = pos;
                        lineWidth = 0;
                        _numLines++;
                        ws = true;
                    }
                }
                pos++;
            }
            revalidateHorizontalAlignment(linePos, pos, _widthLimit - x);
            var minX:Float = -1;
			for (i in 0..._characterOffsetsX.length) {
                glyph = _font._glyphs[_text.charCodeAt(i)];
                if (glyph != null) {
                    if (_characterOffsetsX[i] + glyph._width * altRelativeSize > maxWidth) {
                        maxWidth = _characterOffsetsX[i] + glyph._width * altRelativeSize;
                    }
                }
                if (minX == -1 || _characterOffsetsX[i] < minX) {
                    minX = _characterOffsetsX[i];
                }
            }
            if (_horizontalAlignment == CENTER || _horizontalAlignment == RIGHT) {
                maxWidth -= minX;
				for (i in 0..._characterOffsetsX.length) {
                    _characterOffsetsX[i] -= minX;
                }
            }
            maxHeight = y + _font._lineHeight * altRelativeSize;
			if (!_downscaleToLimits || maxHeight <= _heightLimit) {
				break;
			} else {
				altRelativeSize *= 0.9;
			}
        }

        if (_downscaleToLimits && maxHeight > 0 && maxWidth > 0) {
            var vLimitScale:Float = _heightLimit / maxHeight;
            var hLimitScale:Float = _widthLimit / maxWidth;
            if (hLimitScale < vLimitScale) {
                _formatScale = hLimitScale;
            } else {
                _formatScale = vLimitScale;
            }

            if (!_upscaleToLimits && _formatScale > 1) {
                _formatScale = 1;
            }

			for (i in 0..._characterOffsetsX.length) {
                _characterOffsetsX[i] *= _formatScale;
                _characterOffsetsY[i] *= _formatScale;
            }
        } else {
            _formatScale = 1;
        }

        var horizontalOffset:Float = _widthLimit - maxWidth * _formatScale;
        if (_horizontalAlignment != LEFT) {
            if (_horizontalAlignment == CENTER) {
                horizontalOffset /= 2;
            }
        } else {
            horizontalOffset = 0;
        }

        var verticalOffset:Float = _heightLimit - maxHeight * _formatScale;
        if (_verticalAlignment != TOP) {
            if (_verticalAlignment == CENTER) {
                verticalOffset /= 2;
            }
        } else {
            verticalOffset = 0;
        }
		for (i in 0..._characterOffsetsY.length) {
            _characterOffsetsX[i] += horizontalOffset;
            _characterOffsetsY[i] += verticalOffset;
        }


		_radius = Math.max(Math.max(-horizontalOffset, -verticalOffset), Math.max(-horizontalOffset + maxWidth * _formatScale, -verticalOffset + maxHeight * _formatScale));
        _hitmask.setRectangle(-horizontalOffset, -verticalOffset, maxWidth * _formatScale, maxHeight * _formatScale);
        _formatScale *= altRelativeSize / _fontRelativeSize;
    }
}
