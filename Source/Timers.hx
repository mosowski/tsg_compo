package;

import fabulous.F;
import flash.events.Event;

class Timers {

	public static var frame:Signal;
	public static var second:Signal;
	public static var animation:Signal;

	public static var lastSecondTick:Float;
	public static var lastAnimationTick:Float;

	public static function init():Void {
		frame = new Signal();
		second = new Signal();
		animation = new Signal();

		F.ticked.add(onEnterFrame);
		lastSecondTick = F.exactTime;
		lastAnimationTick = F.exactTime;
	}

	private static function onEnterFrame(_:Dynamic):Void {
		frame.post();
		if (F.exactTime > lastSecondTick + 1) {
			lastSecondTick = F.exactTime;
			second.post();
		}
		if (F.exactTime > lastAnimationTick + 0.1) {
			lastAnimationTick = F.exactTime;
			animation.post();
		}
	}
}
