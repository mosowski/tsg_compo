package object;

import atlas.AtlasSprite;
import flash.display.Sprite;
import spell.Spell;

import fabulous.Node;

class Item extends Node {
	public var sprite:AtlasSprite;
    public var name:String;
    public var collectable:Spell;

	public function new(name:String, atlasName:String) {
		super();
        this.name = name;
		addChild(sprite = new AtlasSprite(Atlases.get(atlasName), "idle"));
	}

	public function preformDefaultAction():Void {
	}

}
