package;

import atlas.Atlas;
import fabulous.F;

import openfl.Assets;

class Atlases {

	private static var atlases:Map<String,Atlas>;

	public static function init():Void {
		atlases = new Map<String,Atlas>();
	}

	public static function load(name:String):Atlas {
		var atlas = atlases[name];
		if (atlas == null) {
			atlas = new Atlas();
			var json = haxe.Json.parse(Assets.getText("assets/atlases/"+name+".atlas"));
			atlas.setup(json, F.getSheet("sheet").getFrame(name));
			atlases[name] = atlas;
		}
		return atlas;
	}

	public static function get(name:String):Atlas {
		return load(name);
	}
}
