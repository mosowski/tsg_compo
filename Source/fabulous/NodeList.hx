package fabulous {
	
    class NodeList {
        public var _elem:Node;
        public var _next:NodeList;
        public var _prev:NodeList;

        public function NodeList(elem:Node = null, prev:NodeList = null) {
            _elem = elem;
            _prev = prev;
            _next = null;
        }

        
        public function append(elem:Node):Void {
            var item:NodeList = this;
            while (item._next) {
                item = item._next;
            }
            item._next = new NodeList(elem, item);
        }

        
        /**
         * Inserts element at specified index, shifting rest of existing elements if necessary.
         * Returns new head of the list.
         * @param elem
         * @param index
         * @return
         */
        public function insert(elem:Node, index:Int):NodeList {
            var item:NodeList = this;
            var count:Int = index;
            while (count-- > 0 && item._next) {
                item = item._next;
            }
            var newItem:NodeList;
            if (count > 0) {
                newItem = new NodeList(elem, item);
                item._next = newItem;
            } else {
                newItem = new NodeList(elem, item._prev);
                newItem._next = item;
                if (item._prev) {
                    item._prev._next = newItem;
                }
                item._prev = newItem;
            }
            if (index == 0) {
                return newItem;
            } else {
                return this;
            }
        }

        
        public function get length():Int {
            var result:Int = 0;
            var item:NodeList = this;
            while (item) {
                item = item._next;
                result++;
            }
            return result;
        }

        
        public function clear():Void {
            _elem = null;
            _prev = null;
            _next = null;
        }

        
        public function remove():Void {
            if (_next) {
                _next._prev = _prev;
            }
            if (_prev) {
                _prev._next = _next;
            }
        }

        
        public function find(elem:Node):NodeList {
            var item:NodeList = this;
            while (item) {
                if (item._elem == elem) {
                    return item;
                } else {
                    item = item._next;
                }
            }
            return null;
        }

        
        public function at(index:Int):NodeList {
            var item:NodeList = this;
            while (index-- > 0) {
                item = item._next;
            }
            return item;
        }

        
        public function addNext(elem:Node):Void {
            var item:NodeList = new NodeList(elem, this);
            item._next = _next;
            _next = item;
        }

        
        public function addPrev(elem:Node):Void {
            var item:NodeList = new NodeList(elem, _prev);
            item._next = this;
            this._prev = item;
        }

        
        public function removeAt(index:Int):Void {
            var item:NodeList = this;
            while (index-- > 0) {
                item = item._next;
            }
            item.remove();
        }

        
        public function removeElem(elem:Node):Void {
            var item:NodeList = this;
            while (item && item._elem != elem) {
                item = item._next;
            }
            if (item) {
                item.remove();
            }
        }

    }

}