package fabulous;
	
class TextRenderer extends Renderer {
    public var _buffer:RenderBatchBuffer;

    public function new(device:Device) {
        super(device);
    }

    override public function init():Void {
        _buffer = _device._renderBatcher._quadBuffer;
    }

    override public function processRenderable(renderable:Renderable):Void {
        var text:Text = cast renderable;
		var node:Node = text._node;

		var a:Float = node._derivedMtx.a;
		var b:Float = node._derivedMtx.b;
		var c:Float = node._derivedMtx.c;
		var d:Float = node._derivedMtx.d;
		var tx:Float = node._derivedMtx.tx;
		var ty:Float = node._derivedMtx.ty;
        var alpha:Float = text._node._derivedAlpha;
		
        var glyphScale:Float = text._formatScale * text._fontRelativeSize;
        var texture:Texture = text._font._texture;
        var material:Material = text._material;

		var left:Float;
		var top:Float;
		var right:Float;
		var bottom:Float;

		text._screenSize = text._font._baseSize * glyphScale * d;

        if (texture != null) {
            var charOffsetsX:Array<Float> = text._characterOffsetsX;
            var charOffsetsY:Array<Float> = text._characterOffsetsY;
            var vb:VertexBuffer = _buffer._vb;

            vb.beginWrite();

			for (i in 0...text._text.length) {
                var glyph:Glyph = text._font._glyphs[text._text.charCodeAt(i)];
                if (glyph != null) {
                    _device._renderBatcher.queue(_buffer, material, texture, 6);

					left = charOffsetsX[i];
					top = charOffsetsY[i];
					right = (charOffsetsX[i] + glyph._width * glyphScale);
					bottom = (charOffsetsY[i] + glyph._height * glyphScale);

					vb.writeFloat(left * a + top * c + tx);
					vb.writeFloat(left * b + top * d + ty);
                    vb.writeFloat(glyph._uvLeft);
                    vb.writeFloat(glyph._uvTop);
                    vb.writeFloat(alpha);

					vb.writeFloat(left * a + bottom * c + tx);
					vb.writeFloat(left * b + bottom * d + ty);
                    vb.writeFloat(glyph._uvLeft);
                    vb.writeFloat(glyph._uvBottom);
                    vb.writeFloat(alpha);

					vb.writeFloat(right * a + top * c + tx);
					vb.writeFloat(right * b + top * d + ty);
                    vb.writeFloat(glyph._uvRight);
                    vb.writeFloat(glyph._uvTop);
                    vb.writeFloat(alpha);

					vb.writeFloat(right * a + bottom * c + tx);
					vb.writeFloat(right * b + bottom * d + ty);
                    vb.writeFloat(glyph._uvRight);
                    vb.writeFloat(glyph._uvBottom);
                    vb.writeFloat(alpha);
                }
			}
            vb.endWrite();
        }

    }

}
