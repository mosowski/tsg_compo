package fabulous;

import openfl.gl.GL;
	
class RenderBatcher {
    public var _device:Device;

    public var _quadBuffer:RenderBatchBuffer;
    public var _unorderedBuffer:RenderBatchBuffer;

    public var _renderQueue:Array<RenderBatch>;
    public var _currentRenderBatchId:Int;
    public var _currentRenderBatch:RenderBatch;

    public var _currentTexture:Texture;
    public var _currentMaterial:Material;
    public var _currentBuffer:RenderBatchBuffer;

    public var _buffers:Array<RenderBatchBuffer>;

    public function new(device:Device) {
        _device = device;
        _renderQueue = new Array<RenderBatch>();
    }

    public function init():Void {
        _buffers = new Array<RenderBatchBuffer>();
        _quadBuffer = new RenderBatchBuffer(_device, VertexBuffer.MAX_SIZE, RenderBatchBuffer.VBTYPE_FLOAT, IndexBuffer.MAX_SIZE, [
 			new VertexBufferAttrib("a_pos", 2),
			new VertexBufferAttrib("a_uv0", 2),
			new VertexBufferAttrib("a_alpha", 1)
		]);
        _buffers.push(_quadBuffer);

        _unorderedBuffer = new RenderBatchBuffer(_device, VertexBuffer.MAX_SIZE, RenderBatchBuffer.VBTYPE_FLOAT, IndexBuffer.MAX_SIZE, [
 			new VertexBufferAttrib("a_pos", 2),
			new VertexBufferAttrib("a_uv0", 2),
			new VertexBufferAttrib("a_alpha", 1)
		]);
        _buffers.push(_unorderedBuffer);
    }

    public function beginBatch(buffer:RenderBatchBuffer, material:Material, texture:Texture):Void {
        _currentMaterial = material;
        _currentTexture = texture;
        _currentBuffer = buffer;

        if (_currentRenderBatchId >= _renderQueue.length) {
            _renderQueue[_currentRenderBatchId] = new RenderBatch();
        }
        _currentRenderBatch = _renderQueue[_currentRenderBatchId];
        _currentRenderBatch._firstIndex = buffer._numQueuedIndices;
        _currentRenderBatch._material = material;
        _currentRenderBatch._texture= texture;
        _currentRenderBatch._buffer = buffer;
    }

    public function flushQueue():Void {
        closeCurrentBatch();

        var currentBuffer:RenderBatchBuffer = null;

        var renderBatch:RenderBatch;
		for (i in 0..._currentRenderBatchId) {
            renderBatch = _renderQueue[i];
            if (renderBatch._numIndices > 0) {
                if (currentBuffer != renderBatch._buffer) {
                    if (currentBuffer != null) {
                        currentBuffer.unbind();
                    }
                    currentBuffer = renderBatch._buffer;
                    currentBuffer.bind();
                    currentBuffer.upload();
                }

                renderBatch._material.bind(_device, renderBatch);

				GL.drawElements(GL.TRIANGLES, renderBatch._numIndices, GL.UNSIGNED_SHORT, renderBatch._firstIndex * 2);

                _device.clearSamplers();
            }
        }
        if (currentBuffer != null) {
            currentBuffer.unbind();
        }
    }

    public function resetBatching():Void {
        _currentRenderBatchId = 0;
        _currentRenderBatch = null;
        _currentMaterial = null;
        _currentTexture = null;
		for (i in 0..._buffers.length) {
            _buffers[i].reset();
        }
    }

    public function closeCurrentBatch():Void {
        if (_currentRenderBatch == null) {
            return;
        }
        if (_currentRenderBatch._buffer != null) {
            _currentRenderBatch._numIndices = _currentRenderBatch._buffer._numQueuedIndices - _currentRenderBatch._firstIndex;
        } else {
            _currentRenderBatch._numIndices = 0;
        }
        _currentRenderBatchId++;
    }

    public function queue(buffer:RenderBatchBuffer, material:Material, texture:Texture, indexRangeLength:Int):Void {
        if (_currentMaterial == null
        || (_currentMaterial != null
        && (texture != _currentTexture || material != _currentMaterial || buffer != _currentBuffer))) {
            closeCurrentBatch();
            beginBatch(buffer, material, texture);
        }
        buffer._numQueuedIndices += indexRangeLength;
    }
}
