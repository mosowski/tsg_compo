package fabulous {
    class Stats {
        public var _visibleNodes:Int;
        public var _visibleRenderables:Int;
        public var _drawnTriangles:Int;
		public var _drawCalls:Int;
		public var _clippedRenderables:Int;
        public function Stats() {
        }

        public function reset():Void {
            _visibleNodes = 0;
            _visibleRenderables = 0;
            _drawnTriangles = 0;
			_clippedRenderables = 0;
			_drawCalls = 0;
        }

        public function get visibleNodes():Int {
			return _visibleNodes;
		}
        public function get visibleRenderables():Int {
			return _visibleRenderables;
		}
        public function get drawnTriangles():Int {
			return _drawnTriangles;
		}
		public function get clippedRenderables():Int {
			return _clippedRenderables;
		}
		public function get drawCalls():Int {
			return _drawCalls;
		}

    }

}
