package fabulous;

import flash.geom.Point;
import flash.geom.Rectangle;
import flash.geom.Matrix;

class Hitmask {
    public var _path:Array<Point>;
    public var _indices:Array<Int>;

    public function new() {
        _path = new Array<Point>();
        _indices = new Array<Int>();
    }

    public function fromObject(object:Dynamic):Void {
        _path = new Array<Point>();
        var i:Int = 0;
		while (i < object.path.length) {
			_path.push(new Point(object.path[i], object.path[i + 1]));
			i += 2;
        }
        _indices = cast object.indices;
    }

    public function makeQuad():Void {
        _path = new Array<Point>();
		_path.push(new Point());
		_path.push(new Point());
		_path.push(new Point());
		_path.push(new Point());
        _indices = new Array<Int>();
		_indices.push(0);
		_indices.push(1);
		_indices.push(2);
		_indices.push(2);
		_indices.push(3);
		_indices.push(0);
    }

    
    public function fromRectangle(rectangle:Rectangle):Void {
        _path[0].setTo(rectangle.left, rectangle.top);
        _path[1].setTo(rectangle.left, rectangle.bottom);
        _path[2].setTo(rectangle.right, rectangle.bottom);
        _path[3].setTo(rectangle.right, rectangle.top);
    }

	public function setRectangle(x:Float, y:Float, width:Float, height:Float):Void {
        _path[0].setTo(x, y);
        _path[1].setTo(x, y + height);
        _path[2].setTo(x + width, y + height);
        _path[3].setTo(x + width, y);
	}

    public function hitTest(ax:Float, ay:Float, mtx:Matrix):Bool {
        var i:Int, j:Int = 0, inside:Bool = false;
        var x = (ax - mtx.tx) / mtx.a;
        var y = (ay - mtx.ty) / mtx.d;
		i = _path.length - 1;
		while (i >= 0) {
            if (((_path[i].y <= y && y < _path[j].y) || (_path[j].y <= y && y < _path[i].y))
            	&& (x < (_path[j].x - _path[i].x) * (y - _path[i].y) / (_path[j].y - _path[i].y) + _path[i].x)) {
                inside = !inside;
            }
			j = i--;
        }
        return inside;
    }
}
