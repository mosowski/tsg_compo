package fabulous {
    import flash.display3D.Context3DBlendFactor;
    import flash.display3D.Context3DProgramType;
    import flash.utils.ByteArray;
    import flash.utils.Endian;

    class MaterialMul extends Material {
        public static var _shader:Shader;

        public var _color:ByteArray;

        public function MaterialMul(r:Float, g:Float, b:Float, a:Float) {
            _color = new ByteArray();
            _color.endian = Endian.LITTLE_ENDIAN;
            _color.writeFloat(r);
            _color.writeFloat(g);
            _color.writeFloat(b);
            _color.writeFloat(a);
        }

        public static function initShader(device:Device):Void {
            _shader = new Shader(device);
            _shader.setCode(
                [
                    // va0.xy: vertex position
                    // va1.xy: vertex uv
                    // va2.x: vertex alpha
                    // vc0.xy: screen scale
                    // vc0.zw: screen translation
                    "mov vt0, va0",
                    "mul vt0.xy, vt0.xy, vc0.xy",
                    "add vt0.xy, vt0.xy, vc0.zw",
                    "mov op, vt0",
                    "mov v0, va1",
                    "mov v1, va2"
                ].join("\n"),
                [
                    // v0: vertex uv
                    // v1: vertex alpha
                    // fs0: texture
                    // fc0: color to multiply by texture color
                    "tex ft0, v0, fs0 <sampler>",
                    "mul oc, ft0, fc0"
                ].join("\n")
            );
        }

        
        public function setColor(r:Float, g:Float, b:Float, a:Float):Void {
            _color.position = 0;
            _color.writeFloat(r);
            _color.writeFloat(g);
            _color.writeFloat(b);
            _color.writeFloat(a);
        }

		public function set intColor(c:Int):Void {
			_color.position = 0;
			_color.writeFloat(((c >> 16) & 255) / 255);
			_color.writeFloat(((c >> 8) & 255) / 255);
			_color.writeFloat((c & 255) / 255);
			_color.writeFloat(((c >> 24) & 255) / 255);
		}


        
        override public function bind(device:Device, batch:RenderBatch):Void {
            device.setTextureAt(0, batch._textureResource);
            device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
            device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);
            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _color, 0);
        }
    }

}
