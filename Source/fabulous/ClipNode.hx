package fabulous {
    import flash.geom.Rectangle;

    class ClipNode extends Node {
        private var _clip:Clip;
        private var _animationFinished:Signal;

        public function ClipNode(clip:Clip = null) {
            _animationFinished = new Signal();
            if (clip) {
                setClip(clip);
            }
        }

        public function setClip(newClip:Clip):Void {
            if (_clip) {
                _clip.animationFinished.remove(onAnimationFinished);
                removeRenderable(_clip);
            }
            _clip = newClip;
            if (_clip) {
                _clip.animationFinished.add(onAnimationFinished);
                addRenderable(_clip);
            }
        }

        public function playAnimation(animationName:String, startFrame:Int = 0, repeats:Int = -1, rewindAtEnd:Bool = false):Void {
            if (_clip) {
                _clip.playAnimation(animationName, startFrame, repeats, rewindAtEnd);
            }
        }

        
        public function set playbackSpeed(value:Float):Void {
            if (_clip) {
                _clip.playbackSpeed = value;
            }
        }

        private function onAnimationFinished():Void {
            _animationFinished.post();
        }

        
        public function get animationFinished():Signal {
            return _animationFinished;
        }

        
        public function get width():Float {
            return _derivedScaleX * _clip.width;
        }

        
        public function get height():Float {
            return _derivedScaleY * _clip.height;
        }

        
        public function get bounds():Rectangle {
            return _clip.getBounds();
        }

        
        public function get currentFrame():Int {
            return _clip.currentFrame;
        }
		
		public function set currentFrame(v:Int):Void {
			_clip.currentFrame = v;
		}

		public function get repeats():Int {
			return _clip.repeats;
		}
		public function set repeats(v:Int):Void {
			_clip.repeats = v;
		}

		public function get clipData():ClipData {
			return _clip.clipData;
		}

		public function get clipAnimationData():ClipAnimationData {
			return _clip.clipAnimationData;
		}

        
        public function get derivedLeft():Float {
            return _derivedX + _derivedScaleX * _clip.getBounds().left;
        }

        
        public function get derivedTop():Float {
            return _derivedY + _derivedScaleY * _clip.getBounds().top;
        }

        
        public function get derivedRight():Float {
            return _derivedX + _derivedScaleX * _clip.getBounds().right;
        }

        
        public function get derivedBottom():Float {
            return _derivedY + _derivedScaleY * _clip.getBounds().bottom;
        }

    }

}
