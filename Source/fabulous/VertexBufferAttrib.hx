package fabulous;

class VertexBufferAttrib {
	public var _name:String;
	public var _offset:Int;
	public var _size:Int;

	public function new(name:String, size:Int) {
		_name = name;
		_size = size;
	}
}
