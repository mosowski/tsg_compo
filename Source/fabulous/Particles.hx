package fabulous {
    import flash.display3D.Context3DBlendFactor;
    import flash.display3D.Context3DVertexBufferFormat;
    import flash.geom.Rectangle;    
	
    class Particles extends Renderable {
        public var _textureResource:TextureResource;
        public var _material:MaterialParticles;
        public var _device:Device;
		public var _anchorNode:Node;

        public var _vertexBuffer:ByteArrayVertexBuffer;
        public var _renderBatchBuffer:RenderBatchBuffer;

        public var _hitmask:Hitmask;
        public var _bounds:Rectangle;
		public var _radius:Float;

        public var _particlesToSpawn:Float;
        public var _quota:Int;
        public var _spawnSlotId:Int;
        public var _expirationTimes:Array<Float>;

        public var _lastSpawnerX:Float;
        public var _lastSpawnerY:Float;

        public var _spawner:ParticleSpawnerBehaviour;
        public var _srcBlendFactor:String;
        public var _destBlendFactor:String;

        public var _spawnRate:Float;
        public var _spawnLimit:Float;

        public static const _offsets:Array<Float> = Array<Float>([ -0.5, -0.5, 0.5, -0.5, -0.5, 0.5, 0.5, 0.5]);

		public static var _renderer:Renderer;

        public function Particles(material:MaterialParticles, device:Device) {
            _textureResource = null;
            _material = material;
			_material._particles = this;
            _device = device;

            _particlesToSpawn = 0;
            _spawnLimit = -1;
            _spawnSlotId = 0;
            _srcBlendFactor = Context3DBlendFactor.ONE;
            _destBlendFactor = Context3DBlendFactor.ONE;

            _hitmask = new Hitmask();
            _hitmask.makeQuad();

            _bounds = new Rectangle();
			_radius = 1000000;
        }

		
		override public function get renderer():Renderer {
			return _renderer;
		}

        public function set spawner(v:ParticleSpawnerBehaviour):Void {
            _spawner = v;
			if (_spawner) {
            	resetSpawner();
			}
        }

        public function get spawner():ParticleSpawnerBehaviour {
            return _spawner;
        }
		
		public function get anchorNode():Node {
			return _anchorNode;
		}

		public function set anchorNode(v:Node):Void {
			_anchorNode = v;
		}

        public function set srcBlendFactor(v:String):Void {
            _srcBlendFactor = v;
        }

        public function get srcBlendFactor():String {
             return _srcBlendFactor;
        }

        public function set destBlendFactor(v:String):Void {
            _destBlendFactor = v;
        }

        public function get destBlendFactor():String {
             return _destBlendFactor;
        }

        public function resetSpawner():Void {
			if (!_spawner || !_anchorNode) {
            	_spawner.tick(this, 0);
			}
        }

        public function set quota(v:Int):Void {
            if (_quota != v) {
                _quota = v;

            	_renderBatchBuffer = new RenderBatchBuffer(_device, 1, quota * 4, RenderBatchBuffer.VBTYPE_BYTEARRAY, quota * 6, [
                    Context3DVertexBufferFormat.FLOAT_4,
                    Context3DVertexBufferFormat.FLOAT_4,
                    Context3DVertexBufferFormat.FLOAT_4,
                    Context3DVertexBufferFormat.BYTES_4,
                    Context3DVertexBufferFormat.BYTES_4
            	] );
            	_vertexBuffer = _renderBatchBuffer._vbs[0] as ByteArrayVertexBuffer;

                _expirationTimes = new Array<Float>(_quota);
            }
        }

        public function get quota():Int {
            return _quota;
        }

        override public function tick(device:Device):Void {
			if (!_spawner || !_anchorNode) {
				return;
			}

            var numNewParticles:Float = spawnRate * _device.dt;
            if (_spawnLimit >= 0) {
                if (numNewParticles >= _spawnLimit) {
                    numNewParticles = _spawnLimit;
                    _spawnLimit = 0;
                } else {
                    _spawnLimit -= numNewParticles;
                }
            }
            _particlesToSpawn += numNewParticles;


            var trials:Int = _quota;
            var time:Float = _device._currentTime - _device.dt;

            _spawner.tick(this, _particlesToSpawn);

            while (trials-- && _particlesToSpawn >= 1) {
                if (time > _expirationTimes[_spawnSlotId]) {

                    _spawner.spawn();

                    var bornTime:Float = time + Math.random()*_device.dt;
                    _expirationTimes[_spawnSlotId] = bornTime + _spawner.life;

					_vertexBuffer.vertexPosition = _spawnSlotId * 4;
            		_vertexBuffer.beginWrite();
                    for (var i:Int = 0; i < 4; ++i) {
                        // va0
                        _vertexBuffer.writeFloat(bornTime);
                        _vertexBuffer.writeFloat(_spawner.life);
                        _vertexBuffer.writeFloat(_offsets[i * 2]);
                        _vertexBuffer.writeFloat(_offsets[i * 2 + 1]);

                        // va1
                        _vertexBuffer.writeFloat(_spawner.startX);
                        _vertexBuffer.writeFloat(_spawner.startY);
                        _vertexBuffer.writeFloat(_spawner.deltaX);
                        _vertexBuffer.writeFloat(_spawner.deltaY);

                        // va2
                        _vertexBuffer.writeFloat(_spawner.startSize);
                        _vertexBuffer.writeFloat(_spawner.deltaSize);
                        _vertexBuffer.writeFloat(_spawner.startRotation);
                        _vertexBuffer.writeFloat(_spawner.deltaRotation);

                        // va3
                        _vertexBuffer.writeUnsigned:Int(_spawner.startColor);
                        // va4
                        _vertexBuffer.writeUnsigned:Int(_spawner.endColor);
                    }
            		_vertexBuffer.endWrite();
                    _particlesToSpawn -= 1;

                }
                _spawnSlotId = (_spawnSlotId + 1) % _quota;
            }
            if (!trials) {
                _particlesToSpawn = 0;
            }

        }

        public function dispose():Void {
        }

        override public function getBounds():Rectangle {
            return _bounds;
        }
		
		
		override public function get radius():Float {
			return _radius;
		}

        override public function getHitmask():Hitmask {
            return _hitmask;
        }

        
        public function set textureResource(value:TextureResource):Void {
            _textureResource = value;
        }

        
        public function get textureResource():TextureResource {
            return _textureResource;
        }

        
        public function set material(value:MaterialParticles):Void {
			if (_material != value) {
				if (_material) {
					_material._particles = null;
				}
            	_material = value;
				if (_material) {
					_material._particles = this;
				}
			}
        }

        
        public function get material():MaterialParticles { 
            return _material;
        }

        public function get spawnRate():Float {
            return _spawnRate;
        }

        public function set spawnRate(value:Float):Void {
            _spawnRate = value;
        }

        public function get spawnLimit():Float {
            return _spawnLimit;
        }

        public function set spawnLimit(value:Float):Void {
            _spawnLimit = value;
        }


    }

}
