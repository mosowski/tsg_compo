package fabulous;

class Font {
	public var _device:Device;
    public var _name:String;
    public var _texture:Texture;
    public var _glyphs:Map<Int,Glyph>;
    public var _lineHeight:Float;
    public var _baseSize:Float;
    public var _isLoaded:Bool;

    public function new(device:Device) {
		_device = device;
        _glyphs = new Map<Int,Glyph>();
        _lineHeight = 0;
        _baseSize = 1;
        _texture = null;
        _isLoaded = false;
    }

    public function fromObject(object:Dynamic):Void {
        _name = object.name;
        _lineHeight = object.lineHeight;
        _baseSize = object.base;
		var glyphsIter:Array<Array<Int>> = object.glyphs;
        for (glyph in glyphsIter) {
            _glyphs[glyph[0]] = new Glyph(glyph[1], glyph[2], glyph[3], glyph[4], glyph[5], glyph[6], glyph[7]);
        }

        _texture = _device._textureMgr.loadTexture(object.texture);

        for (glyph in _glyphs) {
            glyph._uvLeft = glyph._x / _texture._width;
            glyph._uvTop = glyph._y / _texture._height;
            glyph._uvRight = (glyph._x + glyph._width) / _texture._width;
            glyph._uvBottom = (glyph._y + glyph._height) / _texture._height;
        }

        _isLoaded = true;
    }

	public var baseSize(get,null):Float;
    public function get_baseSize():Float {
        return _baseSize;
    }
}
