package fabulous;

import openfl.gl.GL;
import openfl.gl.GLBuffer;

class VertexBuffer {
	public var _device:Device;
	public var _buffer:GLBuffer;
	public var _numVertices:Int;
    public var _data32PerVertex:Int;
    public var _format:Array<VertexBufferAttrib>;
	public var _formatByName:Map<String,VertexBufferAttrib>;
    public var _changeRangeBegin:Int;
    public var _changeRangeEnd:Int;
    public var _position:Int;

    public static var MAX_SIZE:Int = 0xFFFF;
	
	public function new(device:Device) {
		_device = device;
	}

    public function setSizeAndFormat(numVertices:Int, format:Array<VertexBufferAttrib>):Void {
        if (_buffer != null) {
			GL.deleteBuffer(_buffer);
        }

        _numVertices = numVertices;
		_format = format;
		_formatByName = new Map<String,VertexBufferAttrib>();
        _data32PerVertex = 0;
		for (i in 0..._format.length) {
            _format[i]._offset =_data32PerVertex;
			_formatByName[_format[i]._name] = _format[i];
			_data32PerVertex += _format[i]._size;
        }
		_buffer = GL.createBuffer();
        _position = 0;
    }

    public function upload():Void {
        throw "Abstract";
    }

	/** Call it before writing to the *consecutive* vertices in the buffer, but 
	  * just after setting the right position. */
    public function beginWrite():Void {
        if (_position / _data32PerVertex < _changeRangeBegin) {
            _changeRangeBegin = Std.int(_position / _data32PerVertex);
        }
    }

    public function endWrite():Void {
        if (_position / _data32PerVertex > _changeRangeEnd) {
            _changeRangeEnd = Std.int(_position / _data32PerVertex);
        }
    }

    public function writeFloat(f:Float):Void {
        throw "Abstract";
    }

	public var position(get,set):Int;
    public function set_position(value:Int):Int {
        return _position = value;
    }
    public function get_position():Int {
        return _position;
    }

    
	public var vertexPosition(get,set):Int;
    public function set_vertexPosition(value:Int):Int {
        return position = value * _data32PerVertex;
    }
    public function get_vertexPosition():Int {
        return Std.int(_position / _data32PerVertex);
    }

}

