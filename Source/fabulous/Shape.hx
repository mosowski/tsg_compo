package fabulous {
    import flash.geom.Point;
    import flash.geom.Rectangle;    

	class Shape extends Renderable {
        public var _textureResource:TextureResource;
        public var _material:Material;

        public var _hitmask:Hitmask;
        public var _bounds:Rectangle;
		public var _radius:Float;
        public var _hasBoundsChanged:Bool;

        public var _positions:Array<Point>;
        public var _vertices:Array<Float>;
        public var _indices:Array<Int>;

        public static var _renderer:Renderer;

		public function Shape(material:Material) {
            _material = material;
            _vertices = new Array<Float>();
            _indices = new Array<Int>();
            _bounds = new Rectangle();
            _hitmask = new Hitmask();
		}

        
        override public function get renderer():Renderer {
            return _renderer;
        }

        
        override public function getHitmask():Hitmask {
            return _hitmask;
        }

        
        override public function getBounds():Rectangle {
            updateBounds();
            return _bounds;
        }
		
		
		override public function get radius():Float {
			return _radius;
		}

        public function setVertexCount(count:Int):Void {
            _vertices = new Array<Float>(count*5, true);
        }

        public function setTriangleCount(count:Int):Void {
            _indices = new Array<Int>(count * 3, true);
        }

        public function triangulate():Void {
            updateBounds();
            _indices = new Array<Int>();
            Geom.triangulate(_indices, _hitmask._path);
        }

        
        public function setPos(id:Int, x:Float, y:Float):Void {
            id *= 5;
            _vertices[id] = x;
            _vertices[id + 1] = y;
            _hasBoundsChanged = true;
        }

        
        public function setPosUv(id:Int, x:Float, y:Float, u:Float, v:Float):Void {
            id *= 5;
            _vertices[id] = x;
            _vertices[id+1] = y;
            _vertices[id+2] = u;
            _vertices[id + 3] = v;
            _hasBoundsChanged = true;
        }

        
        public function setPosUvAlpha(id:Int, x:Float, y:Float, u:Float, v:Float, a:Float):Void {
            id *= 5;
            _vertices[id] = x;
            _vertices[id+1] = y;
            _vertices[id+2] = u;
            _vertices[id+3] = v;
            _vertices[id + 4] = a;
            _hasBoundsChanged = true;
        }

        
        public function setAlphaForAll(a:Float=1):Void {
            for (var i:Int = 0; i < _vertices.length; i += 5) {
                _vertices[i + 4] = a;
            }
        }

        /**
         * Makes vertices from points. If you need to make vertices from raw coordinates, use
         * setPath2.
         * @param path
         */
        
        public function setPath(path:Array<Point>):Void {
            setVertexCount(path.length);
            for (var i:Int = 0; i < path.length; ++i) {
                _vertices[i * 5] = path[i].x;
                _vertices[i * 5 + 1] = path[i].y;
            }
            _hasBoundsChanged = true;
            triangulate();
        }

        /**
         * Alternative method to make shape from vector of numbers arranged in pattern:
         * [x,y,x,y,x,y,...]
         * @param path
         */
        
        public function setPath2(path:Array<Float>):Void {
            setVertexCount(path.length / 2);
            for (var i:Int = 0; i < path.length; i+=2) {
                _vertices[i/2 * 5] = path[i];
                _vertices[i/2 * 5 + 1] = path[i+1];
            }
            _hasBoundsChanged = true;
            triangulate();
        }

        
        public function setTriangle(id:Int, a:Int, b:Int, c:Int):Void {
            id *= 3;
            _indices[id] = a;
            _indices[id + 1] = b;
            _indices[id + 2] = c;
        }

        /**
         * Maps texture as a rectangle to the shape, generating uvs from vertex position.
         * (u,v) = (scaleX * x + offX, scaleY * y + offY)
         */
        
        public function makePlanarUv(offX:Float=0, offY:Float=0, scaleX:Float=1, scaleY:Float=1, useCustomBounds:Bool = false, boundsX:Float = 0, boundsY:Float = 0, boundsW:Float = 0, boundsH:Float = 0):Void {
         	if (!useCustomBounds) {
				updateBounds();
            	boundsW = _bounds.width;
            	boundsH = _bounds.height;
				boundsX = _bounds.x;
				boundsY = _bounds.y;
			}
            for (var i:Int = 0; i < _vertices.length; i += 5) {
                _vertices[i + 2] = ((_vertices[i] - boundsX) / boundsW) * scaleX + offX;
                _vertices[i + 3] = ((_vertices[i + 1] - boundsY) / boundsH) * scaleY + offY;
            }
        }


        
        public function set textureResource(value:TextureResource):Void {
            _textureResource = value;
        }

        
        public function get textureResource():TextureResource {
            return _textureResource;
        }

        
        public function set material(value :Material):Void {
            _material = value;
        }

        
        public function get material():Material {
            return _material;
        }

        public function updateBounds():Void {
            if (_hasBoundsChanged) {
                _bounds.x = _vertices[0];
                _bounds.y = _vertices[1];
                _bounds.width = 0;
                _bounds.height = 0;
                _hitmask._path.length = 0;
                _hitmask._path.push(new Point(_vertices[0], _vertices[1]));
                for (var i:Int = 5; i < _vertices.length; i += 5) {
                    if (_vertices[i] < _bounds.x) {
                        _bounds.left = _vertices[i];
                    } else if (_vertices[i] > _bounds.right) {
                        _bounds.right = _vertices[i];
                    }
                    if (_vertices[i+1] < _bounds.y) {
                        _bounds.top = _vertices[i+1];
                    } else if (_vertices[i+1] > _bounds.bottom) {
                        _bounds.bottom = _vertices[i+1];
                    }					
                    _hitmask._path.push(new Point(_vertices[i], _vertices[i+1]));
                }
				_radius = Math.max(-_bounds.left, -_bounds.top, _bounds.right, _bounds.bottom);
                _hasBoundsChanged = false;
            }
        }
	
	}

}
