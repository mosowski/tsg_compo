package map;

import ai.*;
import spell.*;
import object.*;

class Populator {

	public static function populateNormal(n:Int):Void {
		while (n-- > 0) {
			var c = Std.random(32);
			var r = Std.random(32);
			var tile = State.normalMap.getTile(c,r);
			if (tile != null && tile.isWalkable()) {
				var r = Std.random(100);
				var char;
				var ai;
				if (r < 15) {
					ai = new Mindless(char = new Character("npc-bomb"));
                    char.addSpell(new Kamikaze());
				} else if (r < 70) {
					ai = new Mindless(char = new Character("npc-ghoul"));
                    char.addSpell(new MeleeAttack(15));
				} else {
					ai = new Mindless(char = new Character("npc-ogre"));
                    char.addSpell(new MeleeAttack(40));
				}
				char.addSpell(new Walk());
				char.setTile(tile);
				State.addAI(ai);

				if (n == 0) { 
					char.addSpell(new OpenHatch());
				}
			} else {
				n++;
			}
		}
	}

	public static function populateHell(n:Int):Void {
		while (n-- > 0) {
			var c = Std.random(32);
			var r = Std.random(32);
			var tile = State.hellMap.getTile(c,r);
			if (tile != null && tile.isWalkable()) {
				var r = Std.random(100);
				var char;
				var ai;
				if (true) { // r < 100) {
					ai = new Mindless(char = new Character("npc-hellbully"));
                    char.addSpell(new MeleeAttack(30));
					char.setHealth(400);
                    char.maxHealth = 400;
					char.actionPoints = 2;
				}
				char.addSpell(new Walk());
				char.setTile(tile);
				State.addAI(ai);
			} else {
				n++;
			}
		}
	}
}

