package fabulous;

import flash.geom.Matrix;
import flash.display.BitmapData;

import openfl.gl.GL;
import openfl.utils.Int32Array;

class UncompressedTexture extends Texture {

    public function new() {
        super();
    }

    /**
     * Uploads bitmap as texture source to GPU. Disposes already uploaded
     * texture; throws exception when trying to upload texture being loaded
     * using texture manager.
     * @param	bitmapData
     */
    public function loadFromBitmapData(bitmapData:BitmapData):Void {
        if (_isLoading) {
            throw "Trying to upload source to texture being loaded.";
        }

        if (_glTexture != null && _glTexture != F.device._textureMgr._unloadedFallbackGLTexture) {
			GL.deleteTexture(_glTexture);
        }

		_glTexture = GL.createTexture();
		GL.bindTexture(GL.TEXTURE_2D, _glTexture);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.NEAREST);
		GL.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.NEAREST);

		_sourceWidth = bitmapData.width;
		_sourceHeight = bitmapData.height;

		if (!(Bits.isPow2(bitmapData.width) && Bits.isPow2(bitmapData.height))) {
			_width = Bits.nextPow2(bitmapData.width);
			_height = Bits.nextPow2(bitmapData.height);

			if (_width > 2048) {
				_width = 2048;
			}
			if (_height > 2048) {
				_height = 2048;
			}

			Log.w("Texture " + _name + " is non-power-of-two ("+bitmapData.width+" x " + bitmapData.height+"). Upscaling to " + _width + " x " + _height);

			var tmpBitmap:BitmapData = new BitmapData(_width, _height, true, 0x0000000);
			tmpBitmap.draw(bitmapData, new Matrix(_width/bitmapData.width, 0, 0, _height/bitmapData.height), null, null, null, true);

			GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, _width, _height, 0, GL.RGBA, GL.UNSIGNED_BYTE, new Int32Array(cast BitmapData.getRGBAPixels(tmpBitmap)));

			tmpBitmap = null;
		} else {
            _width = bitmapData.width;
            _height = bitmapData.height;
			trace("wh", _width, _height);

			GL.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, _width, _height, 0, GL.RGBA, GL.UNSIGNED_BYTE, new Int32Array(cast BitmapData.getRGBAPixels(bitmapData)));
		}

        _isLoaded = true;
        _loaded.post();
    }
}
