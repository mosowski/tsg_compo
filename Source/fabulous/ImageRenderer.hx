package fabulous;

class ImageRenderer extends Renderer {
    public var _buffer:RenderBatchBuffer;

    public function new(device:Device) {
        super(device);
    }

    override public function init():Void {
        _buffer = _device._renderBatcher._quadBuffer;
    }

    override public function processRenderable(renderable:Renderable):Void {
        var image:Image = cast renderable;
		var node:Node = image._node;
		var a:Float = node._derivedMtx.a;
		var b:Float = node._derivedMtx.b;
		var c:Float = node._derivedMtx.c;
		var d:Float = node._derivedMtx.d;
		var tx:Float = node._derivedMtx.tx;
		var ty:Float = node._derivedMtx.ty;
        var alpha:Float = node._derivedAlpha;

        if (image._texture != null) {
            var vb:VertexBuffer = _buffer._vb;

            _device._renderBatcher.queue(_buffer, image._material, image._texture, 6);

            vb.beginWrite();

			vb.writeFloat(image._x00 * a + image._y00 * c + tx);
			vb.writeFloat(image._x00 * b + image._y00 * d + ty);
            vb.writeFloat(image._u00);
            vb.writeFloat(image._v00);
            vb.writeFloat(alpha * image._alpha00);

			vb.writeFloat(image._x01 * a + image._y01 * c + tx);
			vb.writeFloat(image._x01 * b + image._y01 * d + ty);
            vb.writeFloat(image._u01);
            vb.writeFloat(image._v01);
            vb.writeFloat(alpha * image._alpha01);

			vb.writeFloat(image._x10 * a + image._y10 * c + tx);
			vb.writeFloat(image._x10 * b + image._y10 * d + ty);
            vb.writeFloat(image._u10);
            vb.writeFloat(image._v10);
            vb.writeFloat(alpha * image._alpha10);

			vb.writeFloat(image._x11 * a + image._y11 * c + tx);
			vb.writeFloat(image._x11 * b + image._y11 * d + ty);
            vb.writeFloat(image._u11);
            vb.writeFloat(image._v11);
            vb.writeFloat(alpha * image._alpha11);

            vb.endWrite();
        }
    }
}
