package atlas;

import flash.geom.Rectangle;
import flash.display.BitmapData;

import fabulous.Texture;

class AtlasFrame {
	public var rect:Rectangle;
	public var width:Float;
	public var height:Float;
	public var texture:Texture;

	public function new() {
		rect = new Rectangle();
	}
}

