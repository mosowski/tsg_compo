package spell;

class Curse {
	public var turnsLeft:Int;
	public var target:CurseTarget;

	public function new(turnsLeft:Int = -1) {
		this.turnsLeft= turnsLeft;
	}

	public function begin(target:CurseTarget):Bool {
		return false;
	}

	public function apply():Void {
	}

	public function end():Void {
	}
}
