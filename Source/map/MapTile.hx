package map;

import flash.display.Sprite;
import flash.events.MouseEvent;
import atlas.AtlasSprite;

import object.Character;
import object.Item;
import map.WorldMap;

import fabulous.Node;

class MapTile extends Node {

	private var _floorSprite:AtlasSprite;
	private var _wallSprite:AtlasSprite;
	private var _itemsContainer:Node;
	private var _charactersContainer:Node;
	private var _fxsContainer:Node;

    public var _tileset:Tilesets;
    public var column:Int;
    public var row:Int;
	public var alignment:WallAlignment;
    public var worldMap:WorldMap;

	public var items:Array<Item>;
	public var characters:Array<Character>;
    public var wallDamage:Int;
	public var fxs:Map<String,AtlasSprite>;

    public function new(c:Int, r:Int, tileset:Tilesets, alignment:WallAlignment, map:WorldMap) {
		super();
        column = c;
        worldMap = map;
       	row = r;
		this.alignment = alignment;
        _tileset = tileset;
		items = new Array<Item>();
		characters = new Array<Character>();
		fxs = new Map<String,AtlasSprite>();

		_floorSprite = new AtlasSprite(Atlases.get("tileset-"+Std.string(tileset)+"-floor"), "Floor");
		_floorSprite.isTouchEnabled = true;
		_floorSprite.tapped.add(onClicked);
		addChild(_floorSprite);
        if (tileset != Tilesets.End) {
            wallDamage = 0;
            _wallSprite = new AtlasSprite(Atlases.get("tileset-"+Std.string(tileset)+"-wall"), Std.string(alignment)+wallDamage);
            addChild(_wallSprite);
        }
		_itemsContainer = new Node();
		addChild(_itemsContainer);
		_charactersContainer = new Node();
		addChild(_charactersContainer);
		_fxsContainer = new Node();
		addChild(_fxsContainer);

		var randomdmg = Std.random(100);
		if (randomdmg < 20) {
			damage(20);
		} else if (randomdmg < 40) {
			damage(10);
		}
    }

    public function haveItem(itemName:String):Bool {
        for (item in items) {
            if (item.name == itemName) {
                return true;
            }
        }
        return false;
    }

	private function onClicked(e:Dynamic):Void {
		State.tileClicked.post(this);
	}

	public function addCharacter(character:Character):Void {
		characters.push(character);
		_charactersContainer.addChild(character);
	}

	public function removeCharacter(character:Character):Void {
		characters.splice(Lambda.indexOf(characters, character), 1);
		_charactersContainer.removeChild(character);
	}

    public function isWalkable():Bool {
        return alignment == WallAlignment.Empty;
    }

	public function addItem(item:Item):Void {
		items.push(item);
		_itemsContainer.addChild(item);
	}

    public function damage(damage:Int = 10):Void {
		var i:Int = characters.length - 1;
		while (i >= 0) {
			var character = characters[i];
            character.damage(damage);
			i--;
        }
        if (alignment != WallAlignment.Empty && _tileset != Tilesets.Hell && _tileset != Tilesets.End) {
            wallDamage += Std.int(damage/cast(10, Int));
            if (wallDamage > 2) {
                alignment = WallAlignment.Empty;
                wallDamage = 2;
            }
            _wallSprite.play(Std.string(alignment) + wallDamage);
        }
    }

	public function heal(amount:Int = 10):Void {
		for (char in characters) {
			char.addHealth(amount);
		}
	}

	public function removeItem(item:Item):Void {
		items.splice(Lambda.indexOf(items, item), 1);
		_itemsContainer.removeChild(item);
	}

	public function addFx(name:String, fx:AtlasSprite):Void {
		if (!fxs.exists(name)) {
			_fxsContainer.addChild(fx);
			fxs[name] = fx;
		}
	}

	public function removeFx(name:String):Void {
		if (fxs.exists(name)) {
			_fxsContainer.removeChild(fxs[name]);
			fxs.remove(name);
		}
	}

	public function getNeighbour(x:Int,y:Int):MapTile {
		return worldMap.getTile(column + x, row + y);
	}
}
