package fabulous {
    import flash.net.URLLoaderDataFormat;
	
    class TextLoader extends BinaryLoader {

        public function TextLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null) {
            super(device, name, url, onComplete);
            _urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
        }

    }

}