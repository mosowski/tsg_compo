package fabulous;

import flash.Lib;
import flash.display.Stage;

// A static proxy class created to make engine more user friendly
class F {
	public static var device:Device;

	public static var initializer:Initializer;
	
    public static function start(stage:Stage, profile:String, assetRootDir:String, clipsLib:String, fontsLib:String, spriteSheetsLib:String):Void {
		Log.addLogger(new TraceLogger());
		initializer = new Initializer(stage, profile, assetRootDir, clipsLib, fontsLib, spriteSheetsLib);
		device = initializer._device;
	}

	public static var ticked(get,null):Signal;
	public static function get_ticked():Signal {
		return device._ticked;
	}

	public static var exactTime(get,null):Float;
	public static function get_exactTime():Float {
		return Lib.getTimer() / 1000;
	}

	public static var dt(get,null):Float;
	public static function get_dt():Float {
		return device._dt;
	}
	
	public static function log(msg:String):Void {
		Log.i(msg);
	}

	public static var rootNode(get,null):Node;
	public static function get_rootNode():Node {
		return device._rootNode;
	}

	public static var deviceProfile(get,null):String;
	public static function get_deviceProfile():String {
		return device._deviceProfile;
	}

	/*
	public static function get tweener():Tweener {
		return _device.tweener;
	}
	*/

	public static function getTexture(path:String):Texture {
		return device._textureMgr.loadTexture(path);			
	}

	public static function createRenderTargetTexture(name:String, width:Int, height:Int):RenderTargetTexture {
		return device._textureMgr.createRenderTargetTexture(name, width, height);
	}

	public static function disposeTexture(path:String):Void {
		device._textureMgr.disposeTexture(path);
	}

	public static function getSheet(name:String):SpriteSheet {
		return device._spriteSheetMgr.getSheet(name);
	}

	/*
	public static function getClipData(name:String):ClipData {
		return _device.clipDataMgr.getClipData(name);
	}

	public static function getFont(name:String):Font {
		return _device.fontMgr.getFont(name);
	}

	*/

	public static var materialBasic(get,null):MaterialBasic;
    public static function get_materialBasic():MaterialBasic {
        return device._materialMgr._materialBasic;
    }

	public static var materialLight(get,null):MaterialLight;
    public static function get_materialLight():MaterialLight {
        return device._materialMgr._materialLight;
    } 
	
	public static var materialBurned(get,null):MaterialBurned;
    public static function get_materialBurned():MaterialBurned {
        return device._materialMgr._materialBurned;
    }


	/*
    public static function get materialBasicAlphaDefault():MaterialBasicAlpha {
        return _device.materialMgr.materialBasicAlphaDefault;
    }

	public static function getLoader(name:String):BaseLoader {
		return _device.loaderMgr.getLoader(name);
	}

	public static function loadText(url:String, onComplete:Function, loaderName:String = null):Void {
		loaderName ||= url;
		_device.loaderMgr.startTextLoader(loaderName, url, onComplete);
	}
	*/
}
