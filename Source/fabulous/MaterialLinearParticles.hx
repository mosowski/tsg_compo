package fabulous {    
    import flash.display3D.Context3DProgramType;
    import flash.utils.ByteArray;
    import flash.utils.Endian;

    class MaterialLinearParticles extends MaterialParticles {
        public static var _basicShader:Shader;
        public static var _gradientShader:Shader;
        public static var _params:ByteArray;
        public static var _screenConstants:ByteArray;

        public var _gradientTexture:TextureResource;

        public function MaterialLinearParticles() {
            super();
        }

        public static function initShader(device:Device):Void {
            _screenConstants = new ByteArray();
            _screenConstants.endian = Endian.LITTLE_ENDIAN;

            _params = new ByteArray();
            _params.endian = Endian.LITTLE_ENDIAN;
            _params.writeFloat(0);
            _params.writeFloat(0.5);
            _params.writeFloat(0);
            _params.writeFloat(1);

            _basicShader = new Shader(device);
            _basicShader.setCode(
                [
                    // va: @ Particles.as
                    // vc0:xy: screen scale
                    // vc0.zw: screen + node translation
                    // vc1.xyzw: time, 0.5, 0, 1

                    // calculate factor
                    "sub vt0.x, vc1.x, va0.x",
                    "div vt0.x, vt0.x, va0.y",

                    // calculate uv
                    "add v0, va0.zw, vc1.yy",

                    // calculate center pos
                    "mul vt1.xy, va1.zw, vt0.xx",
                    "add vt1.xy, vt1.xy, va1.xy",

                    // calculate angle and rotated corner
                    "mul vt3.x, va2.w, vt0.x",
                    "add vt3.x, vt3.x, va2.z",
                    "sin vt3.y, vt3.x",
                    "cos vt3.z, vt3.x",
                    // x' = cos x - sin y
                    "mul vt4.x, vt3.z, va0.z",
                    "mul vt4.z, vt3.y, va0.w",
                    "sub vt4.x, vt4.x, vt4.z",
                    // y' = cos y + sin x
                    "mul vt4.y, vt3.z, va0.w",
                    "mul vt4.z, vt3.y, va0.z",
                    "add vt4.y, vt4.y, vt4.z",

                    // calculate scaled corner offset
                    "mul vt2.x, va2.y, vt0.x",
                    "add vt2.x, vt2.x, va2.x",
                    "slt vt2.y, vt0.x, vc1.w",
                    "mul vt2.x, vt2.x, vt2.y",
                    "mul vt2.xy, vt2.xx, vt4.xy",

                    "add vt1.xy, vt1.xy, vt2.xy",
                    "mov vt1.zw, vc1.zw",

                    // calculate color
                    "sub vt2.xyzw, va4.xyzw, va3.xyzw",
                    "mul vt2.xyzw, vt2.xyzw, vt0.xxxx",
                    "add vt2.xyzw, vt2.xyzw, va3.xyzw",
                    "mul vt2.xyz, vt2.xyz, vt2.www",
                    "mov v1.xyzw, vt2.xyzw",

                    "mul vt1.xy, vt1.xy, vc0.xy",
                    "add vt1.xy, vt1.xy, vc0.zw",
                    "mov op, vt1",
                ].join("\n"),
                [
                    // v0: vertex uv
                    // v1: color
                    // fs0: texture
                    "tex ft0, v0, fs0 <sampler>",
                    "mul oc, ft0, v1"
                ].join("\n")
            );

            _gradientShader = new Shader(device);
            _gradientShader.setCode(
                _basicShader._vertexProgramCode + "\nmov v2, vt0.xxxx",
                [
                    // v0: vertex uv
                    // v1: color
                    // v2: time
                    // fs0: texture
                    // fs1: gradient 
                    "tex ft0, v0, fs0 <2d, nomip, linear, norepeat>",
                    "tex ft1, v2.xx, fs1 <2d, nomip, linear, norepeat>",
                    "mul ft0, ft0, ft1",
                    "mul oc, ft0, v1"
                ].join("\n")
            );
        }

        
        public function set gradientTexture(value:TextureResource):Void {
            _gradientTexture = value;
        }

        
        public function get gradientTexture():TextureResource {
            return _gradientTexture;
        }

        
        override public function bind(device:Device, batch:RenderBatch):Void {
            device.setTextureAt(0, batch._textureResource);
            if (_gradientTexture) {
                device.setTextureAt(1, _gradientTexture);
                device.setShader(_gradientShader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
            } else {
                device.setShader(_basicShader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
            }
            device.setBlendFactors(_particles._srcBlendFactor,_particles._destBlendFactor);

            _params.position = 0;
            _params.writeFloat(device._currentTime);

            _screenConstants.position = 0;
			_screenConstants.writeFloat(2 / device._screenWidth * _particles._anchorNode._derivedScaleX);
			_screenConstants.writeFloat(-2 / device.screenHeight * _particles._anchorNode._derivedScaleY);
			_screenConstants.writeFloat(-1 + _particles._anchorNode._derivedX * 2/ device._screenWidth);
			_screenConstants.writeFloat(1 + _particles._anchorNode._derivedY * -2 / device.screenHeight);

            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, _screenConstants, 0);
            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 1, 1, _params, 0);
        }
    }

}
