package hud;

import fabulous.F;
import fabulous.Node;
import fabulous.Image;
import fabulous.DebugTextNode;

import flash.events.Event;
import spell.Spell;
import object.Character;
import openfl.Assets;

class Spellbar extends Node {
	public var totalWidth:Int;
	public var totalHeight:Int;

	public var highlight:Node;
	public var icons:Array<SpellbarIcon>;
	public var fillIndex:Int;

	public function new() {
		super();
		totalWidth = 320;
		totalHeight = 40;
		var bgImg:Image = new Image(F.materialBasic);
		bgImg.frame= F.getSheet("sheet").getFrame("spellbar-bg");
		addRenderable(bgImg);
		
		highlight = new Node();
		var highlightImg:Image = new Image(F.materialBasic);
		highlightImg.frame = F.getSheet("sheet").getFrame("spellbar-highlight");
		highlight.addRenderable(highlightImg);
		addChild(highlight);
		highlight.isVisible = false;

		icons = new Array<SpellbarIcon>();
		for (i in 0...8) {
			var icon:SpellbarIcon = new SpellbarIcon();
			icon.x = i * 40 + 4;
			icon.y = 4;
			icons.push(icon);
			addChild(icon);
		}
		fillIndex = 0;
	}

	public function setCharacter(character:Character):Void {
		clearIcons();
		if (character != null) {
			for (i in 0...character.spells.length) {
				addSpell(character.spells[i]);
			}
		}
	}

	public function addSpell(spell:Spell):Void {
		icons[fillIndex].setSpell(spell);
		fillIndex++;
	}

	public function highlightSpell(spell:Spell):Void {
		highlight.isVisible =false;
		if (spell != null) {
			for (i in 0...8) {
				if (icons[i].spell == spell) {
					highlight.x = i*40;
					highlight.isVisible = true;
				}
			}
		}
	}

	public function clearIcons():Void {
		for (i in 0...8) {
			icons[i].clear();
		}
		fillIndex = 0;
	}
}

class SpellbarIcon extends Node {
	public var img:Image;
	public var spell:Spell;
	public var amountText:DebugTextNode;

	public function new() {
		super();
		var bgImg = new Image(F.materialBasic);
		bgImg.frame = F.getSheet("sheet").getFrame("icon-bg");
		addRenderable(bgImg);
		img = new Image(F.materialBasic);
		addRenderable(img);
		isTouchEnabled = true;
		amountText = new DebugTextNode(F.device, 32,32);
		addChild(amountText);
		tapped.add(onClicked);
	}

	public function clear():Void {
		setIcon(null);
		spell = null;
		amountText.text = "";
	}

	private function onClicked(e:Event):Void {
		if (spell != null) {
			State.selectSpell(spell);
		}
	}

	public function setSpell(spell:Spell):Void {
		this.spell = spell;
		setIcon(spell.iconName);
		if (spell.amount >= 0) {
			amountText.text = Std.string(spell.amount);
		} else {
			amountText.text = "-";
		}
	}

	public function setIcon(name:String):Void {
		if (name != null) {
			img.frame = F.getSheet("sheet").getFrame(name);
		} else {
			img.frame = null;
		}
	}
}
