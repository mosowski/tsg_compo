package fabulous {
    import flash.display3D.Context3DBlendFactor;
    import flash.display3D.Context3DProgramType;
    import flash.utils.ByteArray;
    import flash.utils.Endian;

    class MaterialBlend extends Material {
        public static var _shader:Shader;

        public var _blendX:Float;
        public var _blendY:Float;
        public var _blendFalloff:Float;
        public var _blendRadius:Float;
        public var _blendParams:ByteArray;

        public function MaterialBlend(x:Float, y:Float, r:Float) {
            _blendParams = new ByteArray();
            _blendParams.endian = Endian.LITTLE_ENDIAN;
            _blendX = x;
            _blendY = y;
            _blendFalloff = 15;
            _blendRadius = r;
        }

        public static function initShader(device:Device):Void {
            _shader = new Shader(device);
            _shader.setCode(
                [
                    // va0.xy: vertex position
                    // va1.xy: vertex uv
                    // va2.x: vertex alpha
                    // vc0.xy: screen scale
                    // vc0.zw: screen translation
                    "mov vt0, va0",
                    "mul vt0.xy, vt0.xy, vc0.xy",
                    "add vt0.xy, vt0.xy, vc0.zw",
                    "mov op, vt0",
                    "mov v0, va1",
                    "mov v1, va2",
                    "mov v2, va0"
                ].join("\n"),
                [
                    // v0: vertex uv
                    // v1: vertex alpha
                    // v2: vertex pos
                    // fs0: texture
                    // fc0.xy: blend center
                    // fc0.z: blend edge thickness
                    // fc0.w: squared blend radius
                    "tex ft0, v0, fs0 <sampler>",
                    "sub ft1, v2, fc0",
                    "add ft1.z, ft1.z, fc0.z",
                    "dp3 ft1, ft1, ft1",
                    "sqt ft1.x, ft1.x",
                    "sub ft1.x, ft1.x, fc0.w",
                    "div ft1.x, ft1.x, fc0.z",
                    "sat ft1.x, ft1.x",
                    "mul ft1.x, ft1.x, v1.x",
                    "mul oc, ft0, ft1.xxxx"
                ].join("\n")
            );
        }

        
        public function set blendX(value:Float):Void {
            _blendX = value;
        }

        
        public function get blendX():Float {
            return _blendX;
        }

        
        public function set blendY(value:Float):Void {
            _blendY = value;
        }

        
        public function get blendY():Float {
            return _blendY;
        }

        
        public function set blendFalloff(value:Float):Void {
            _blendFalloff = value;
        }

        
        public function get blendFalloff():Float {
            return _blendFalloff;
        }

        
        public function set blendRadius(value:Float):Void {
            _blendRadius = value;
        }

        
        public function get blendRadius():Float {
            return _blendRadius;
        }

        
        override public function bind(device:Device, batch:RenderBatch):Void {
            device.setTextureAt(0, batch._textureResource);
            device.setShader(_shader, ShaderSamplerOptions.TEX_2D_NOMIP_LINEAR);
            device.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA);

            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 1, device._screenConstants, 0);

            _blendParams.position = 0;
            _blendParams.writeFloat(_blendX);
            _blendParams.writeFloat(_blendY);
            _blendParams.writeFloat(_blendFalloff);
            _blendParams.writeFloat(_blendRadius);
            device._ctx.setProgramConstantsFromByteArray(Context3DProgramType.FRAGMENT, 0, 1, _blendParams, 0);
        }
    }

}