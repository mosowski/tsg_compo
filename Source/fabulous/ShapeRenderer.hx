package fabulous {    
	
    class ShapeRenderer extends Renderer {
        public var _buffer:RenderBatchBuffer;

        public function ShapeRenderer(device:Device) {
            super(device);
        }

        override public function init():Void {
            _buffer = _device._renderBatcher._unorderedBuffer;
        }

        override public function processRenderable(renderable:Renderable):Void {
            var shape:Shape = renderable as Shape;
            var x:Int = shape._node._derivedX;
            var y:Int = shape._node._derivedY;
            var scaleX:Float = shape._node._derivedScaleX;
            var scaleY:Float = shape._node._derivedScaleY;
            var alpha:Float = shape._node._derivedAlpha;
            var rotation:Float = shape._node._derivedRotation;

            if (shape._textureResource && shape._vertices.length) {
                var vertexBuffer:ByteArrayVertexBuffer = _buffer._vbs[0] as ByteArrayVertexBuffer;
                var indexBuffer:IndexBuffer = _buffer._ib;

                var i:Int, n:Int;
                _device._renderBatcher.queue(_buffer, shape._material, shape._textureResource, shape._indices.length);
                indexBuffer.beginWrite();
                for (i = 0; i < shape._indices.length; ++i) {
                    indexBuffer.writeIndex(vertexBuffer.vertexPosition + shape._indices[i]);
                }
                indexBuffer.endWrite();

                var sin:Float = Math.sin(rotation);
                var cos:Float = Math.cos(rotation);

                // Note: Expressions within array access operator caused enormous garbage growth on iOS.
                n = shape._vertices.length;
                vertexBuffer.beginWrite();
                var vertexData:Array<Float> = shape._vertices;
                i = 0;
                while (i < n) {
                    vertexBuffer.writeFloat(vertexData[i] * scaleX * cos - vertexData[i+1] * scaleY * sin + x);
                    vertexBuffer.writeFloat(vertexData[i+1] * scaleY * cos + vertexData[i] * scaleX * sin + y);
                    vertexBuffer.writeFloat(vertexData[i+2]);
                    vertexBuffer.writeFloat(vertexData[i+3]);
                    vertexBuffer.writeFloat(vertexData[i + 4] * alpha);
                    i += 5;
                }
                vertexBuffer.endWrite();
            }

        }

    }
}

