package fabulous  {
    import flash.geom.Rectangle;    
	
    class Clip extends Renderable {
        public var _clipData:ClipData;

        public var _animationName:String;
        public var _animationData:ClipAnimationData;
        public var _currentFrame:Float;
        public var _repeats:Int;
        public var _rewindAtEnd:Bool;

        public var _material:Material;

        public var _animationFinished:Signal;
        public var _loopFinished:Signal;
        public var _labelReached:Signal;
        public var _clipDataLoaded:Signal;

        public var _playbackSpeed:Float;

        public static var _renderer:Renderer;

        public function Clip(clipData:ClipData, material:Material) {
            _clipData = clipData;
            _animationFinished = new Signal();
            _loopFinished = new Signal();
            _labelReached = new Signal();
            _clipDataLoaded = new Signal();
            _playbackSpeed = 1.0;

            if (_clipData && !_clipData.isLoaded) {
                _clipData._clipDataLoaded.addOnce(onClipDataLoadedListener);
            }
            _material = material;
        }

        
        override public function get renderer():Renderer {
            return _renderer;
        }

        override public function tick(device:Device):Void {
            if (_repeats != 0) {
                var previousFrame:Float = _currentFrame;
                _currentFrame = (_currentFrame + _playbackSpeed * device._playbackScale) % _animationData._totalFrames;
                var frameFloat:Int = (int)(_currentFrame);
                if (_animationData._labels[frameFloat]) {
                    _labelReached.post(_animationData._labels[frameFloat]);
                }
                if (_currentFrame < previousFrame) {
                    _repeats--;
                    _loopFinished.post();
                    if (_repeats == 0) {
                        _animationFinished.post();
                        if (_rewindAtEnd) {
                            _currentFrame = 0;
                        } else {
                            _currentFrame = _animationData._totalFrames - 1;
                        }
                    }
                }
            }
        }

        public function playAnimation(animationName:String, startFrame:Int = 0, repeats:Int = -1, rewindAtEnd:Bool = false):Void {
            _animationName = animationName;
            _animationData = _clipData.getAnimationData(animationName);
            _currentFrame = startFrame;
            _repeats = repeats;
            _rewindAtEnd = rewindAtEnd;
        }

        
        override public function getHitmask():Hitmask {
            return _animationData._hitmask;
        }

        
        override public function getBounds():Rectangle {
            return _animationData._bounds;
        }
		
		
		override public function get radius():Float {
			return _animationData._radius;
		}

        
        public function get width():Float {
            return _animationData._bounds.width;
        }

        
        public function get height():Float {
            return _animationData._bounds.height;
        }

        
        public function get numMetaNodes():Int {
            return _animationData.numMetaNodes;
        }

        
        public function getMetaNode(index:Int):MetaNode {
            return _animationData.getMetaNode(index, _currentFrame);
        }

        
        public function set playbackSpeed(value:Float):Void {
            _playbackSpeed = value;
        }

        
        public function get playbackSpeed():Float {
            return _playbackSpeed;
        }

        
        public function get totalFrames():Int {
            return _animationData._totalFrames;
        }

        
        public function get currentFrame():Int {
            return (int)(_currentFrame);
        }
		
		public function set currentFrame(v:Int):Void {
			_currentFrame = v;
		}

		public function get repeats():Int {
			return _repeats;
		}
		public function set repeats(v:Int):Void {
			_repeats = v;
		}

        
        public function get clipData():ClipData {
            return _clipData;
        }

        
		public function get clipAnimationData():ClipAnimationData {
			return _animationData;
		}

        public function onClipDataLoadedListener():Void {
            if (_animationName) {
                playAnimation(_animationName, _currentFrame, _repeats, _rewindAtEnd);
            }
            _clipDataLoaded.post();
        }

        
        public function set material(value :Material):Void {
            _material = value;
        }

        
        public function get material():Material {
            return _material;
        }

        
        public function get animationFinished():Signal {
            return _animationFinished;
        }

        
        public function get loopFinished():Signal {
            return _loopFinished;
        }

        
        public function get labelReached():Signal {
            return _labelReached;
        }

        
        public function get clipDataLoaded():Signal {
            return _clipDataLoaded;
        }



    }

}
