package fabulous;

class MaterialManager {
    public var _device:Device;

    public var _materialBasic:MaterialBasic;
    public var _materialBurned:MaterialBurned;
    public var _materialLight:MaterialLight;
   // public var _materialBasicAlphaDefault:MaterialBasicAlpha;

    public function new(device:Device) {
        _device = device;
    }

    public function init():Void {
        MaterialBasic.initShader();
        _materialBasic = new MaterialBasic();
        MaterialBurned.initShader();
        _materialBurned = new MaterialBurned();
        MaterialLight.initShader();
        _materialLight = new MaterialLight();
/*
        MaterialBasicAlpha.initShader(_device);
        _materialBasicAlphaDefault = new MaterialBasicAlpha();

        MaterialTexSubConst.initShader(_device);
		MaterialMul.initShader(_device);
		MaterialSaturate.initShader(_device);
		MaterialGrayscale.initShader(_device);
		MaterialHSL.initShader(_device);
        MaterialBlend.initShader(_device);
		MaterialDistanceField.initShader(_device);
		MaterialDistanceFieldOutline.initShader(_device);
        MaterialLinearParticles.initShader(_device);
        MaterialMaskedWaves.initShader(_device);
	*/
    }

    
	public var materialBasic(get,null):MaterialBasic;
    public function get_materialBasic():MaterialBasic {
        return _materialBasic;
    }

    /*
	public var materialBasicAlpha(get,null):MaterialBasicAlpha;
    public function get_materialBasicAlpha():MaterialBasicAlpha {
        return _materialBasicAlphaDefault;
    }
	*/
}
