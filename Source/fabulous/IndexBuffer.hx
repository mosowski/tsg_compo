package fabulous;

import openfl.gl.GL;
import openfl.gl.GLBuffer;

import openfl.utils.Int16Array;

class IndexBuffer {
    public var _device:Device;
    public var _buffer:GLBuffer;
    public var _numIndices:Int;
    public var _changeRangeBegin:Int;
    public var _changeRangeEnd:Int;
    public var _data:Int16Array;
    private var _position:Int;

    public static var MAX_SIZE:Int = 0xFFFF;

    public function new(device:Device) {
        _device = device;
    }

    public function setSize(numIndices:Int):Void {
        if (_buffer != null) {
			GL.deleteBuffer(_buffer);
        }
        _numIndices = numIndices;
		_buffer = GL.createBuffer();

        _data = new Int16Array(numIndices);
		var i:Int = 0, v:Int = 0;
		while (i < _numIndices - 6) {
            _data[i + 0] = v + 0;
            _data[i + 1] = v + 1;
            _data[i + 2] = v + 2;
            _data[i + 3] = v + 1;
            _data[i + 4] = v + 2;
            _data[i + 5] = v + 3;
			i += 6;
			v += 4;
        }
		_changeRangeBegin = 0;
        _changeRangeEnd = numIndices;
        //upload();
    }

    
    public function upload():Void {
        if (_changeRangeBegin < _changeRangeEnd) {
			GL.bufferData(GL.ELEMENT_ARRAY_BUFFER, _data, GL.DYNAMIC_DRAW);
            _changeRangeBegin = _numIndices;
            _changeRangeEnd = 0;
        }
    }

    
    public function beginWrite():Void {
        if (_position < _changeRangeBegin) {
            _changeRangeBegin = _position;
        }
    }

    
    public function endWrite():Void {
        if (_position > _changeRangeEnd) {
            _changeRangeEnd = _position;
        }
    }

    
    public function writeIndex(value:Int):Void {
        _data[_position] = value;
        ++_position;
    }

    
	public var position(get,set):Int;
    public function set_position(value:Int):Int {
        return _position = value;
    }
    public function get_position():Int {
        return _position;
    }

}
