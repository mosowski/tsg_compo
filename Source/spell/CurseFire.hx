package spell;

import atlas.AtlasSprite;

class CurseFire extends Curse {

	public var fx:AtlasSprite;
	public var damage:Int;

	public function new(dmg:Int=30) {
		super();
		damage = dmg;
		turnsLeft = 5;
	}

	override public function begin(target:CurseTarget):Bool {
		this.target = target;
		if (target.tile != null && target.tile.isWalkable()) {
			fx = new AtlasSprite(Atlases.get("fx-fire"), "idle");
			target.tile.addFx("fire", fx);
			return true;
		} else {
			return false;
		}
	}

	override public function apply():Void {
        for (character in target.tile.characters) {
            character.damage(damage);
        }
	}

	override public function end():Void {
		fx.animationFinished.addOnce(onFxFinished);
		fx.play("end");
	}

	private function onFxFinished(_:Dynamic):Void {
		target.tile.removeFx("fire");
	}
}
