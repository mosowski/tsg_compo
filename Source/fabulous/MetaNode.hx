package fabulous {
	
    class MetaNode {
        public var _name:String;
        public var _x:Float;
        public var _y:Float;
        public var _scaleX:Float;
        public var _scaleY:Float;
        public var _visible:Bool;

        public function MetaNode() {
        }

        
        public function get name():String {
            return _name;
        }

        
        public function get x():Float {
            return _x;
        }

        
        public function get y():Float {
            return _y;
        }

        
        public function get scaleX():Float {
            return _scaleX;
        }

        
        public function get scaleY():Float {
            return _scaleY;
        }

        
        public function get isVisible():Bool {
            return _visible;
        }

    }

}