package fabulous {
	
    class MetaNodeData {
        public var _name:String;
        public var _keyFramesFloats:Array<int>;
        public var _keyFrames:Array<MetaNode>;

        public function MetaNodeData() {
        }

        public function fromDynamic(object:Dynamic):Void {
            _name = object.name;
            _keyFramesFloats = Array<int>(object.keyFramesFloats);
            _keyFrames = new Array<MetaNode>(_keyFramesFloats.length, true);
            for (var i:Int = 0; i < _keyFramesFloats.length; ++i) {
                var metaNode:MetaNode = _keyFrames[i] = new MetaNode();
                metaNode._name = _name;
                metaNode._x = object.x[i];
                metaNode._y = object.y[i];
                metaNode._scaleX = object.scaleX[i];
                metaNode._scaleY = object.scaleY[i];
                metaNode._visible = object.visible[i] as Bool;
            }
        }

        
        public function getMetaNodeForFrame(frameNo:Int):MetaNode {
            for (var i:Int = _keyFramesFloats.length - 1; i >= 0; --i) {
                if (frameNo >= _keyFramesFloats[i]) {
                    return _keyFrames[i];
                }
            }
            return _keyFrames[_keyFramesFloats.length - 1];
        }

    }

}