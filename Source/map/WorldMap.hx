package map;

import fabulous.Node;

import openfl.Assets;

class WorldMap {
    private var map:Array<MapTile>;
	public var sprite:Node;
    public var rowNo:Int;
    public var columnNo:Int;

	public var pixelsWidth:Float;
	public var pixelsHeight:Float;

    private var columnOffsets:Array<Int>;
    private var rowOffsets:Array<Int>;

    public function new(tileset:Tilesets, path:String) {
        columnOffsets = [0, 0, 1, -1];
        rowOffsets = [1, -1, 0, 0];
        var jsonString:String = Assets.getText(path);
        var json:Dynamic = haxe.Json.parse(jsonString);
        var tileArray:Array<Dynamic> = Reflect.field(json, "map");
        rowNo = Reflect.field(json, "noOfRows");
        columnNo = Reflect.field(json, "noOfColumns");

		sprite = new Node();

        tileArray.sort(function(x:Dynamic, y:Dynamic):Int {
                var columnX:Int = Reflect.field(x, "column");
                var columnY:Int = Reflect.field(y, "column");
                return columnX - columnY;
            });

        tileArray.sort(function(x:Dynamic, y:Dynamic):Int {
                var rowX:Int = Reflect.field(x, "row");
                var rowY:Int = Reflect.field(y, "row");
                return rowX - rowY;
            });

        map = new Array<MapTile>();

        for (tile in tileArray) {
            var column:Int = Reflect.field(tile, "column");
            var row:Int = Reflect.field(tile, "row");
            var alignmentString:String = Reflect.field(tile, "alignment");
            var occupied:Bool = Reflect.field(tile, "wall");
            var alignment:WallAlignment = WallAlignment.Empty;
            if (occupied) {
                alignment = Type.createEnum(WallAlignment, alignmentString);
            }
            map.push(new MapTile(column, row, tileset, alignment, this));
        }

		for (tile in map) {
			sprite.addChild(tile);
			tile.x = tile.column * 32;
			tile.y = tile.row * 32;
		}
		pixelsWidth = columnNo * 32;
		pixelsHeight = rowNo * 32;
    }

    public function getTile(col:Int, row:Int) {
		if (col >= 0 && col < columnNo && row >= 0 && row < rowNo) {
        	return map[row * columnNo + col];
		} else {
			return null;
		}
    }

    public function getRandomFreeTile() {
        do {
            var rand:Int = Std.int(Math.random() * (map.length - 1));
            if (map[rand].isWalkable()) {
                return map[rand];
            }
        } while (true);
    }

    public function getPath(src:MapTile, dst:MapTile):Array<MapTile> {
        var queue:Array<MapTile> = new Array<MapTile>();
        var visited:Map<MapTile, Int> = new Map<MapTile, Int>();

        queue.push(src);
        var cost:Int = 0;
        while(queue.length > 0) {
            var currentNode:MapTile = queue.shift();
            if (currentNode == dst) {
                visited[dst] = cost;
                queue.splice(0, queue.length);
            }
            else {
                visited[currentNode] = cost;
                cost++;
                var i:Int = 0;
                for (i in 0...columnOffsets.length) {
                    var neighbour:MapTile = getTile(currentNode.column + columnOffsets[i], currentNode.row + rowOffsets[i]);
                    if (neighbour.isWalkable() && null == visited[neighbour]) {
                        queue.push(neighbour);
                    }
                }
            }
        }

        var path:Array<MapTile> = new Array<MapTile>();
        path.push(dst);
        while (path[path.length - 1] != src) {
            var currentNode:MapTile = path[path.length - 1];
            var lowestCost:Int = visited[currentNode];
            var nextElement:MapTile = null;
            for (i in 0...columnOffsets.length) {
                var neighbour:MapTile = getTile(currentNode.column + columnOffsets[i], currentNode.row + rowOffsets[i]);
                if (visited[neighbour] < lowestCost) {
                    nextElement = neighbour;
                    lowestCost = visited[neighbour];
                }
            }
            path.push(nextElement);
        }
        path.reverse();

        return path;
    }
}
