package fabulous;

import openfl.gl.GL;

class MaterialLight extends Material {
    public static var _shader:Shader;

	public var _lightPosition:Array<Float>;

    public function new() {
		super();
		_lightPosition= [0.0, 0.0, 1.0];
    }
	
    public static function initShader():Void {
        _shader = new Shader();

		_shader.setVertexProgram([
			"attribute vec2 a_pos;",
			"attribute vec2 a_uv0;",

			"uniform vec4 u_screen;",

			"varying vec2 v_uv0;",
			"varying vec2 v_pos;",

			"void main(void) {",
			"	gl_Position = vec4(a_pos.xy * u_screen.xy + u_screen.zw, 0.0, 1.0);",
			"	v_pos = a_pos.xy;",
			"	v_uv0 = a_uv0;",
			"}"
		].join("\n"));

		_shader.setFragmentProgram([
			"#ifdef GL_ES",
			"precision mediump float;",
			"#endif",

			"varying vec2 v_uv0;",
			"varying vec2 v_pos;",

			"uniform sampler2D u_sampler0;",
			"uniform sampler2D u_sampler1;",
			"uniform vec3 u_lpos;",

			"void main(void) {",
			"	vec4 tex = texture2D(u_sampler0, v_uv0);",
			"	vec2 nrm = (texture2D(u_sampler1, v_uv0).xy -vec2(0.5,0.5)) * 2.0;",
			"	vec2 ray = (v_pos.xy / u_lpos.z) - u_lpos.xy;",
			"	float d = length(ray);",
			"	float g = min(max(dot(normalize(ray), nrm), 0.0), 1.0);",
			"	float a = clamp(pow(128.0 / d, 3.0), 0.0, 100.0);",
			"	float f = clamp(g * a, 0.75, 2.0);",
			"	gl_FragColor = vec4(f * tex.xyz, tex.w);",
			"}"
		].join("\n"));

		_shader.compile();
    }

    
    override public function bind(device:Device, batch:RenderBatch):Void {
        device.setTextureAt(0, batch._texture);
        device.setTextureAt(1, device._textureMgr.loadTexture(StringTools.replace(StringTools.replace(batch._texture._name,"page1.png", "page1_norm.png"), "assets/", "")));
        device.setBlendFactors(GL.ONE, GL.ONE_MINUS_SRC_ALPHA);
		device.setShader(_shader);

		device.setVertexAttrib("a_pos");
		device.setVertexAttrib("a_uv0");

		GL.uniform1i(_shader.getUniform("u_sampler0"), 0);
		GL.uniform1i(_shader.getUniform("u_sampler1"), 1);
		GL.uniform4fv(_shader.getUniform("u_screen"), device._screenConstants);
		GL.uniform3fv(_shader.getUniform("u_lpos"), _lightPosition);
    }
}
