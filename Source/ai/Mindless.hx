package ai;

import spell.Walk;
import spell.MeleeAttack;
import spell.Kamikaze;
import object.Character;
import fabulous.F;
import spell.SpellTarget;
import map.MapTile;

class Mindless extends AI {

    private var columnOffsets:Array<Int>;
    private var rowOffsets:Array<Int>;

	public function new(character:Character) {
        columnOffsets = [0, 0, 1, -1, 0];
        rowOffsets = [1, -1, 0, 0, 0];
		super(character);
	}

	override public function think(_:Dynamic):Void {
		if (character != null) {
			var walk:Walk = cast character.getSpellByType(Walk);
			var attack:MeleeAttack = cast character.getSpellByType(MeleeAttack);
			var kamikaze:Kamikaze = cast character.getSpellByType(Kamikaze);
			if (kamikaze != null) {
				var hasVictim = false;
            	for (i in 0...columnOffsets.length) {
                	var neighbour:MapTile = character.tile.getNeighbour(columnOffsets[i], rowOffsets[i]);
					for (char in neighbour.characters) {
						if (char.originalOwner != null) {
							hasVictim = true;
						}
					}
				}
				if (hasVictim) {
                    performSpell(kamikaze, character.tile);
                    return;
				}
			}

			if (attack != null) {
            	for (i in 0...columnOffsets.length) {
                	var neighbour:MapTile = character.tile.getNeighbour(columnOffsets[i], rowOffsets[i]);
                	var target:SpellTarget = new SpellTarget(character, neighbour);
                	if (neighbour.characters.length > 0 && attack.canApply(target)) {
                    	performSpell(attack, neighbour);
                    	return;
                	}
            	}
			}

			if (walk != null) {
				var isNearPlayer = false;
				for (player in State.players) {
					if (player.character.tile.worldMap == character.tile.worldMap) {
						if (Math.abs(player.character.tile.row - character.tile.row) <= 5 &&
							Math.abs(player.character.tile.column - character.tile.column) <= 5) {
							isNearPlayer = true;
						}
					}
				}
				if (isNearPlayer) {
					// totally mindless ai lol
					for (i in 0...10) {
						var tx = Std.random(3) - 1;
						var ty = Std.random(3) - 1;
						if (tx != 0 || ty != 0) {
							var tile = character.tile.getNeighbour(tx,ty);
							if (tile != null && tile.isWalkable()) {
								performSpell(walk,  tile);
							}
						}
					}
				} else {
					skipTurn();
				}
			}
		}
	}
}
