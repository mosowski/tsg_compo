package fabulous;

class TweenItem {
    public var _next:TweenItem;

    public var _active:Bool;
    public var _id:Int;
    public var _object:Dynamic;
    public var _props:Array<String>;
    public var _from:Dynamic;
    public var _diff:Dynamic;
    public var _startTime:Float;
    public var _duration:Float;
    public var _ease:Float->Float;
    public var _onComplete:Void->Void;

    public function new() {
        _next = null;
        _active = false;
        _object = null;
        _props = [];
    }

    public function stop():Void {
        _onComplete = null;
        _active = false;
        _object = null;
        _ease = null;
    }
}

