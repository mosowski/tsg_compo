package atlas;

import flash.display.Sprite;
import flash.display.Bitmap;

import fabulous.Node;
import fabulous.Image;
import fabulous.F;

class AtlasSprite extends Node {
	private var _atlas:Atlas;
	private var _animation:AtlasAnimation;
	private var _currentTime:Float;
	private var _currentFrame:Int;
	private var _image:Image;
	private var _playbackSpeed:Float;
	
	public var animationFinished:Signal;

	public function new(atlas:Atlas, animationName:String = null, animationFrame:Int = 0) {
		super();
		_image = new Image(F.materialLight);
		// _image = new Image(F.materialBasic);
		addRenderable(_image);

		_atlas = atlas;
		if (animationName != null) {
			play(animationName);
		} else {
			_animation = null;
			_currentTime = 0;
			_currentFrame = 0;
		}
        animationFinished = new Signal();
		Timers.animation.add(tick);
	}

	public var atlas(get,set):Atlas;
	public function set_atlas(v:Atlas):Atlas {
		_atlas = v;
		_animation = null;
		return v;
	}
	public function get_atlas():Atlas {
		return _atlas;
	}

	public function play(name:String, frame:Int = 0, speed:Float = 1):Void {
		_animation = _atlas.animations[name];
		if (_animation == null) {
			trace("no animation " + name);
		}
		_currentTime = 0;
		_currentFrame = frame;
		_playbackSpeed = speed;
	}

	public var animationTime(get,null):Float;
	public function get_animationTime():Float {
		return _animation.frames.length * 0.1;
	}

	public function stop():Void {
		_playbackSpeed = 0;
	}

	public function dispose():Void {
		Timers.animation.remove(tick);
	}

	public function tick(_:Dynamic):Void {
		_currentTime += 0.1 * _playbackSpeed;
		if (_currentTime > 0.1) {
			_currentFrame = (_currentFrame + 1) % _animation.frames.length;
            if (_currentFrame + 1 >= _animation.frames.length) {
                animationFinished.post();
            }
			_currentTime -= 0.1;
		}
		var frame = _animation.frames[_currentFrame];
		_image.texture = frame.texture;
		_image.setUVRect(frame.rect.x, frame.rect.y, frame.rect.width, frame.rect.height);
		_image.setShapeRect(0, 0, frame.width, frame.height);

		if (Math.abs(_derivedMtx.tx/Layers.scale + 16 - F.materialLight._lightPosition[0]) < 80 &&
			Math.abs(_derivedMtx.ty/Layers.scale + 16 - F.materialLight._lightPosition[1]) < 80) {
			_image.material = F.materialLight;
		} else {
			_image.material = F.materialBurned;
		}
	}
}


