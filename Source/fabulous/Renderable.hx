package fabulous;

import flash.geom.Rectangle;    

class Renderable {
    public var _node:Node;

    public function new():Void {
    }

	public var radius(get, null):Float;
	public function get_radius():Float {
		return 0;
	}

	public var hitmask(get, null):Hitmask;
    public function get_hitmask():Hitmask {
        return null;
    }

    public function tick(device:Device):Void {
    }

	public var renderer(get, null):Renderer;
    public function get_renderer():Renderer {
        throw "Not implemented";
		return null;
    }
}
