package fabulous {
	import flash.utils.getTimer;
	
	class LoaderQueue {
		private static const TIME_LIMIT:Int = 400;
		
		public var _queue:Array<BaseLoader>;
		public var _device:Device;
		
		public function LoaderQueue( device:Device ) {
			_queue = new Array<BaseLoader>();
			_device = device;
		}
		
		public function add( loader:BaseLoader ):Void {
			if ( !_queue.length ) {
				_device._ticked.add( processQueue );
			}
			
			if ( _queue.indexOf( loader ) == -1 ) {
                if ( loader is BitmapLoader ) {
                    _queue.unshift(loader);
                }
                else {
                    _queue.push(loader);
                }
			}
		}
		
		public function processQueue():Void {
			var start:Int = getTimer();
			var duration:Int;
			for ( var i:Int = 0; i < _queue.length && duration < TIME_LIMIT; i++ ) {
				var loader:BaseLoader = _queue.shift();
				loader.load();
				duration = getTimer() - start;
                i--;
			}
			
			if ( !_queue.length ) {
                _device.ticked.remove( processQueue );
			}
		}
	}
}
