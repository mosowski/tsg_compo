package fabulous;
import openfl.Assets;

import flash.display.Stage;

class Initializer {
    public var _stage:Stage;
    public var _device:Device;

    public var _profile:String;
    public var _assetRootDir:String;
    public var _clipsLibraryPath:String;
    public var _fontsLibraryPath:String;
    public var _spriteSheetsLibraryPath:String;

    public function new(stage:Stage, profile:String, assetRootDir:String, clipsLib:String, fontsLib:String, spriteSheetsLib:String) {
        _stage = stage;

        _profile = profile;
        _assetRootDir = assetRootDir;
        _clipsLibraryPath = clipsLib;
        _fontsLibraryPath = fontsLib;
        _spriteSheetsLibraryPath = spriteSheetsLib;

		_device = new Device(stage);
        _device._deviceProfile = _profile;
        _device._assetRootDir = _assetRootDir;
        trace(Assets.getText(_device.getPath(spriteSheetsLib)));
        _device._spriteSheetMgr.loadConfigFromObject(haxe.Json.parse(Assets.getText(_device.getPath(spriteSheetsLib))));

		/*
        if (_clipsLibraryLoader) {
            _device.clipDataMgr.loadClipDataConfigFromDynamic(_clipsLibraryLoader.getJSON());
        }
        if (_fontsLibraryLoader) {
            _device.fontMgr.loadFontsConfigFromDynamic(_fontsLibraryLoader.getJSON());
        }
        if (_spriteSheetsLibraryLoader) {
            _device.spriteSheetMgr._loaded.add(onSpriteSheetsLoaded);
        } else {
            _completed.post();
        }
		*/
    }
}
