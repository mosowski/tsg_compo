package fabulous {
    class ParticleSpawnerUniformBehaviour implements ParticleSpawnerBehaviour {

        public var sMinR:Float;
        public var sMaxR:Float;
        public var sMinG:Float;
        public var sMaxG:Float;
        public var sMinB:Float;
        public var sMaxB:Float;
        public var sMinA:Float;
        public var sMaxA:Float;

        public var sMinSize:Float;
        public var sMaxSize:Float;

        public var eMinR:Float;
        public var eMaxR:Float;
        public var eMinG:Float;
        public var eMaxG:Float;
        public var eMinB:Float;
        public var eMaxB:Float;
        public var eMinA:Float;
        public var eMaxA:Float;

        public var eMinSize:Float;
        public var eMaxSize:Float;

        public var minLife:Float;
        public var maxLife:Float;
        public var minAngle:Float;
        public var maxAngle:Float;
        public var minVel:Float;
        public var maxVel:Float;
        public var minRotation:Float;
        public var maxRotation:Float;
        public var minOmega:Float;
        public var maxOmega:Float;

        public var _life:Float;
        public var _startX:Float;
        public var _startY:Float;
        public var _deltaX:Float;
        public var _deltaY:Float;
        public var _startSize:Float;
        public var _deltaSize:Float;
        public var _startRotation:Float;
        public var _deltaRotation:Float;
        public var _startColor:Int;
        public var _endColor:Int;

        public var _lastOriginX:Float;
        public var _lastOriginY:Float;
		// used for particles initial position interpolation
        public var _originStepX:Float;
        public var _originStepY:Float;
		// used by spawners to control single particle spawn position
        public var _originOffsetX:Float;
        public var _originOffsetY:Float;
		// single particle interpolated initial position
        public var _spawnerX:Float;
        public var _spawnerY:Float;

        public function ParticleSpawnerUniformBehaviour() {
            _originOffsetX = 0;
            _originOffsetY = 0;
            _lastOriginX = 0;
            _lastOriginY = 0;
            _spawnerX = 0;
            _spawnerY = 0;
        }

        public function tick(particles:Particles, numParticles:Float):Void {
			var originX:Float = (particles._node._derivedX - particles._anchorNode._derivedX)/particles._anchorNode._derivedScaleX;
			var originY:Float = (particles._node._derivedY - particles._anchorNode._derivedY)/particles._anchorNode._derivedScaleY;

            if (numParticles > 0) {
                _originStepX = (originX - _lastOriginX) / numParticles;
                _originStepY = (originY - _lastOriginY) / numParticles;
                _spawnerX = _lastOriginX;
                _spawnerY = _lastOriginY;
            } else {
                _spawnerX = originX;
                _spawnerY = originY;
            }
            _lastOriginX = originX;
            _lastOriginY = originY;
        }

        public function spawn():Void {
            var angle:Float = minAngle + Math.random() * (maxAngle - minAngle);
            var vel:Float = minVel + Math.random() * (maxVel - minVel);

            _life = minLife + Math.random() * (maxLife - minLife);
            _startX = _spawnerX + _originOffsetX;
            _startY = _spawnerY + _originOffsetY;
            _deltaX = Math.sin(angle) * _life * vel;
            _deltaY = Math.cos(angle) * _life * vel;

            _spawnerX += _originStepX;
            _spawnerY += _originStepY;

            _startSize = sMinSize + Math.random() * (sMaxSize - sMinSize);
            _deltaSize = eMinSize + Math.random() * (eMaxSize - eMinSize) - _startSize;

            _startRotation = minRotation + Math.random() * (maxRotation - minRotation);
            _deltaRotation = (minOmega + Math.random() * ( maxOmega - minOmega)) * _life;

            var sColorFactor:Float = Math.random();
            var eColorFactor:Float = Math.random();
            var sR:Float = sMinR + sColorFactor * (sMaxR - sMinR);
            var sG:Float = sMinG + sColorFactor * (sMaxG - sMinG);
            var sB:Float = sMinB + sColorFactor * (sMaxB - sMinB);
            var sA:Float = sMinA + sColorFactor * (sMaxA - sMinA);
            var eR:Float = eMinR + eColorFactor * (eMaxR - eMinR);
            var eG:Float = eMinG + eColorFactor * (eMaxG - eMinG);
            var eB:Float = eMinB + eColorFactor * (eMaxB - eMinB);
            var eA:Float = eMinA + eColorFactor * (eMaxA - eMinA);

            _startColor = Int(sR * 255) | (Int(sG * 255) << 8) | (Int(sB * 255) << 16) | (Int(sA * 255) << 24);
            _endColor = Int(eR * 255) | (Int(eG * 255) << 8) | (Int(eB * 255) << 16) | (Int(eA * 255) << 24);
        }


        
        public function get life():Float {
            return _life;
        }

        
        public function get startX():Float {
            return _startX;
        }

        
        public function get startY():Float {
            return _startY;
        }

        
        public function get deltaX():Float {
            return _deltaX;
        }

        
        public function get deltaY():Float {
            return _deltaY;
        }

        
        public function get startSize():Float {
            return _startSize;
        }

        
        public function get deltaSize():Float {
            return _deltaSize;
        }

        
        public function get startRotation():Float {
            return _startRotation;
        }

        
        public function get deltaRotation():Float {
            return _deltaRotation;
        }

        
        public function get startColor():Int {
            return _startColor;
        }

        
        public function get endColor():Int {
            return _endColor;
        }

    }

}
