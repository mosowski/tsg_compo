package spell;

class MeleeAttack extends Spell {
    private var damage:Int;
    public function new(damage:Int) {
        super("icon-fist", -1);
        this.damage = damage;
        cost = 1;
		displayName = "Melee";
    }

    override public function canApply(target:SpellTarget):Bool {
		if (super.canApply(target)) {
			if (Math.abs(target.tile.column - target.caster.tile.column) + Math.abs(target.tile.row - target.caster.tile.row) < 2) {
				var npcVsNpc= false;
				for (char in target.tile.characters) {
					npcVsNpc = npcVsNpc || ((target.caster.aiOwner != null) && (char.aiOwner != null));
				}
				return !npcVsNpc;
			}
		}
		return false;
	}

    override public function apply(target:SpellTarget):Void {
		State.extendAnimationDelay(1);
		if (target.tile.characters.length > 0) {
        	target.tile.damage(damage);
		} else {
			target.tile.damage(10);
		}
    }
}
