package fabulous;

class Glyph {
    public var _x:Int;
    public var _y:Int;
    public var _width:Int;
    public var _height:Int;
    public var _xoffset:Int;
    public var _yoffset:Int;
    public var _xadvance:Int;

    public var _uvLeft:Float;
    public var _uvTop:Float;
    public var _uvRight:Float;
    public var _uvBottom:Float;

    public function new(x:Int, y:Int, w:Int, h:Int, xoffset:Int, yoffset:Int, xadvance:Int) {
        _x = x;
        _y = y;
        _width = w;
        _height = h;
        _xoffset = xoffset;
        _yoffset = yoffset;
        _xadvance = xadvance;
        _uvLeft = 0;
        _uvTop = 0;
        _uvRight = 0;
        _uvBottom = 0;
    }

}
