package fabulous;
	
class TraceLogger extends Logger {

    public function new() {
		super();
    }
    
    override public function print(msg:String):Void {
        trace(msg);
    }
}
