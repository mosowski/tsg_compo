package fabulous;

import flash.Lib;
import flash.display.Stage;
import flash.geom.Rectangle;
import flash.events.Event;    
import flash.events.MouseEvent;
import flash.events.TouchEvent;    
import flash.utils.ByteArray;    
import flash.utils.Endian;

import openfl.display.OpenGLView;
import openfl.utils.Float32Array;
import openfl.gl.GLTexture;
import openfl.gl.GL;

class Device {
    public var _stage:Stage;
    public var _screenWidth:Int;
    public var _screenHeight:Int;
	public var _glView:OpenGLView;

    //public var _loaderMgr:LoaderManager;
    public var _textureMgr:TextureManager;
    public var _materialMgr:MaterialManager;
    //public var _clipDataMgr:ClipDataManager;
    public var _fontMgr:FontManager;
    public var _spriteSheetMgr:SpriteSheetManager;

    public var _assetRootDir:String;
    public var _deviceProfile:String;

    public var _renderBatcher:RenderBatcher;

    //public var _clipRenderer:ClipRenderer;
    public var _imageRenderer:ImageRenderer;
    //public var _spriteRenderer:SpriteRenderer;
    public var _textRenderer:TextRenderer;
    //public var _shapeRenderer:ShapeRenderer;
    //public var _particleRenderer:ParticleRenderer;
    //public var _debugRenderer:DebugRenderer;
    public var _renderers:Array<Renderer>;

    public var _screenConstants:Array<Float>;

    public var _rootNode:Node;
    public var _touches:Map<Int,Touch>;
    public var _numTouches:Int;
	public var _minimalSwipeDistance:Float;

    public var _clearR:Float;
    public var _clearG:Float;
    public var _clearB:Float;
	public var _defaultFramebuffer:Int;

    public var _ticked:Signal;

    public var _lastFrameTime:Float;
    public var _desiredFPS:Float;
    public var _dt:Float;
    public var _playbackScale:Float;
    public var _currentTime:Float;

	public var _inputScaleX:Float;
	public var _inputScaleY:Float;

	public var _currentDomainMemoryDynamic:ByteArray;

    public function new(stage:Stage) {
        _stage = stage;
		_glView = new OpenGLView();
		_stage.addChild(_glView);

		_defaultFramebuffer = GL.getParameter(GL.FRAMEBUFFER_BINDING);

        _ticked = new Signal();

        //_loaderMgr = new LoaderManager(this);
        _textureMgr = new TextureManager(this);
        _materialMgr = new MaterialManager(this);
        //_clipDataMgr = new ClipDataManager(this);
        _fontMgr = new FontManager(this);
        _spriteSheetMgr = new SpriteSheetManager(this);

        _rootNode = new Node();

        _renderBatcher = new RenderBatcher(this);

        //Clip._renderer = _clipRenderer = new ClipRenderer(this);
        Image._renderer = _imageRenderer = new ImageRenderer(this);
        //Sprite._renderer = _spriteRenderer = new SpriteRenderer(this);
        Text._renderer = _textRenderer = new TextRenderer(this);
        //DebugSprite._renderer = _debugRenderer = new DebugRenderer(this);
        //Shape._renderer = _shapeRenderer = new ShapeRenderer(this);
        //Particles._renderer = _particleRenderer = new ParticleRenderer(this);
        _renderers = new Array<Renderer>();
        _renderers.push(_imageRenderer);
        _renderers.push(_textRenderer);

        _screenConstants = [0,0,0,0];

        _touches = new Map<Int,Touch>();
		_minimalSwipeDistance = 30;
		_inputScaleX = 1.0;
		_inputScaleY = 1.0;

		setClearColor();

        _currentTime = Lib.getTimer() / 1000;
        _lastFrameTime = _currentTime;
        _desiredFPS = stage.frameRate;
        _dt = 0;
        _playbackScale = 1;

    	_currentGLTextures = new Array<GLTexture>();
    	_currentTextures = new Array<Texture>();
    	_currentReadyTextures = new Array<Texture>();

		_glView.render = tick;

		initEngine();
    }

    public function tick(rect:Rectangle):Void {
		setViewport(Std.int(rect.x), Std.int(rect.y), Std.int(rect.width), Std.int(rect.height));
		
		GL.clearColor(_clearR, _clearG, _clearB, 1);
		GL.clear(GL.COLOR_BUFFER_BIT);

        _currentTime = Lib.getTimer() / 1000;
        _dt = _currentTime - _lastFrameTime;
        _lastFrameTime = _currentTime;
        _playbackScale = _desiredFPS * _dt;

        _ticked.post();
    }

    private function initEngine():Void {
        reshape();

		setupStageInputListeners();

        _textureMgr.init();
        _materialMgr.init();
        //_clipDataMgr.init();
        _fontMgr.init();
        _spriteSheetMgr.init();

        _renderBatcher.init();

        for (renderer in _renderers) {
            renderer.init();
        }
    }

    public function reshape():Void {
        _screenWidth = _stage.stageWidth;
        _screenHeight = _stage.stageHeight;

      	_screenConstants[0] = 2 / _screenWidth;
      	_screenConstants[1]= -2 / _screenHeight;
      	_screenConstants[2]= -1;
      	_screenConstants[3]= 1;
    }

    public function getPath(path:String):String {
        return _assetRootDir + "/" + path;
    }

    public function setupStageInputListeners():Void {
		if (true) {
            _stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
            _stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
            _stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		} 
        _stage.addEventListener(TouchEvent.TOUCH_BEGIN, onTouchBegin);
        _stage.addEventListener(TouchEvent.TOUCH_MOVE, onTouchMove);
        _stage.addEventListener(TouchEvent.TOUCH_END, onTouchEnd);
    }

    private function onMouseDown(event:MouseEvent):Void {
        _stage.dispatchEvent(new TouchEvent(TouchEvent.TOUCH_BEGIN, event.bubbles, event.cancelable, event.localX, event.localY));
    }

    private function onMouseMove(event:MouseEvent):Void {
		if (_numTouches > 0) {
            _stage.dispatchEvent(new TouchEvent(TouchEvent.TOUCH_MOVE, event.bubbles, event.cancelable, event.localX, event.localY));
		}
    }

    private function onMouseUp(event:MouseEvent):Void {
        _stage.dispatchEvent(new TouchEvent(TouchEvent.TOUCH_END, event.bubbles, event.cancelable, event.localX, event.localY));
    }
	
    private function onTouchBegin(event:TouchEvent):Void {
        var touch:Touch = _touches[event.touchPointID] = new Touch(event.touchPointID, event.localX * _inputScaleX, event.localY * _inputScaleY);
		_numTouches++;
        var node:Node = _rootNode.getHitNode(touch._x, touch._y, Node.HITMASK_TOUCH);
		touch._touchedNode = node != null ? node : _rootNode;

        if (touch._touchedNode._touchBegan != null) {
            touch._touchedNode._touchBegan.post(touch);
        }

        node = _rootNode.getHitNode(touch._x, touch._y, Node.HITMASK_SWIPE_X);
		touch._swipedXNode = node != null ? node : _rootNode;

        node = _rootNode.getHitNode(touch._x, touch._y, Node.HITMASK_SWIPE_Y);
		touch._swipedYNode = node != null ? node : _rootNode;
    }

    private function onTouchMove(event:TouchEvent):Void {
        var touch:Touch = _touches[event.touchPointID];
		if (touch != null) {
			touch.addMovePosition(event.localX * _inputScaleX, event.localY * _inputScaleY);

            if (touch._touchedNode._touchMoved != null) {
                touch._touchedNode._touchMoved.post(touch);
            }

			if (touch._swipeStarted) {
            	if (touch._swipedXNode != null && touch._swipedXNode._swipeMoved != null) {
                	touch._swipedXNode._swipeMoved.post(touch);
            	}
            	if (touch._swipedYNode != null && touch._swipedYNode._swipeMoved != null && touch._swipedYNode != touch._swipedXNode) {
                	touch._swipedYNode._swipeMoved.post(touch);
            	}
			} else if (touch._swipeDistance >= _minimalSwipeDistance) {
				touch._swipeStarted = true;
            	if (touch._swipedXNode != null && touch._swipedXNode._swipeBegan != null) {
                	touch._swipedXNode._swipeBegan.post(touch);
            	}
            	if (touch._swipedYNode != null && touch._swipedYNode._swipeBegan != null && touch._swipedYNode != touch._swipedXNode) {
                	touch._swipedYNode._swipeBegan.post(touch);
            	}
			}
		}

    }

    private function onTouchEnd(event:TouchEvent):Void {
        var touch:Touch = _touches[event.touchPointID];

        if (touch._touchedNode._touchEnded != null) {
            touch._touchedNode._touchEnded.post(touch);
        }

        if (touch._swipedXNode != null && touch._swipedXNode._swipeEnded != null) {
            touch._swipedXNode._swipeEnded.post(touch);
        }

        if (touch._swipedYNode != null && touch._swipedYNode._swipeEnded != null && touch._swipedYNode != touch._swipedXNode) {
            touch._swipedYNode._swipeEnded.post(touch);
        }

        if (!touch._swipeStarted && touch._touchedNode._tapped != null) {
            touch._touchedNode._tapped.post(event);
        }

        _numTouches--;
        _touches.remove(event.touchPointID);
    }

    public function setClearColor(r:Float = 0.3, g:Float = 0.4, b:Float = 0.5):Void {
        _clearR = r;
        _clearG = g;
        _clearB = b;
    }

    public function renderScene(root:Node):Void {
        var renderer:Renderer;
        for (renderer in _renderers) {
            renderer.beginFrame();
        }
        _renderBatcher.resetBatching();

        root.updateTransformation();
        renderNode(root);

        for (renderer in _renderers) {
            renderer.endFrame();
        }
        _renderBatcher.flushQueue();
    }

    public function renderNode(node:Node):Void {
        var numRenderables:Int;            
        var renderable:Renderable;
		var radius:Float;
		var isClipped:Bool = false;
        if (node.isVisible) {
            numRenderables = node._renderables.length;
			for (i in 0...numRenderables) {
                renderable = node._renderables[i];
                if (renderable != null) {
                    renderable.tick(this);
                    var renderer:Renderer = renderable.renderer;						
                    // *1.42 (sqrt(2)) is here to clip the maximally rotated renderable (PI/4)
					radius = 1.42 * Math.max(Math.abs(node._derivedMtx.a * renderable.radius), Math.abs(node._derivedMtx.c * renderable.radius));
					if (node._derivedMtx.tx + radius > 0 && node._derivedMtx.tx - radius < _screenWidth
						&& node._derivedMtx.ty + radius > 0 && node._derivedMtx.ty - radius < _screenHeight) {
                        renderer.processRenderable(renderable);
                    } else {
						isClipped = true;
					}
                }
            }

            if (!(node._isClippingChildren && isClipped)) {
				for (i in 0...node._children.length) {
                    renderNode(node._children[i]);
                }
            }
        }
    }

    public var _currentGLTextures:Array<GLTexture>;
    public var _currentTextures:Array<Texture>;
    public var _currentReadyTextures:Array<Texture>;
	public var _currentVertexBuffer:VertexBuffer;
	public var _currentIndexBuffer:IndexBuffer;
    public var _currentShader:Shader;
    public var _sourceBlendFactor:Int;
    public var _destBlendFactor:Int;
	public var _currentRenderTargetTexture:RenderTargetTexture;

    public function resetStateVariables():Void {
		for (i in 0...4) {
            _currentGLTextures[i] = null;
            _currentTextures[i] = null;
        }
        _currentShader = null;
        _sourceBlendFactor = GL.ONE;
        _destBlendFactor = GL.ONE_MINUS_SRC_ALPHA;
		_currentVertexBuffer = null;
		_currentIndexBuffer = null;
		_currentRenderTargetTexture = null;

        GL.blendFunc(_sourceBlendFactor, _destBlendFactor);
    }

	public function setRenderTargetTexture(texture:RenderTargetTexture):Void {
		if (_currentRenderTargetTexture != texture) {
			_currentRenderTargetTexture = texture;
			
			if (texture != null) {
				_currentRenderTargetTexture.bind();
			} else {
				GL.bindFramebuffer(GL.FRAMEBUFFER, null, _defaultFramebuffer);
				GL.clear(GL.COLOR_BUFFER_BIT);
			}
		}
	}

	public function setViewport(x:Int, y:Int, width:Int, height:Int):Void {
		GL.viewport(x, y, width, height);
        _screenWidth = width;
        _screenHeight = height;
      	_screenConstants[0] = 2 / width;
      	_screenConstants[1]= -2 / height;
      	_screenConstants[2]= -1;
      	_screenConstants[3]= 1;
	}


	public function setVertexBuffer(vb:VertexBuffer):Void {
		if (_currentVertexBuffer != vb) {
			_currentVertexBuffer = vb;

			GL.bindBuffer(GL.ARRAY_BUFFER, vb._buffer);
		}
	}

	public function setIndexBuffer(ib:IndexBuffer):Void {
		if (_currentIndexBuffer != ib) {
			_currentIndexBuffer = ib;
			GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, ib._buffer);
		}
	}

	public function setVertexAttrib(name:String):Void {
		var attrib:VertexBufferAttrib = _currentVertexBuffer._formatByName[name];
		var index:Int = _currentShader.getAttrib(name);
		GL.enableVertexAttribArray(index);
		GL.vertexAttribPointer(index, attrib._size, GL.FLOAT, false, _currentVertexBuffer._data32PerVertex * 4, attrib._offset * 4);
	}
    
    public function setTextureAt(samplerIndex:Int, texture:Texture):Void {
		var glTexture:GLTexture = texture != null ? texture._glTexture : null;
        if (_currentGLTextures[samplerIndex] != glTexture) {
            _currentGLTextures[samplerIndex] = glTexture;
            _currentTextures[samplerIndex] = texture;

			GL.activeTexture(GL.TEXTURE0 + samplerIndex);
			GL.bindTexture(GL.TEXTURE_2D, glTexture);
        }
    }

    public function clearSamplers():Void {
		for (i in 0...4) {
			setTextureAt(i, null);
		}
    }

    public function setShader(shader:Shader):Void {
        if (_currentShader != shader) {
            _currentShader = shader;

			GL.useProgram(shader._program);
        }
    }

    
    public function setBlendFactors(src:Int, dest:Int):Void {
        if (_sourceBlendFactor != src || _destBlendFactor != dest) {
            _sourceBlendFactor = src;
            _destBlendFactor = dest;
			
           	GL.blendFunc(src, dest);
        }
    }

}
