package object;

import player.Player;
import flash.display.Sprite;
import atlas.AtlasSprite;
import map.MapTile;
import spell.Spell;
import spell.PerformRitual;
import spell.OpenHatch;
import spell.MindControl;
import ai.AI;
import hud.InfoText;
import map.Tilesets;
import fabulous.F;
import fabulous.Node;
import fabulous.Image;

import fabulous.Node;

class Character extends Node {
    private var _characterSprite:AtlasSprite;
    private var _characterTile:MapTile;
    private var speed:Int;
    private var goalXOffset:Int;
    private var goalYOffset:Int;
    private var destinationTile:Array<MapTile>;
    private var walking:Bool;
	public var isKeyBearer:Bool;
	public var actionPoints:Int;

	public var spells:Array<Spell>;
	public var spellsChanged:Signal;
    public var died:Signal;
    public var originalOwner:Player;
	public var aiOwner:AI;
    public var healthBar:Node;
    public var health_:Node;

    public var health:Int = 100;
    public var maxHealth:Int = 100;

    public function new(characterLook:String) {
        super();
        healthBar = new Node();
        health_ = new Node();
        var healthImg:Image = new Image(F.materialBasic);
        healthImg.frame = F.getSheet("sheet").getFrame("bar");
        health_.addRenderable(healthImg);
        healthBar.addChild(health_);
        healthBar.scaleX = 0.2;
        healthBar.scaleY = 0.2;
        healthBar.y = -5;
        healthBar.x = 5;
		actionPoints = 1;
        addChild(healthBar);

        _characterSprite = new AtlasSprite(Atlases.get(characterLook), "idle");
        addChild(_characterSprite);
        speed = 4;
        walking = false;
        destinationTile = new Array<MapTile>();
		spells = new Array<Spell>();
		spellsChanged = new Signal();
        died = new Signal();

		addSpell(new MindControl());
    }

	public function addSpell(spell:Spell):Void {
        if (Type.getClass(spell) == OpenHatch) {
            makeKeyBearer();
        }
        for (ownSpell in spells) {
            if (Type.getClass(ownSpell) == Type.getClass(spell)) {
                ownSpell.amount += spell.amount;
                return;
            }
        }
        spells.push(spell);
        spell.depleted.add(onSpellDepleted);
        spellsChanged.post(this);

	}

	public function getSpellByType(type:Class<Spell>):Spell {
		for (i in 0...spells.length) {
			if (Std.is(spells[i], type)) {
				return spells[i];
			}
		}
		return null;
	}

	public function setHealth(v:Int):Void {
		health = v;
	}

    public function goToHell():Void {
        setTile(State.hellMap.getTile(1, 1));
        health = 100;
        addSpell(new PerformRitual());
        healthBar.isVisible = true;
        health_.scaleX = 1;
        _characterSprite.play("idle");
    }

    public function addHealth(amount:Int):Void {
        if (health + amount > maxHealth) {
            amount = maxHealth - health;
        }
        health += amount;
		new InfoText("+"+amount +" HP", getSpriteX(), getSpriteY());
        health_.scaleX = health/maxHealth;
    }

    public function damage(damageAmount:Int):Void {
        health -= damageAmount;
		new InfoText("-"+damageAmount +" HP", getSpriteX(), getSpriteY());
        health_.scaleX = health/maxHealth;
        if (health <= 0) {
            healthBar.isVisible = false;
            //setTile(_characterTile);
            x = 0;
            y = 0;
            goalYOffset = 0;
            goalXOffset = 0;
            destinationTile.splice(0, destinationTile.length);
            Timers.animation.remove(updateCharacter);
            walking = false;
            _characterSprite.play("death");
			State.extendAnimationDelay(_characterSprite.animationTime);
            _characterSprite.animationFinished.addOnce(dead);
			trace("Zaraz umre");
        }
    }

	public function makeKeyBearer():Void {
		trace("mamy szczesliwca!");
		isKeyBearer = true;
	}

    public function dead(a:Int):Void {
        
        if (!(null != originalOwner && tile._tileset == Tilesets.Hell)) {
            var item:Item = new Item("spell", "scroll");
			if (isKeyBearer) {
                for (spell in spells) {
                    if (Type.getClass(spell) == OpenHatch) {
                        item.collectable = spell;
                    }
                }
                onSpellDepleted(item.collectable);
				isKeyBearer = false;
			} else {
            	item.collectable = State.getRandomSpell();
			}
            tile.addItem(item);
        }
        if (null != _characterTile) {
            _characterTile.removeCharacter(this);
			_characterTile = null;
        }
        if (null != originalOwner) {
            goToHell();
        }
        died.post();
    }

	private function onSpellDepleted(spell:Spell):Void {
		var i:Int = Lambda.indexOf(spells, spell);
		if (i >= 0) {
			spells.splice(i, 1);
			spell.depleted.remove(onSpellDepleted);
			spellsChanged.post(this);
		}
	}

    public function setTile(newTile:MapTile) {
        if (null != _characterTile) {
            _characterTile.removeCharacter(this);
        }
        this.x = 0;
        this.y = 0;
        newTile.addCharacter(this);
        _characterTile = newTile;
        if (health > 0) {
            for (curse in Spellcaster.curses) {
                if (newTile == curse.target.tile) {
                    curse.apply();
                }
            }
        }
    }

	public function walkDistance(dst:MapTile):Float {
		return _characterTile.worldMap.getPath(_characterTile, dst).length;
	}

    public function walkTo(dst:MapTile):Float {
        destinationTile = _characterTile.worldMap.getPath(_characterTile, dst);
        var returnValue:Float = destinationTile.length * 32/speed * 0.1;
        if (!walking) {
            goToTile();
        }
        return returnValue;
    }

    public function addWaypoint(waypoint:MapTile) {
        destinationTile.push(waypoint);
        if (!walking) {
            goToTile();
        }
    }

    private function goToTile() {
        if (!walking) {
            walking = true;
            Timers.animation.add(updateCharacter);
        }
        var newTile:MapTile = destinationTile[0];
        var columnDiff:Int = newTile.column - _characterTile.column;
        var rowDiff:Int = newTile.row - _characterTile.row;

        if (columnDiff != 0) {
            goalXOffset = columnDiff * 32;
            if (columnDiff < 0) {
                _characterSprite.play("walkLeft");
            }
            else {
                setTile(newTile);
                this.x = -goalXOffset;
                _characterSprite.play("walkRight");
            }
        }
        if (rowDiff != 0) {
            goalYOffset = rowDiff * 32;
            if (rowDiff < 0) {
                _characterSprite.play("walkUp");
            }
            else {
                setTile(newTile);
                this.y = -goalYOffset;
                _characterSprite.play("walkDown");
            }
        }
    }

    public function updateCharacter(notUsed:Dynamic):Void {
        if (goalXOffset < 0) {
            goalXOffset += speed;
            this.x -= speed;
        }
        else if (goalXOffset > 0) {
            goalXOffset -= speed;
            this.x += speed;
        }

        if (goalYOffset < 0) {
            goalYOffset += speed;
            this.y -= speed;
        }
        else if (goalYOffset > 0) {
            goalYOffset -= speed;
            this.y += speed;
        }
        if (goalXOffset + goalYOffset == 0) {
            var currentTile:MapTile = destinationTile.shift();

            if (_characterTile != currentTile) {
                setTile(currentTile);
            }
            if (destinationTile.length > 0) {
                goToTile();
                return;
            }
            walking = false;
            Timers.animation.remove(updateCharacter);
            _characterSprite.play("idle");
            collectSpell();
        }
    }

    private function collectSpell():Void {
        for (item in tile.items) {
            if (item.name == "spell") {
                addSpell(item.collectable);
                tile.removeItem(item);
                return;
            }
        }
    }

	public var tile(get,null):MapTile;
	public function get_tile():MapTile {
		return _characterTile;
	}

	public function getSpriteX():Float {
		return _characterTile.x + this.x;
	}

	public function getSpriteY():Float {
		return _characterTile.y + this.y;
	}
}
