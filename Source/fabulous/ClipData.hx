package fabulous {

    class ClipData {
        public var _name:String;
        public var _animations:Dynamic;

        public var _isLoaded:Bool;

        public var _clipDataLoaded:Signal;

        public function ClipData() {
            _isLoaded =  false;
            _clipDataLoaded = new Signal();
        }

        public function fromDynamic(object:Dynamic, device:Device):Void {
            _name = object.name;

            _animations = { };
            for (var animationName:String in object.animations) {
                var animationData:ClipAnimationData = new ClipAnimationData();
                animationData.fromDynamic(object.animations[animationName], device);

                _animations[animationName] = animationData;
            }

            if (object.name != ClipDataManager.UNLOADED_CLIP_DATA_NAME) {
                _isLoaded = true;
                _clipDataLoaded.post();
            }
        }

        public function getAnimationData(name:String):ClipAnimationData {
            if (_isLoaded) {
                var animationData:ClipAnimationData = _animations[name];
                if (animationData == null) {
                    throw "No animation '" + name + "' in clip data '" + _name + "'.";
                }
                return animationData;
            } else {
                return _animations[ClipDataManager.UNLOADED_ANIMATION_DATA_NAME];
            }
        }

        public function get isLoaded():Bool {
            return _isLoaded;
        }

        public function get animationNames():Array<String> {
            var names:Array<String> = new Array<String>();
            for (var name:String in _animations) {
                names.push(name);
            }
            return names;
        }

        public function get name():String {
            return _name;
        }

        public function get clipDataLoaded():Signal {
            return _clipDataLoaded;
        }
    }

}
