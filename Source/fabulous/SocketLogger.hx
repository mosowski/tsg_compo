package fabulous {
    import flash.events.Event;
    import flash.events.IOErrorEvent;    
    import flash.net.XMLSocket;

    class SocketLogger extends Logger {
        public var _socket:XMLSocket;
        public var _ip:String;
        public var _port:Int;
        public var _queue:Array<String>;

        public function SocketLogger(ip:String, port:Int) {
            _ip = ip;
            _port = port;
            _queue = new Array<String>();
        }

        override public function init():Void {
            _socket = new XMLSocket(_ip, _port);
            _socket.addEventListener(Event.CONNECT, onConnected);
            _socket.addEventListener(IOErrorEvent.IO_ERROR, onError);
        }

        private function onError(e:Event):Void {
            trace("Cannot connect to socket logger host.");
        }

        private function onConnected(e:Event):Void {
            while (_queue.length) {
                print(_queue[0]);
                _queue.shift();
            }
        }

        
        override public function print(msg:String):Void {
            if (_socket.connected) {
                _socket.send(msg);
            } else {
                _queue.push(msg);
            }
        }

    }

}
