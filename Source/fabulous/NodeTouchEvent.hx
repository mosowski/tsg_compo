package fabulous
{
	import flash.events.TouchEvent;


    class NodeTouchEvent extends TouchEvent
    {
        public static const DESTROY:String = "destroy";

        private var node:Node;
        private var _stageX:Float;
        private var _stageY:Float;

        public function NodeTouchEvent(_node:Node, _type:String, x:Float, y:Float)
        {
            super(_type, false);
            node = _node;
            _stageX = x;
            _stageY = y;
        }

        override public function get stageX():Float
        {
            return _stageX;
        }

        override public function get stageY():Float
        {
            return _stageY;
        }

        override public function get target():Dynamic
        {
            return node;
        }

        public function dispose():Void
        {
            node = null;
        }
    }
}
