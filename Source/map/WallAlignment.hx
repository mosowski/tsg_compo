package map;

enum WallAlignment {
    Vertical;
    Horizontal;
    LeftEdge;
    RightEdge;
    UpperEdge;
    LowerEdge;
    LeftT;
    RightT;
    UpperT;
    LowerT;
    Cross;
    RightUpper;
    LeftUpper;
    RightLower;
    LeftLower;
    Empty;
    Adamantium;
}
