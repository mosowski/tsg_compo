package;

import spell.*;
import hud.InfoText;

class Spellcaster {
	public static var queue:Array<QueuedSpell>;
	public static var curses:Array<Curse>;

	public static function init() {
		queue = new Array<QueuedSpell>();
		curses = new Array<Curse>();
	}

	public static function perform(spell:Spell, target:SpellTarget):Bool {
		if (spell.perform(target)) {
			if (spell.timeout == 0) {
				spell.apply(target);
			} else {
				queue.push(new QueuedSpell(spell, target));
			}
			return true;
		}
		if (target.caster.originalOwner != null) {
			new InfoText("Nope.", target.caster.getSpriteX(), target.caster.getSpriteY());
		}
		return false;
	}

	public static function endTurn():Void {
		for (i in -(queue.length - 1)...1) {
			var queuedSpell = queue[-i];
			queuedSpell.turnsLeft--;
			if (queuedSpell.turnsLeft == 0) {
				queuedSpell.spell.apply(queuedSpell.target);
			}
			queue.splice(-i, 1);
		}

		for (i in -(curses.length - 1)...1) {
			var curse = curses[-i];
			curse.turnsLeft--;
			if (curse.turnsLeft == 0) {
				curse.end();
				curses.splice(-i, 1);
			} else {
				curse.apply();
			}
		}
	}

	public static function curse(curse:Curse, target:CurseTarget):Void {
		if (curse.begin(target)) {
			curses.push(curse);
		}
	}
}

class QueuedSpell {
	public var spell:Spell;
	public var target:SpellTarget;
	public var turnsLeft:Int;

	public function new(spell:Spell, target:SpellTarget) {
		this.spell = spell;
		this.target = target;
		this.turnsLeft = spell.timeout;
	}
}
