package;

import flash.display.Sprite;
import flash.Lib;

import hud.StartScreen;

import fabulous.F;
import fabulous.Node;
import fabulous.Image;
import fabulous.RenderTargetTexture;


class Main extends Sprite {

	public var justQuad:Node;

	public function new () {
		super ();
		F.start(Lib.current.stage, "default", "assets", null, null, "spriteLib.json");
		F.ticked.add(onTick);

//		F.createRenderTargetTexture("rt", 512, 512);

//		createJustQuad();

		Layers.Width = Lib.current.stage.stageWidth;
		Layers.Height = Lib.current.stage.stageHeight;

		var startScreen:StartScreen = new StartScreen();
		F.rootNode.addChild(startScreen);
	}

	public function createJustQuad():Void {
		justQuad = new Node();
		var img = new Image(F.materialBasic);
		img.texture=  F.getTexture("rt");
		justQuad.addRenderable(img);
		img.setShapeRect(0,0,Layers.Width, Layers.Height);
		img.setUVRect(0,0.75,1,-0.75);
		justQuad.scaleX = justQuad.scaleY = 4;
		F.device._inputScaleX = F.device._inputScaleY = 0.25;
	}


	public function onTick(_:Dynamic):Void {
		if (false && cast(F.getTexture("rt"), RenderTargetTexture)._isCreated) {
			F.device.setViewport(0, 0, Std.int(Layers.Width), Std.int(Layers.Height));
			F.device.setRenderTargetTexture(cast F.getTexture("rt"));
			F.device.renderScene(F.rootNode);

			F.device.setViewport(0, 0, Lib.current.stage.stageWidth, Lib.current.stage.stageHeight);
			F.device.setRenderTargetTexture(null);
			F.device.renderScene(justQuad);
		} else {
			F.device.renderScene(F.rootNode);
		}

		if (State.tweener != null) {
        	State.tweener.tick();
		}
	}
}
