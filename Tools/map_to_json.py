#!/bin/python
import sys
import json
map = []

f = open(sys.argv[1], 'r')

for line in f:
    line = line.rstrip('\n').rstrip('\r');
    listOfTiles = line.split(" ")
    map.append(listOfTiles)

output_map = []


for i in range(1,len(map) - 1):
    for j in range(1, len(map[i]) - 1):
        if '0' == map[i][j]:
            output_map.append({'wall':False, 'column': j, 'row': i})
            continue
        result = int(map[i-1][j]) + 2 * int(map[i][j-1]) + 4*int(map[i][j+1]) + 8 * int(map[i+1][j])

        alignment = ""

        if 1 == result:
            alignment = "LowerEdge"
        if 8 == result:
            alignment = "UpperEdge"
        if 9 == result:
            alignment = "Vertical"
        if 2 == result:
            alignment = "LeftEdge"
        if 4 == result:
            alignment = "RightEdge"
        if 6 == result:
            alignment = "Horizontal"
        if 7 == result:
            alignment = "UpperT"
        if 11 == result:
            alignment = "LeftT"
        if 13 == result:
            alignment = "RightT"
        if 14 == result:
            alignment = "LowerT"
        if 15 == result:
            alignment = "Cross"
        if 3 == result:
            alignment = "RightLower"
        if 5 == result:
            alignment = "LeftLower"
        if 12 == result:
            alignment = "LeftUpper"
        if 10 == result:
            alignment = "RightUpper"

        if 0 != result:
            output_map.append({'wall':True, 'alignment': alignment, 'column': j, 'row': i})

for i in range(1,len(map) - 1):
    if int(map[1][i]) == 0:
        output_map.append({'wall':True, 'alignment': 'Horizontal', 'column': i, 'row': 0})
    else:
        output_map.append({'wall':True, 'alignment': 'LowerT', 'column': i, 'row': 0})
    if int(map[i][len(map) - 2]) == 0:
        output_map.append({'wall':True, 'alignment': 'Horizontal', 'column': i, 'row': len(map[i]) - 1})
    else:
        output_map.append({'wall':True, 'alignment': 'UpperT', 'column': i, 'row': len(map[i]) - 1})

for i in range(1, len(map[0]) - 1):
    if int(map[i][1]) == 0:
        output_map.append({'wall':True, 'alignment': 'Vertical', 'column': 0, 'row': i})
    else:
        output_map.append({'wall':True, 'alignment': 'RightT', 'column': 0, 'row': i})

for i in range(1, len(map[len(map) - 1]) - 1):
    if int(map[i][len(map) - 2]) == 0:
        output_map.append({'wall':True, 'alignment': 'Vertical', 'column': len(map) - 1, 'row': i})
    else:
        output_map.append({'wall':True, 'alignment': 'LeftT', 'column': len(map) - 1, 'row': i})


output_map.append({'wall':True, 'alignment': 'LeftUpper', 'column': 0, 'row': 0})
output_map.append({'wall':True, 'alignment': 'LeftLower', 'column': 0, 'row': len(map[0]) - 1})
output_map.append({'wall':True, 'alignment': 'RightUpper', 'column': len(map) - 1, 'row': 0})
output_map.append({'wall':True, 'alignment': 'RightLower', 'column': len(map) - 1, 'row': len(map[len(map)-1]) - 1})


print json.dumps({'map': output_map, 'noOfColumns': len(map), 'noOfRows': len(map[0])})
