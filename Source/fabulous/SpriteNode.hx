package fabulous {
    class SpriteNode extends Node {
        public var _sprite:Sprite;

        public function SpriteNode(material:Material) {
            _sprite = new Sprite(material);
            addRenderable(_sprite);
        }

        
        public function set pivotX(value:Float):Void {
            _sprite.pivotX = value;
        }

        
        public function set pivotY(value:Float):Void {
            _sprite.pivotY = value;
        }

        
        public function get pivotX():Float {
            return _sprite.pivotX;
        }

        
        public function get pivotY():Float {
            return _sprite.pivotY;
        }

        
        public function set frame(value:SpriteSheetFrame):Void {
            _sprite.frame = value;
        }

        
        public function get frame():SpriteSheetFrame {
            return _sprite.frame;
        }

        public function get width():Float {
            return _sprite.width * scaleX;
        }
		public function set width(v:Float):Void {
			_sprite.width = v / scaleX;
		}

        public function get height():Float {
            return _sprite.height * scaleY;
        }
		public function set height(v:Float):Void {
			_sprite.height = v / scaleY;
		}



        
        public function get derivedLeft():Float {
            return _derivedX + _derivedScaleX * _sprite._bounds.left;
        }

        public function get derivedTop():Float {
            return _derivedY + _derivedScaleY * _sprite._bounds.top;
        }

        public function get derivedRight():Float {
            return _derivedX + _derivedScaleX * _sprite._bounds.right;
        }

        public function get derivedBottom():Float {
            return _derivedY + _derivedScaleY * _sprite._bounds.bottom;
        }

        public function set material(value:Material):Void {
            _sprite.material = value;
        }
        public function get material():Material {
            return _sprite.material;
        }

		public function get textureResource():TextureResource {
			return _sprite.textureResource;
		}
		public function set textureResource(v:TextureResource):Void {
			_sprite.textureResource = v;
		}


    }

}
