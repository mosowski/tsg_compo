package spell;

import object.Character;
import map.MapTile;

class SpellTarget {
	public var caster:Character;
	public var tile:MapTile;
	public var characters:Array<Character>;

	public function new(caster:Character, tile:MapTile) {
		this.caster = caster;
		this.tile = tile;
		this.characters = tile.characters.slice(0);
	}
}
