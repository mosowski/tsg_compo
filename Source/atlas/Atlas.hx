package atlas;

import fabulous.SpriteSheetFrame;

class Atlas {
	public var animations:Map<String,AtlasAnimation>;
	public var sheetFrame:SpriteSheetFrame;

	public function new() {
		animations = new Map<String,AtlasAnimation>();
	}

	public function setup(json:Dynamic, sheetFrame:SpriteSheetFrame):Void {
		this.sheetFrame = sheetFrame;
		for (animName in Reflect.fields(json)) {
			var animation = new AtlasAnimation();
			var animationJson:Array<Dynamic> = Reflect.field(json, animName);
			for (i in 0...animationJson.length) {
				var frame:AtlasFrame = new AtlasFrame();
				U.objectFromJson(frame.rect, animationJson[i]);
				frame.rect.x += sheetFrame._left;
				frame.rect.y += sheetFrame._top;
				frame.width = frame.rect.width;
				frame.height = frame.rect.height;
				frame.rect.width /= sheetFrame._texture.sourceWidth;
				frame.rect.height /= sheetFrame._texture.sourceHeight;
				frame.rect.x /= sheetFrame._texture.sourceWidth;
				frame.rect.y /= sheetFrame._texture.sourceHeight;
				frame.texture = sheetFrame._texture;
				animation.frames.push(frame);
			}
			animations[animName] = animation; 
		}
	}
}





