package fabulous;

class Bits {

	public static function isPow2(n:Int):Bool {
		return n != 0 && (n & (n-1)) == 0;
	}

	public static function nextPow2(n:Int):Int {
		--n;
		n |= n >> 1;
		n |= n >> 2;
		n |= n >> 4;
		n |= n >> 8;
		n |= n >> 16;
		++n;
		return n;
	}
}
