package spell;

import fabulous.F;
import fabulous.DebugTextNode;

class OpenHatch extends Spell {
	public function new() {
		super("spell-key");
		displayName = "Key to Victory";
		cost = 1;
	}

	override private function canApply(target:SpellTarget):Bool {
		return target.tile.haveItem("hatch");
	}

	override public function apply(target:SpellTarget):Void {
		Layers.showMap(State.endMap);
        target.caster.originalOwner.actionPoints = 1000;
        target.caster.setTile(State.endMap.getTile(1, 1));
        var debugText:DebugTextNode = new DebugTextNode(F.device, 500, 500);
        debugText.x = 200;
        debugText.y = 200;
		F.rootNode.addChild(debugText);
		debugText.text = "Victory, Victory, Victory!";
	}
}
