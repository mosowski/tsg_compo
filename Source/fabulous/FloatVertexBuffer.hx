package fabulous;

import openfl.utils.Float32Array;
import openfl.gl.GL;

class FloatVertexBuffer extends VertexBuffer {

    public var _data:Float32Array;

    public function new(device:Device) {
        super(device);
    }

    override public function setSizeAndFormat(numVertices:Int, format:Array<VertexBufferAttrib>):Void {
        super.setSizeAndFormat(numVertices, format);
		_data = new Float32Array(_numVertices * _data32PerVertex);
		_changeRangeBegin = 0;
        _changeRangeEnd = _numVertices;
        //upload();
    }

    
    override public function upload():Void {
        if (_changeRangeBegin <_changeRangeEnd) {
			GL.bufferData(GL.ARRAY_BUFFER, _data, GL.DYNAMIC_DRAW);
            _changeRangeBegin = _numVertices;
            _changeRangeEnd = 0;
        }
		
    }

    
    override public function writeFloat(f:Float):Void {
        _data[_position] = f;
        ++_position;
    }

    
    override public function set_position(value:Int):Int {
        return _position = value;
    }
    override public function get_position():Int {
        return _position;
    }

}
