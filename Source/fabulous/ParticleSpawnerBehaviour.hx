package fabulous {
    public interface ParticleSpawnerBehaviour {
        function tick(particles:Particles,numParticles:Float):Void;
        function spawn():Void;
        function get life():Float;
        function get startX():Float;
        function get startY():Float;
        function get deltaX():Float;
        function get deltaY():Float;
        function get startSize():Float;
        function get deltaSize():Float;
        function get startRotation():Float;
        function get deltaRotation():Float;
        function get startColor():Int;
        function get endColor():Int;
    }

}
