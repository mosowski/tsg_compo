package fabulous;
	
class Logger {

    public function new() {
    }

    public function init():Void {
    }

    public function print(msg:String):Void {
        throw "Not implemented.";
    }

    
    public function i(msg:String):Void {
        print("I\\ " + msg);
    }

    
    public function e(msg:String):Void {
        print("E\\ " + msg);
    }

    
    public function w(msg:String):Void {
        print("W\\ " + msg);
    }
}
