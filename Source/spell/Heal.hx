package spell;
import hud.InfoText;
class Heal extends Spell {
	public function new() {
		super("heart", 5);
		cost = 1;
		displayName = "Heal";
	}

	override private function canApply(target:SpellTarget):Bool {
		return target.tile.characters.length > 0;
	}

	override public function apply(target:SpellTarget):Void {
        target.tile.heal(30);
	}
}
