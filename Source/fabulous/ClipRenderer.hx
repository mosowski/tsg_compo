package fabulous {

    class ClipRenderer extends Renderer {
        public var _buffer:RenderBatchBuffer;

        public function ClipRenderer(device:Device) {
            super(device);
        }

        override public function init():Void {
            _buffer = _device._renderBatcher._unorderedBuffer;
        }

        override public function processRenderable(renderable:Renderable):Void {
            var clip:Clip = renderable as Clip;

            var animationData:ClipAnimationData = clip._animationData;
            var currentFrame:Int = clip._currentFrame;
            var x:Int = clip._node._derivedX;
            var y:Int = clip._node._derivedY;
            var scaleX:Float = clip._node._derivedScaleX;
            var scaleY:Float = clip._node._derivedScaleY;
            var alpha:Float = clip._node._derivedAlpha;
            var rotation:Float = clip._node._derivedRotation;

            if (animationData) {
                var vertexBuffer:ByteArrayVertexBuffer = _buffer._vbs[0] as ByteArrayVertexBuffer;
                var indexBuffer:IndexBuffer = _buffer._ib;

                var _frame:ClipFrame = animationData._frames[currentFrame];

                var i:Int;
                var frameIndices:Array<int> = _frame._indices;
                var numFrameIndices:Int = frameIndices.length;
                _device._renderBatcher.queue(_buffer, clip._material, _frame._texture, numFrameIndices);
                indexBuffer.beginWrite();
                for (i = 0; i < numFrameIndices; ++i) {
                    indexBuffer.writeIndex(vertexBuffer.vertexPosition + frameIndices[i]);
                }
                indexBuffer.endWrite();

                var vertexData:Array<Float> = _frame._vertices;
                var numFrameVerticesFloats:Int = vertexData.length;

                var sin:Float = Math.sin(rotation);
                var cos:Float = Math.cos(rotation);

                vertexBuffer.beginWrite();
                for (i = 0; i < numFrameVerticesFloats; i+= 5) {
                    vertexBuffer.writeFloat(vertexData[i] * scaleX * cos - vertexData[int(i+1)] * scaleY * sin + x);
                    vertexBuffer.writeFloat(vertexData[int(i+1)] * scaleY * cos + vertexData[i] * scaleX * sin + y);
                    vertexBuffer.writeFloat(vertexData[int(i+2)]);
                    vertexBuffer.writeFloat(vertexData[int(i+3)]);
                    vertexBuffer.writeFloat(vertexData[int(i+4)] * alpha);
                }

                vertexBuffer.endWrite();
            }

        }

    }
}

