package spell;

class MindControl extends Spell {
    public function new() {
        super("icon-magic-mind-control", 2);
		displayName = "Soul transfer";
        cost = 1;
    }

    override private function canApply(target:SpellTarget):Bool {
        return super.canApply(target) && target.tile.characters.length > 0;
    }

    override public function apply(target:SpellTarget):Void {
        var index:Int = Std.int(Math.random() * (target.tile.characters.length - 1));
		var char = target.tile.characters[index];
		if (char.originalOwner == null) {
        	State.activePlayer.setCharacter(char);
		}
    }
}
