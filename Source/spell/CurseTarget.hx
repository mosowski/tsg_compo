package spell;

import object.Character;
import map.MapTile;

class CurseTarget {
	public var character:Character;
	public var tile:MapTile;

	public function new(character:Character = null, tile:MapTile = null) {
		this.character = character;
		this.tile = tile;
	}
}
