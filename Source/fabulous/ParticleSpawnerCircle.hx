package fabulous {
    class ParticleSpawnerCircle extends ParticleSpawnerUniformBehaviour {
        private var _minRadius:Float;
        private var _maxRadius:Float;

        public function ParticleSpawnerCircle() {
			super();
        }

        public function set minRadius(v:Float):Void {
            _minRadius = v;
        }

        public function set maxRadius(v:Float):Void {
            _maxRadius = v;
        }

        override public function spawn():Void {
            var angle:Float = Math.random() * 2 * Math.PI;
            var radius:Float = _minRadius + Math.sqrt(Math.random()) * (_maxRadius - _minRadius);
            _originOffsetX = Math.sin(angle) * radius;
            _originOffsetY = Math.cos(angle) * radius;
            super.spawn();
        }

    }

}
