    package fabulous {
	import flash.display.Sprite;
	import flash.geom.Rectangle;	
	
    class DebugSprite extends Renderable {
        public var _sprite:flash.display.Sprite;
        public var _hitmask:Hitmask;
        public var _bounds:Rectangle;
		public var _radius:Float;

        public static var _renderer:Renderer;

        public function DebugSprite() {
            _sprite = new flash.display.Sprite();
            _hitmask = new Hitmask();
            _hitmask.makeQuad();
            _bounds = new Rectangle(0, 0, 0, 0);
        }

        
        override public function get renderer():Renderer {
            return _renderer;
        }

        public function get sprite():flash.display.Sprite {
            return _sprite;
        }

        override public function getBounds():Rectangle {
            return _bounds;
        }
		
		
		override public function get radius():Float {
			return _radius;
		}

        override public function getHitmask():Hitmask {
            return _hitmask;
        }

        public function updateBounds():Void {
            _bounds = _sprite.getBounds(_sprite);
            _hitmask._path[0].setTo(_bounds.left, _bounds.top);
            _hitmask._path[1].setTo(_bounds.left, _bounds.bottom);
            _hitmask._path[2].setTo(_bounds.right, _bounds.bottom);
            _hitmask._path[3].setTo(_bounds.right, _bounds.top);
			_radius = Math.max( -_bounds.left, -_bounds.top, _bounds.right, _bounds.bottom);
        }

        
        public function set node(value:Node):Void {
            _node = value;
        }

        
        public function get node():Node {
            return _node;
        }

    }

}