package fabulous {
    import flash.utils.Dictionary;
	
    class BatchedLoader extends BaseLoader {
        public var _children:Array<BaseLoader>;
        public var _childrenByName:Dictionary;
        public var _hasLoaderStarted:Bool;
        public var _hasStopped:Bool;

        public var stopOnError:Bool;

        public var _childCompleted:Signal;

        public function BatchedLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null) {
            super(device, name, url, onComplete);
            _children = new Array<BaseLoader>();
            _childrenByName = new Dictionary();
            _childCompleted = new Signal();
        }

        public function addChild(loader:BaseLoader):Void {
            _children.push(loader);
            _childrenByName[loader.name] = loader;
            if (_hasLoaderStarted) {
                loader._completed.addOnce(onChildLoaderComplete);
                loader.load();
            }
        }

        public function addChildren(loaders:Array):Void {
            for (var i:Int = 0; i < loaders.length; ++i) {
                _children.push(loaders[i]);
                _childrenByName[loaders[i].name] = loaders[i];
            }
            if (_hasLoaderStarted) {
                for (i = 0; i < loaders.length; ++i) {
                    loaders[i]._completed.addOnce(onChildLoaderComplete);
                    loaders[i].load();
                }
            }
        }

        public function getChild(name:String):BaseLoader {
            return _childrenByName[name];
        }

        override public function load():Void {
            super.load();
            if (_children.length == 0) {
                _completed.post(_name);
            } else {
                var childrenCopy:Array<BaseLoader> = _children.slice();
                for (var i:Int = 0; i < childrenCopy.length; ++i) {
                    childrenCopy[i]._completed.addOnce(onChildLoaderComplete);
					childrenCopy[i]._crashed.addOnce(onChildLoaderError);
                    childrenCopy[i].load();
                }
            }
        }

        private function removeChild(childName:String):Void {
            for (var i:Int = 0; i < _children.length; ++i) {
                if (_children[i].name == childName) {
                    _children.splice(i, 1);
                    break;
                }
            }
        }

        private function onChildLoaderError(childName:String):Void {
            removeChild(childName);
            if (stopOnError) {
                _hasStopped = true;
                _crashed.post(name);
            }
        }


        private function onChildLoaderComplete(childName:String):Void {
            if (!_hasStopped) {
                removeChild(childName);

                _childCompleted.post(childName);

                if (_children.length == 0) {
                    _completed.post(_name);
                }
            }
        }

    }

}
