package fabulous;

import flash.display.BitmapData;    

import openfl.gl.GLTexture;

import openfl.Assets;

class TextureManager {
    public var _device:Device;
    public var _textures:Map<String,Texture>;

    public var _unloadedFallbackGLTexture:GLTexture;
    public var _unloadedTexture:UncompressedTexture;
    public static var UNLOADED_TEXTURE_NAME:String = "__unloadedTexture__";

    public function new(device:Device) {
        _device = device;
        _textures = new Map<String,Texture>();
    }

    public function init():Void {
        _unloadedTexture = new UncompressedTexture();
        _unloadedTexture.loadFromBitmapData(new BitmapData(1, 1, true, 0));
        _textures[UNLOADED_TEXTURE_NAME] = cast (_unloadedTexture, Texture);
        _unloadedFallbackGLTexture = _unloadedTexture._glTexture;
    }

    /**
     * Returns texture resource if loaded, else cues proper loader which
     * automatically uploads texture to GPU when loaded.
     * @param	name
     * @return
     */
    public function loadTexture(path:String):Texture {
		var texture = _textures[path];
        if (!_textures.exists(path)) {
			if (path.indexOf("http://")<0) {
                var localPath = _device.getPath(path);
				texture = loadLocalUncompressedTexture(localPath);
			} else {
			}

			_textures[path] = texture;
		} else {
		}
		return texture;
    }

	public function loadLocalUncompressedTexture(path:String):Texture {
		var texture = new UncompressedTexture();
		texture._name = path;
		texture.loadFromBitmapData(Assets.getBitmapData(path));
		return texture;
	}

	public function createTextureFromBitmapData(name:String, bitmapData:BitmapData):UncompressedTexture {
		var texture = new UncompressedTexture();
		texture._name = name;
		texture.loadFromBitmapData(bitmapData);
		_textures[name] = cast texture;
		return texture;
	}

	public function createRenderTargetTexture(name:String, width:Int, height:Int):RenderTargetTexture {
		var texture = new RenderTargetTexture();
		texture._name = name;
		texture.init(width, height);
		_textures[name] = cast texture;
		return texture;
	}

    public function initAsUnloaded(texture:Texture):Void {
        texture._glTexture = _unloadedFallbackGLTexture;
        texture._hasAlpha = _unloadedTexture._hasAlpha;
        texture._isCompressed = _unloadedTexture._isCompressed;
        texture._width = _unloadedTexture._width;
        texture._height = _unloadedTexture._height;
    }

    public function disposeTexture(path:String):Void {
        var texture:Texture = _textures[path];
        texture.dispose();
		_textures.remove(path);
    }

}
