package;

import flash.display.DisplayObjectContainer;
import flash.events.Event;
import flash.Lib;

import map.WorldMap;
import object.Character;

import hud.Spellbar;

import fabulous.F;
import fabulous.Node;
import fabulous.DebugTextNode;

class Layers {

	public static var Width:Float = 512;
	public static var Height:Float = 384;

	public static var map:Node;
    public static var fxLayer:Node;
	public static var spellbar:Spellbar;
	public static var debugText:DebugTextNode;

	private static var _targetCamX:Float;
	private static var _targetCamY:Float;
	public static var _currCamX:Float;
	public static var _currCamY:Float;
	private static var _targetCharacter:Character;
	public static var viewWidth:Float;
	public static var viewHeight:Float;

	public static var scale:Float = 4;

	public static var lastFPSTime:Float;
	public static var framesCount:Int;

	public static function init():Void {
		scale = Math.round(Lib.current.stage.stageWidth / 512);
		F.rootNode.scaleX = scale;
		F.rootNode.scaleY = scale;

		map = new Node();
        fxLayer = new Node();
		F.rootNode.addChild(map);
        F.rootNode.addChild(fxLayer);

		spellbar = new Spellbar();
		F.rootNode.addChild(spellbar);
		setHudPosition();

		debugText = new DebugTextNode(F.device, 200, 200);
		//F.rootNode.addChild(debugText);
		debugText.text = "Hello Motherfucker!";
		lastFPSTime = F.exactTime;
		framesCount = 0;

		_targetCamX = 0;
		_targetCamY = 0;
		_currCamX = 0;
		_currCamY = 0;
		viewWidth = 1000;
		viewHeight = 1000;
		Timers.frame.add(onFrame);

		Lib.current.stage.addEventListener(Event.RESIZE, onStageResize);
	}

	private static function onStageResize(e:Event):Void {
		scale = Math.round(Lib.current.stage.stageWidth / 512);
		F.rootNode.scaleX = scale;
		F.rootNode.scaleY = scale;
		lookAt(_targetCamX, _targetCamY);
		setHudPosition();
	}

	private static function onFrame(_:Dynamic):Void {
		if (_targetCharacter != null && _targetCharacter.tile != null) {
			lookAt(_targetCharacter.getSpriteX(), _targetCharacter.getSpriteY());
			F.materialLight._lightPosition = [(map.x + _targetCharacter.getSpriteX() + 16), (map.y + _targetCharacter.getSpriteY() + 16), scale];
		}
		_currCamX += (-_targetCamX - _currCamX) * 0.1 * (F.dt / (0.0166));
		_currCamY += (-_targetCamY - _currCamY) * 0.1 * (F.dt / (0.0166));
		map.x = Math.round(_currCamX);
		map.y = Math.round(_currCamY);
		if (map.x > 0) {
			map.x = 0;
		}
		if (map.y > 0) {
			map.y = 0;
		}
        fxLayer.x = map.x;
        fxLayer.y = map.y;
		if (F.exactTime >= lastFPSTime + 1) {
			debugText.text = "" + framesCount;
			framesCount = 0;
			lastFPSTime = F.exactTime;
		}
		framesCount++;
	}

	public static function showMap(worldMap:WorldMap):Void {
		while (map.numChildren > 0) {
			map.removeChild(map.getChild(0));
		}
		map.addChild(worldMap.sprite);
		viewWidth = worldMap.pixelsWidth;
		viewHeight = worldMap.pixelsHeight;
	}

	public static function lookAtCharacter(character:Character):Void {
		_targetCharacter = character;
	}

	public static function lookAt(x:Float, y:Float):Void {
		_targetCamX = Math.min(viewWidth - Width / scale, Math.max(0, x - Width/2/scale));
		_targetCamY = Math.min(viewHeight - Height / scale, Math.max(0, y - Height/2/scale));
	}

	private static function setHudPosition():Void {
		spellbar.x = Width / 2 / scale - spellbar.totalWidth / 2;
		spellbar.y = Height / scale - 40;
	}
}
