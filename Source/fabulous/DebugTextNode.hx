package fabulous;
	
class DebugTextNode extends Node {

    public var _text:Text;
    //public var _colorMaterial:MaterialTexSubConst;

    public function new(device:Device, w:Int, h:Int) {
		super();
        //_colorMaterial = device.materialMgr.getMaterialTexSubConst();
        _text = new Text();
        _text.font = device._fontMgr._debugFont;
        _text.widthLimit = w;
        _text.heightLimit = h;
        _text.wordWrap = true;
        _text.downscaleToLimits = false;
        _text.upscaleToLimits = false;
        _text.fontSize = 11;
        addRenderable(_text);
    }

	public var width(get,set):Float;
    public function set_width(v:Float):Float {
        return _text.widthLimit = v;
    }
    public function get_width():Float {
        return _text.widthLimit;
    }

	public var height(get,set):Float;
    public function set_height(v:Float):Float {
        return _text.heightLimit = v;
    }
    public function get_height():Float {
        return _text.heightLimit;
    }

	public var text(get,set):String;
    public function set_text(v:String):String {
        return _text.text = v;
    }
    public function get_text():String {
        return _text.text;
    }
}
