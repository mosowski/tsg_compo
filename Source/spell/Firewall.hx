package spell;

class Firewall extends Spell {
	public function new() {
		super("icon-fire", 3);
		displayName = "Firewall";
	}

	override private function canApply(target:SpellTarget):Bool {
		return super.canApply(target) && target.tile.isWalkable();
	}

	override public function apply(target:SpellTarget):Void {
		Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile));
		Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(0,1)));
		Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(0,-1)));
		Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(1,0)));
		Spellcaster.curse(new CurseFire(), new CurseTarget(null, target.tile.getNeighbour(-1,0)));
	}
}
