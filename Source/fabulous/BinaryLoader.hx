package fabulous {
	import flash.display.Bitmap;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.net.DynamicEncoding;
    import flash.net.URLLoader;
    import flash.net.URLLoaderDataFormat;
    import flash.net.URLRequest;
    import flash.utils.ByteArray;
	
    class BinaryLoader extends BaseLoader {
        public var _format:String;
        public var _inflated:Bool;
        public var _data:Dynamic;
        public var _urlLoader:URLLoader;

        public function BinaryLoader(device:Device, name:String = "", url:String = "", onComplete:Function = null) {
            super(device, name, url, onComplete);
            _urlLoader = new URLLoader();
            _urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
        }

        override public function load():Void {
            super.load();

            addListeners();
            _urlLoader.load(new URLRequest(_url));
        }

        private function addListeners():Void {
            _urlLoader.addEventListener(Event.COMPLETE, onLoaderComplete);
            _urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoaderError);
            _urlLoader.addEventListener(IOErrorEvent.DISK_ERROR, onLoaderError);
        }

        private function removeListeners():Void {
            _urlLoader.removeEventListener(Event.COMPLETE, onLoaderComplete);
            _urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, onLoaderError);
            _urlLoader.removeEventListener(IOErrorEvent.DISK_ERROR, onLoaderError);
        }

        override protected function onLoaderComplete(e:Event):Void {
            _data = _urlLoader.data;
            super.onLoaderComplete(e);
        }

        override public function getByteArray():ByteArray {
            return _data as ByteArray;
        }

        override public function getCompressedByteArray():ByteArray {
            var bytes:ByteArray = _data as ByteArray;
            if (!_inflated) {
                bytes.inflate();
                _inflated = true;
            }
            return bytes;
        }

        override public function getString():String {
            return _data as String;
        }

        override public function getJSON():Dynamic {
            return JSON.parse(_data as String);
        }

        override public function getBitmap():Bitmap {
            return _data as Bitmap;
        }

        override public function getAMF3():Dynamic {
            var bytes:ByteArray = _data as ByteArray;
            bytes.objectEncoding = DynamicEncoding.AMF3;
            bytes.position = 0;
            return bytes.readDynamic();
        }

        override public function getCompressedAMF3():Dynamic {
            var bytes:ByteArray = _data as ByteArray;
            bytes.objectEncoding = DynamicEncoding.AMF3;
            bytes.position = 0;
            if (!_inflated) {
                bytes.inflate();
                _inflated = true;
            }
            return bytes.readDynamic();
        }

        override public function unload():Void {
            _data = null;
            _urlLoader.data = null;
			removeListeners();
        }
    }

}
