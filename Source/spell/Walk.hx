package spell;

class Walk extends Spell {
	public function new() {
		super("icon-walk", -1);
		cost = 1;
		displayName = "Walk";
	}

	override private function canApply(target:SpellTarget):Bool {
		return super.canApply(target) && target.tile.isWalkable() && target.caster.walkDistance(target.tile) < 7;
	}

	override public function apply(target:SpellTarget):Void {
		trace("chyba ide");
		State.setAnimationDelay(target.caster.walkTo(target.tile));
	}
}
