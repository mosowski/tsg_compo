package fabulous;

import openfl.gl.GL;
import openfl.gl.GLShader;
import openfl.gl.GLProgram;
import openfl.gl.GLUniformLocation;

class Shader {
    public var _vertexProgramCode:String;
    public var _fragmentProgramCode:String;

	public var _vertexShader:GLShader;
	public var _fragmentShader:GLShader;
	public var _program:GLProgram;

	public var _uniforms:Map<String,GLUniformLocation>;
	public var _attribs:Map<String,Int>;

    public function new() {
		_uniforms = new Map<String,GLUniformLocation>();
		_attribs = new Map<String,Int>();
    }

    
    public function setVertexProgram(vertexProgramCode:String):Void {
        _vertexProgramCode = vertexProgramCode;
    }

    public function setFragmentProgram(fragmentProgramCode:String):Void {
        _fragmentProgramCode = fragmentProgramCode;
    }

	public function getUniform(name:String):GLUniformLocation {
		if (!_uniforms.exists(name)) {
			_uniforms[name] = GL.getUniformLocation(_program, name);
		}
		return _uniforms[name];
	}

	public function getAttrib(name:String):Int {
		if (!_attribs.exists(name)) {
			_attribs[name] = GL.getAttribLocation(_program, name);
		}
		return _attribs[name];
	}

    public function compile():Void {
        if (_program == null) {

			_vertexShader = GL.createShader(GL.VERTEX_SHADER);
			GL.shaderSource(_vertexShader, _vertexProgramCode);
			GL.compileShader(_vertexShader);

			if (GL.getShaderParameter(_vertexShader, GL.COMPILE_STATUS) == 0) {
				Log.e("Vertex Program compilation error.");
				Log.e(GL.getShaderInfoLog(_vertexShader));
				throw "FabulousError";
			}


			_fragmentShader = GL.createShader(GL.FRAGMENT_SHADER);
			GL.shaderSource(_fragmentShader, _fragmentProgramCode);
			GL.compileShader(_fragmentShader);

			if (GL.getShaderParameter(_fragmentShader, GL.COMPILE_STATUS) == 0) {
				Log.e("Fragment Program compilation error:");
				Log.e(GL.getShaderInfoLog(_fragmentShader));
				throw "FabulousError";
			}

			_program = GL.createProgram();
			GL.attachShader(_program, _vertexShader);
			GL.attachShader(_program, _fragmentShader);
			GL.linkProgram(_program);

			if (GL.getProgramParameter(_program, GL.LINK_STATUS) == 0) {
				Log.e("Shader linking error.");
				throw "FabulousError";
			}
        }
	}
}
