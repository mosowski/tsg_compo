package fabulous;
	
class Log {
    public static var _loggers:Array<Logger> = new Array<Logger>();

    public static function addLogger(logger:Logger):Void {
        _loggers.push(logger);
        logger.init();

    }

    
    public static function i(msg:String):Void {
		for (i in 0..._loggers.length) {
            _loggers[i].i(msg);
        }
    }

    
    public static function e(msg:String):Void {
		for (i in 0..._loggers.length) {
            _loggers[i].e(msg);
        }
    }

    
    public static function w(msg:String):Void {
		for (i in 0..._loggers.length) {
            _loggers[i].w(msg);
        }
    }

}
