package fabulous;

class SpriteSheetFrame {
    public var _left:Float;
    public var _top:Float;
    public var _right:Float;
    public var _bottom:Float;
    public var _imageWidth:Float;
    public var _imageHeight:Float;
    public var _textureId:String;
    public var _pivotX:Float;
    public var _pivotY:Float;
    public var _texture:Texture;

    public function new(left:Float, top:Float, right:Float, bottom:Float, width:Float, height:Float, pivotX:Float, pivotY:Float, textureId:String) {
        _left = left;
        _top = top;
        _right = right;
        _bottom = bottom;
        _imageWidth = width;
        _imageHeight = height;
        _pivotX = pivotX;
        _pivotY = pivotY;
        _textureId = textureId;
    }

	public var imageWidth(get,null):Float;
    public function get_imageWidth():Float {
        return _imageWidth;
    }

	public var imageHeight(get,null):Float;
    public function get_imageHeight():Float {
        return _imageHeight;
    }
}
