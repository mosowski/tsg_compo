#!/usr/bin/python
import sys
import json

f = open(sys.argv[1], 'r')

dimensions = f.readline().rstrip('\n').rstrip('\r').split(" ")
noOfAnimations = int(f.readline())
animations = {}
for i in range(0, noOfAnimations):
    animationName = f.readline().rstrip('\n').rstrip('\r').rstrip(" ")
    noOfFrames = int(f.readline().rstrip('\n').rstrip('\r'))
    frames = []
    for j in range(0, noOfFrames):
        uv = f.readline().rstrip('\n').rstrip('\r').split(" ")
        frames.append({'x': int(uv[0]), 'y': int(uv[1]), 'width': int(dimensions[0]), 'height': int(dimensions[1])})

    animations[animationName] = frames

print json.dumps(animations)

o = open(sys.argv[2], 'w')
o.write(json.dumps(animations))
