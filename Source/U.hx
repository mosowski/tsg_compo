package;

class U {

	public static function objectFromJson(object:Dynamic, json:Dynamic):Void {
		for (field in Reflect.fields(json)) {
			Reflect.setField(object, field, Reflect.field(json, field));
		}
	}
}

